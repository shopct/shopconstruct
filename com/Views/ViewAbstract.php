<?php

namespace ShopCT\Views;


use ShopCT\Core\View;

abstract class ViewAbstract
{

    public $sections;

    /**
     * ViewAbstract constructor.
     * @param $sections
     */
    public function __construct($sections = array())
    {
        $this->sections = $sections;
    }

    /**
     * @return string base view
     */
    abstract function base();

    public function get()
    {
        $view = View::get($this->base());

        foreach ($this->sections as $sectionName => $section) {
            $view = str_replace('<shop-ct-section>' . $sectionName . '</shop-ct-section>', $section, $view);
        }

        return $view;
    }
}