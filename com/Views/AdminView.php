<?php

namespace ShopCT\Views;


class AdminView extends ViewAbstract
{

    public function base()
    {
        return 'admin/layouts/default';
    }

}