<?php

namespace ShopCT\Core;

/**
 * todo: add middlewares and authorization for each controller
 */
class AdminRouter
{
    public static function init()
    {
        if (!isset($_GET['page'])) {
            return;
        }
        if (substr($_GET['page'], 0, 8) !== 'shop_ct_') {
            return;
        }

        /**
         * ex:
         * admin.php?page=shop_ct_dashboard
         * $controllerName = "Dashboard";
         *
         * call DashboardController::index()
         *
         *
         * admin.php?page=shop_ct_dashboard&action=update
         * $controllerName = "Dashboard";
         *
         * call DashboardController::update()
         * */

        $controllerName = ucfirst(substr($_GET['page'], 8));

        $action = 'index';

        if (isset($_GET['action']) && !empty($_GET['action'])) {
            $action = $_GET['action'];
        }

        $className = $controllerName . 'Controller';

        $fullClassName = 'ShopCT\Controllers\\' . $className;

        $instance = new $fullClassName;
        if (method_exists($instance, $action)) {
            $result = call_user_func(array($instance, $action));

            echo $result;
        }
    }
}