<?php

namespace ShopCT\Core;


class EventManager
{

    /**
     * @var self
     */
    private static $_instance;

    private $observers = array();

    public static $defaultNamespace = 'ShopCT\Controllers';

    /**
     * implement singleton pattern
     *
     * @return EventManager
     */
    public static function instance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * @param $event string
     * @param $observer string ClassName@method
     */
    public function add($event, $observer)
    {
        $observerParts = explode('@', $observer, 2);

        if (count($observerParts) !== 2) {
            wp_die('invalid observer format: ' . $observer . ', observer parameter should be like this: ObserverClassName@method, if you want to add a procedural function just use WordPress\' native add_action()');
        }

        $observerClassName = $observerParts[0];
        $observerMethod = $observerParts[1];


        if (strpos($observerClassName, '\\') === false) {
            $observerClassName = $this->getFullClassName($observerClassName);
        }

        if (!isset($this->observers[$observerClassName]) || !$this->observers[$observerClassName] instanceof $observerClassName) {
            $this->observers[$observerClassName] = new $observerClassName();
        }

        if (!method_exists($this->observers[$observerClassName], $observerMethod)) {
            wp_die('trying to attach a non existing method ' . $observerMethod . ' in ' . $observerClassName . ' for event ' . $event);
        }

        add_action($event, array($this->observers[$observerClassName], $observerMethod));

    }

    private function getFullClassName($className)
    {
        return self::$defaultNamespace . '\\' . $className;
    }

}