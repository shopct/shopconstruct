<?php

namespace ShopCT\Core;


class View
{
    public static function get($template, $data = array())
    {
        $templatePath = SHOP_CT_TEMPLATES_PATH . DIRECTORY_SEPARATOR . str_replace('/', DIRECTORY_SEPARATOR, $template) . '.php';

        /**
         * todo: add theme template compatibility
         */

        if (!file_exists($templatePath)) {
            wp_die('Trying to load non existing view: ' . $templatePath);
        }

        ob_start();

        if (!empty($data)) {
            extract($data, EXTR_SKIP);
        }

        try {
            include $templatePath;
        } catch (\Exception $e) {
            wp_die($e->getMessage());
        }

        return ltrim(ob_get_clean());
    }
}