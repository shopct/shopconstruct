<?php

namespace ShopCT\Core;


class AdminMenu
{
    public $items;
    public $hooks;

    public function __construct($items)
    {
        $this->items = $items;
        add_action('admin_menu', array($this, 'init'));
    }

    public function init()
    {
        foreach ($this->items as $menuItem) {
            $title = $menuItem['title'];
            $menuTitle = $menuItem['menu_title'];
            $capability = $menuItem['capability'];
            $menuSlug = $menuItem['menu_slug'];
            $parentSlug = $menuItem['parent_slug'];
            $iconImage = $menuItem['icon_image'];

            if (empty($parentSlug)) {
                add_menu_page($title, $menuTitle, $capability, $menuSlug, array(AdminRouter::class, 'init'), $iconImage);
            } else {
                add_submenu_page($parentSlug, $title, $menuTitle, $capability, $menuSlug, array(AdminRouter::class, 'init'));
            }


//			if ( empty($parent_slug) ) {
//				$this->toplevel_pages[ $this->toplevel_pages_count ] = add_menu_page( $title, $menu_title, $capability, $menu_slug, array( $this, 'init_pages' ), $icon_image );
//				add_action( 'load-' . $this->toplevel_pages[ $this->toplevel_pages_count ], array( $this, 'admin_init' ) );
//				$this->toplevel_pages_count ++;
//			} elseif ( $priority == 2 ) {
//				$this->pages[ $this->pages_count ] = add_submenu_page( $parent_slug, $title, $menu_title, $capability, $menu_slug, array( $this, 'init_pages' ) );
//				add_action( 'load-' . $this->pages[ $this->pages_count ], array( $this, 'admin_init' ) );
//				$this->pages_count ++;
//			}
        }

        global $submenu;

        if (isset($submenu['shop_ct_products'][0][0])) {
            /* Remove 'shop_ct_products' sub menu item and replace it with Products */
            if ($submenu['shop_ct_products'][0][0] == "ShopConstruct") {
                $submenu['shop_ct_products'][0][0] = "Products";
            }
        }
    }
}