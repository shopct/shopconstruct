<?php

namespace ShopCT\Core;

/**
 * Class Migration
 * @package ShopCT\Core
 * todo: add transactions and keep backlog of migrations
 */
class Migration
{
    public function call($class)
    {
        $instance = new $class;

        if (method_exists($instance, 'run')) {
            call_user_func(array($instance, 'run'));
        } else {
            wp_die('migration class does not have "run" method');
        }
    }
}