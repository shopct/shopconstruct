<?php

namespace ShopCT\Core;


abstract class Term
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $slug;

    /**
     * @var string
     */
    public $description;

    /**
     * @var int
     */
    public $parent = 0;


    /**
     * @var \WP_Term
     */
    public $term;

    /**
     * Term constructor.
     * @param int $id
     */
    public function __construct($id = NULL)
    {
        if (!empty($id) && is_numeric($id)) {
            $term = get_term($id, static::$taxonomy);

            if ($term instanceof \WP_Term) {
                $this->term = $term;
                $this->id = $term->term_id;
                $this->name = $term->name;
                $this->slug = $term->slug;
                $this->description = $term->description;
                $this->parent = $term->parent;
            }
        }
    }

    /**
     * Checkes if all required fields are correctly set.
     *
     * @return bool
     */
    private function canBeSaved()
    {
        return !empty($this->name) && taxonomy_exists(static::$taxonomy);
    }

    /**
     * @return string|\WP_Error
     */
    public function getPermalink()
    {
        return get_term_link($this->id, static::$taxonomy);
    }

    /**
     * @return array|bool|\WP_Error
     */
    public function save()
    {
        if (!$this->canBeSaved()) {
            return false;
        }

        $args = array(
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => $this->description,
            'parent' => $this->parent,
        );

        $result = is_null($this->id)
            ? wp_insert_term($this->name, static::$taxonomy, $args)
            : wp_update_term($this->id, static::$taxonomy, $args);

        return $result;
    }

    /**
     * @param $name
     * @param $taxonomy
     * @return bool|Term
     */
    public static function getByName($name, $taxonomy)
    {
        $term = get_term_by('name', $name, $taxonomy);

        if (false !== $term) {
            return new static($term->term_id);
        }

        return false;

    }

    /**
     * @return static[]|null
     */
    public static function all()
    {
        return static::get();
    }

    /**
     * @param array $args
     * @return static[]|null
     */
    public static function get($args = array())
    {
        $args = wp_parse_args($args, array(
            'taxonomy' => static::$taxonomy,
            'hide_empty' => true, //don't show empty terms
            'number' => 0, //get all,
            'orderby' => 'term_id',
            'order' => 'DESC'
        ));

        $terms = get_terms($args);

        if (empty($terms)) {
            return null;
        }

        $result = array();
        foreach ($terms as $term) {
            $result[] = new static($term->term_id);
        }

        return $result;

    }
}