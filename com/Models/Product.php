<?php

namespace ShopCT\Models;


use ShopCT\Lib\Dates;

class Product extends PostType
{

    const SHOW_PER_PAGE = 5;

    /**
     * @var string
     */
    public static $post_type = 'shop_ct_product';

    /**
     * @var array
     */
    public static $post_params = array(
        'post_author' => 'post_author',
        'post_date' => 'post_date',
        'post_date_gmt' => 'post_date_gmt',
        'post_content' => 'post_content',
        'post_title' => 'post_title',
        'post_excerpt' => 'post_excerpt',
        'post_status' => 'post_status',
        'comment_status' => 'comment_status',
        'ping_status' => 'ping_status',
        'post_password' => 'post_password',
        'post_name' => 'post_name',
        'to_ping' => 'to_ping',
        'pinged' => 'pinged',
        'post_modified' => 'post_modified',
        'post_modified_gmt' => 'post_modified_gmt',
        'post_content_filtered' => 'post_content_filtered',
        'post_parent' => 'post_parent',
        'guid' => 'guid',
        'menu_order' => 'menu_order',
        'post_type' => 'post_type',
        'post_mime_type' => 'post_mime_type'
    );

    /**
     * @var bool
     */
    public static $has_meta = true;

    /**
     * @var array
     */
    public static $meta_keys = array(
        'product_type',
        'visibility',
        'sku',
        'manage_stock',
        'stock',
        'stock_status',
        'backorders',
        'width',
        'length',
        'height',
        'weight',
        'regular_price',
        'sale_price',
        'sale_price_dates_from',
        'sale_price_dates_to',
        'sold_individually',
        'virtual',
        'downloadable',
        'downloadable_files',
        'product_image_gallery',
        'download_limit',
        'download_expiry',
        'download_type',
        'product_url',
        'product_button_text',
        'meta_title',
        'meta_description',
        'meta_noindex',
        'note'
    );

    /**
     * @var []
     */
    public $attributes;

    /**
     * @var ProductAttributeTerm[]
     */
    public $attribute_terms;

    /**
     * @var ProductAttributeTerm[]
     */
    public $added_attribute_terms;

    public function __construct($param = null)
    {
        parent::__construct($param);

        /**
         * todo: optimise this
         */
        $attrs = ProductAttribute::getAll();
        foreach ($attrs as $attr) {
            $terms = wp_get_post_terms($this->id, $attr->slug);
            if (!is_wp_error($terms) && !empty($terms)) {
                $this->attributes[$attr->slug] = array();
                foreach ($terms as $term) {
                    $termObj = new ProductAttributeTerm($term->term_id);
                    $this->attributes[$attr->slug][] = $termObj;
                    $this->attribute_terms[] = $termObj;
                }
            }

        }
    }

    /**
     * @return ProductMeta
     */
    public function metaInstance()
    {
        return ProductMeta::getInstance($GLOBALS['wpdb']->prefix . 'shop_ct_product_meta');
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->post->post_title;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        if (is_null($this->regular_price)) {
            return 0;
        } elseif (!$this->isOnSale()) {
            $price = $this->regular_price;
        } elseif (is_numeric($this->sale_price) && $this->sale_price < $this->regular_price) {
            $price = $this->sale_price;
        } else {
            $price = $this->regular_price;
        }
        return apply_filters('shop_ct_get_price', $price, $this);
    }

    /**
     * @return bool
     */
    public function isOnSale()
    {
        return ($this->hasSalePrice() && $this->isInSaleSchedule());
    }

    /**
     * @return bool
     */
    public function hasSalePrice()
    {
        return (bool)(!empty($this->sale_price) && $this->sale_price < $this->regular_price);
    }

    /**
     * @return bool
     */
    public function isInSaleSchedule()
    {
        if (empty($this->sale_price_dates_from) && empty($this->sale_price_dates_to)) {
            return true;
        }

        $current_date = Dates::getWpDatetime();

        $sale_started = (!empty($this->sale_price_dates_from)
            ? ($current_date > new \DateTime($this->sale_price_dates_from, Dates::getTimezone()))
            : true);

        $sale_not_ended = (!empty($this->sale_price_dates_to)
            ? ($current_date < new \DateTime($this->sale_price_dates_to, Dates::getTimezone()) && $sale_started)
            : $sale_started);

        return $sale_not_ended;
    }

    /**
     * @return bool
     */
    public function hasSaleCountdown()
    {

        if (!empty($this->sale_price_dates_from) || !empty($this->sale_price_dates_to)) {
            return $this->isInSaleSchedule();

        }

        return false;
    }

    /**
     * @return string
     */
    public static function getDownloadPermissionsTableName()
    {
        return $GLOBALS['wpdb']->prefix . 'shop_ct_download_permissions';
    }

    /**
     * @param ProductAttribute $attribute
     * @return mixed|null
     */
    public function getAttributeTerms($attribute = null)
    {
        if (null === $attribute) {
            return $this->attribute_terms;
        } else {
            return (isset($this->attributes[$attribute->slug]) && !empty($this->attributes[$attribute->slug]) ? $this->attributes[$attribute->slug] : null);
        }
    }

    /**
     * @param ProductAttributeTerm $term
     * @return $this
     */
    public function addAttributeTerm(ProductAttributeTerm $term)
    {
        $exists = false;

        foreach ($this->attribute_terms as $existing_term) {
            if ($existing_term->id === $term->id) {
                $exists = true;
            }
        }
        if (false === $exists) {
            $this->attribute_terms[] = $term;
            $this->attributes[$term->getAttribute()->slug][] = $term;
            $this->added_attribute_terms[] = $term;
        }


        return $this;
    }

    /**
     * Get total stock.
     *
     * @return int
     */
    public function getTotalStock()
    {
        if (empty($this->total_stock)) {
            $this->total_stock = max(0, $this->getStockQuantity());
        }
        return apply_filters('shop_ct_stock_amount', $this->total_stock);
    }

    /**
     * Returns whether or not the product can be backordered.
     *
     * @return bool
     */
    public function backordersAllowed()
    {
        return apply_filters('shop_ct_product_backorders_allowed', $this->backorders === 'yes' || $this->backorders === 'notify' ? true : false, $this->id);
    }

    /**
     * Returns number of items available for sale.
     *
     * @return int
     */
    public function getStockQuantity()
    {
        return apply_filters('shop_ct_get_stock_quantity', $this->managingStock() ? apply_filters('shop_ct_stock_amount', $this->stock) : null, $this);
    }

    /**
     * @return bool
     */
    public function managingStock()
    {
        return (!isset($this->manage_stock) || !$this->manage_stock || shopCTProductSettings()->manage_stock !== 'yes') ? false : true;
    }

    /**
     * Returns whether or not the product is in stock.
     *
     * @return bool
     */
    public function isInStock()
    {
        return (($this->managingStock() && $this->getTotalStock() > shopCTProductSettings()->notify_no_stock_amount) || $this->stock_status === 'instock' || $this->backordersAllowed());
    }

    public function backordersRequireNotification()
    {
        return $this->backorders === 'notify';
    }

    /**
     * @return int
     */
    public function getImageId()
    {
        if (null === $this->image_id && !empty($this->product_image_gallery)) {
            $this->image_id = $this->product_image_gallery[0];
        }

        return $this->image_id;
    }

    /**
     * Returns the main product image URL.
     * @param string $size
     * @return string
     */
    public function getImageUrl($size = 'thumbnail')
    {
        $imageId = $this->getImageId();
        if (is_null($imageId)) {
            return SHOP_CT_PUBLIC_URL . "images/placeholder.png";
        }

        return wp_get_attachment_image_src($imageId, $size)[0];
    }

    /**
     * @return array
     */
    public function getAvailability()
    {
        if ($this->managingStock()) {
            if ($this->isInStock() && $this->getTotalStock() > shopCTProductSettings()->notify_no_stock_amount) {
                switch (shopCTProductSettings()->stock_format) {
                    case 'no_amount' :
                        $availability = __('In stock', 'shop_ct');
                        break;
                    case 'low_amount' :
                        if ($this->getTotalStock() <= shopCTProductSettings()->notify_low_stock_amount) {
                            $availability = sprintf(__('Only %s left in stock', 'shop_ct'), $this->getTotalStock());
                            if ($this->backordersAllowed() && $this->backordersRequireNotification()) {
                                $availability .= ' ' . __('(can be backordered)', 'shop_ct');
                            }
                        } else {
                            $availability = __('In stock', 'shop_ct');
                        }
                        break;
                    default :
                        $availability = sprintf(__('%s in stock', 'shop_ct'), $this->getTotalStock());
                        if ($this->backordersAllowed() && $this->backordersRequireNotification()) {
                            $availability .= ' ' . __('(can be backordered)', 'shop_ct');
                        }
                        break;
                }
                $class = 'in-stock';
            } elseif ($this->backordersAllowed() && $this->backordersRequireNotification()) {
                $availability = __('Available on backorder', 'shop_ct');
                $class = 'available-on-backorder';
            } elseif ($this->backordersAllowed()) {
                $availability = __('In stock', 'shop_ct');
                $class = 'in-stock';
            } else {
                $availability = __('Out of stock', 'shop_ct');
                $class = 'out-of-stock';
            }
        } elseif (!$this->isInStock()) {
            $availability = __('Out of stock', 'shop_ct');
            $class = 'out-of-stock';
        } else {
            $availability = __('In stock', 'shop_ct');
            $class = 'in-stock';
        }
        return apply_filters('shop_ct_get_availability', array('availability' => $availability, 'class' => $class), $this);
    }

    /**
     * @return string
     */
    public function getShortcode()
    {
        return '[ShopConstruct_product id="' . $this->id . '"]';
    }

    /**
     * Wrapper for get_permalink.
     *
     * @return string
     */
    public function getPermalink()
    {
        return get_permalink($this->id);
    }

    /**
     * Checks if a product needs shipping.
     *
     * @return bool
     */
    public function needsShipping()
    {
        return !$this->virtual;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function save()
    {
        parent::save();

        if (!empty($this->added_attribute_terms)) {
            foreach ($this->added_attribute_terms as $term) {
                wp_add_object_terms($this->id, $term->id, $term->getAttribute()->slug);
            }
        }

        return true;
    }
}