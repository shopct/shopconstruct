<?php

namespace ShopCT\Models;


class Meta
{
    /**
     * @var string
     */
    protected $tableName;

    /**
     * @var array
     */
    protected $cache = array();

    /**
     * @var array
     */
    protected $multiples = array();

    /**
     * @var static
     */
    protected static $instance;

    /**
     * Shop_CT_Meta constructor.
     *
     * @param string $tablename
     */
    public function __construct($tablename)
    {
        $this->tableName = $tablename;
    }

    /**
     * @param $tableName
     * @return static
     */
    public static function getInstance($tableName)
    {
        $className = get_called_class();

        if (!static::$instance instanceof $className) {
            static::$instance = new static($tableName);
        }

        return static::$instance;
    }

    /**
     * Returns meta value from cache if exists, otherwise from database.
     *
     * @param int $itemId
     * @param string $key
     * @param bool $multiple
     *
     * @return mixed|null|string
     */
    public function get($itemId, $key, $multiple = false)
    {
        $itemId = absint($itemId);
        $key = sanitize_key($key);
        $multiple = (bool)$multiple;

        $cacheValue = $this->getFromCache($itemId, $key, $multiple);

        if (NULL !== $cacheValue) {
            return $cacheValue;
        }

        if ($multiple) {
            $this->multiples[$itemId][] = $key;
        }

        $this->cache[$itemId][$key] = $this->getFromDb($itemId, $key, $multiple);

        return $this->cache[$itemId][$key];
    }

    /**
     * Returns meta value from cache if exists.
     *
     * @param int $itemId
     * @param string $key
     * @param $multiple
     *
     * @return null
     */
    private function getFromCache($itemId, $key, $multiple)
    {
        if ($multiple) {
            if (isset($this->multiples[$itemId]) && in_array($key, $this->multiples[$itemId])) {
                return isset($this->cache[$itemId][$key]) ? $this->cache[$itemId][$key] : NULL;
            }

            return NULL;
        } else {
            if (isset($this->multiples[$itemId]) && in_array($key, $this->multiples[$itemId])) {
                return isset($this->cache[$itemId][$key][0]) ? $this->cache[$itemId][$key][0] : NULL;
            } elseif (isset($this->cache[$itemId][$key])) {
                return $this->cache[$itemId][$key];
            }

            return NULL;
        }
    }

    /**
     * @param int $item_id
     * @param string $key
     * @param bool $multiple
     *
     * @return array|null|object|string
     */
    private function getFromDb($item_id, $key, $multiple)
    {
        global $wpdb;

        if ($multiple) {
            $value = $wpdb->get_results('SELECT `value` FROM ' . $this->tableName . ' WHERE `item_id` = ' . $item_id . ' AND `key` = "' . $key . '" ORDER BY `date` DESC;');

            foreach ($value as &$item) {
                $unserializedValue = @unserialize($item);

                if (false !== $unserializedValue || 'b:0;' === $item) {
                    $item = $unserializedValue;
                }
            }

            unset($item);
        } else {
            $value = $wpdb->get_var('SELECT `value` FROM ' . $this->tableName . ' WHERE `item_id` = ' . $item_id . ' AND `key` = "' . $key . '" ORDER BY `date` DESC LIMIT 1;');

            $unserializedValue = @unserialize($value);

            if (false !== $unserializedValue || 'b:0;' === $value) {
                $value = $unserializedValue;
            }
        }

        return $value;
    }

    public function set($itemId, $key, $value)
    {
        $key = sanitize_text_field($key);
        // todo: sanitize value

        if (is_array($value) || is_object($value) || is_bool($value)) {
            $serialized_value = serialize($value);
        }

        global $wpdb;

        $result = $wpdb->insert($this->tableName, ['item_id' => $itemId, 'key' => $key, 'value' => isset($serialized_value) ? $serialized_value : $value]);

        if (false !== $result) {
            if (isset($this->multiples[$itemId]) && in_array($key, $this->multiples[$itemId])) {
                $this->setToCache($itemId, $key, $this->getFromDb($itemId, $key, true));
            } else {
                $this->setToCache($itemId, $key, $value);
            }

            return true;
        }

        return false;
    }

    /**
     * Adds meta to cache.
     *
     * @param int $itemId
     * @param string $key
     * @param mixed $value
     */
    private function setToCache($itemId, $key, $value)
    {
        $this->cache[$itemId][$key] = $value;
    }

    /**
     * Remove value from cache.
     *
     * @param int $itemId
     * @param string $key
     */
    private function deleteFromCache($itemId, $key)
    {
        if (isset($this->cache[$itemId], $this->cache[$itemId][$key])) {
            unset($this->cache[$itemId][$key]);
        }
    }

    /**
     * Delete meta from database and cache.
     *
     * @param int $itemId
     * @param string $key
     *
     * @return bool
     */
    public function delete($itemId, $key)
    {
        global $wpdb;

        $itemId = absint($itemId);
        $key = sanitize_key($key);

        $result = $wpdb->delete($this->tableName, ['item_id' => $itemId, 'key' => $key]);

        if ($result === false) {
            return false;
        }

        $this->deleteFromCache($itemId, $key);

        if (isset($this->multiples[$itemId]) && in_array($key, $this->multiples[$itemId])) {
            unset($this->multiples[$itemId][array_search($key, $this->multiples[$itemId])]);
        }

        return true;
    }

    public function setIfChanged($value, $id, $key)
    {
        return $value === $this->get($id, $key) ? false : $this->set($id, $key, $value);
    }
}