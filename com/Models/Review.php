<?php

namespace ShopCT\Models;


use WP_Comment;

class Review
{
    const COMMENT_TYPE = 'shop_ct_review';
    /**
     * @var int
     */
    public $id;
    /**
     * @var int
     */
    public $product_id;
    /**
     * @var string
     */
    public $author = '';

    /**
     * @var string
     */
    public $author_email = '';
    /**
     * @var string
     */
    public $author_url = '';
    /**
     * @var int
     */
    public $approved = 1;
    /**
     * @var int
     */
    public $parent = 0;
    /**
     * @var int
     */
    public $rating = 0;
    /**
     * @var string
     */
    public $content = '';
    /**
     * @var string $date
     */
    public $date;
    /**
     * @var WP_Comment
     */
    public $comment;
    /**
     * @var Product
     */
    public $product;

    /**
     * Shop_CT_Product_Review constructor.
     *
     * @param int|WP_Comment $id
     */
    public function __construct($id = NULL)
    {
        if (NULL !== $id && (is_numeric($id) || $id instanceof WP_Comment)) {
            if (is_numeric($id)) {
                $id = absint($id);

                $comment = get_comment($id);
            } else {
                $comment = $id;
                $id = $comment->comment_ID;
                $this->comment = $comment;
            }

            if (NULL !== $comment) {
                $this->id = $id;
                $this->product_id = $comment->comment_post_ID;
                $this->author = $comment->comment_author;
                $this->author_email = $comment->comment_author_email;
                $this->author_url = $comment->comment_author_url;
                $this->content = $comment->comment_content;
                $this->parent = $comment->comment_parent;
                $this->approved = $comment->comment_approved;
                $this->date = $comment->comment_date;
                $this->comment = $comment;

                $rating = get_comment_meta($comment->comment_ID, 'rating', true);

                if (!empty($rating)) {
                    $this->rating = $rating;
                }
            }
        }
    }

    /**
     * @param int|string $approved
     *
     * @return Review
     *
     * todo: the logic inside makes no sense
     */
    public function setApproved($approved)
    {
        if ('approve' === $approved || 1 === $approved || '1' === $approved) {
            $this->approved = '1';
        } elseif (in_array($approved, array('hold', 'trash', 'spam'))) {
            $this->approved = $approved;
        } elseif (0 === $approved || '0' === $approved) {
            $this->approved = '0';
        }

        return $this;
    }

    /**
     * @return bool|string
     */
    public function getStatus()
    {
        if ($this->approved == null)
            return false;
        elseif ($this->approved == '1')
            return 'approved';
        elseif ($this->approved == '0')
            return 'unapproved';
        elseif ($this->approved == 'spam')
            return 'spam';
        elseif ($this->approved == 'trash')
            return 'trash';
        else
            return false;
    }

    /**
     * @return bool
     */
    private function canBeSaved()
    {
        return isset($this->product_id, $this->author, $this->author_email, $this->content);
    }

    /**
     * @return bool|int
     */
    public function save()
    {
        if ($this->parent && NULL === $this->product_id) {
            $this->product_id = self::getProductIdByParentId($this->parent);
        }

        if (!$this->canBeSaved()) {
            return false;
        }

        $args = array(
            'comment_post_ID' => $this->product_id,
            'comment_author' => $this->author,
            'comment_author_email' => $this->author_email,
            'comment_author_url' => $this->author_url,
            'comment_approved' => $this->approved,
            'comment_content' => $this->content,
            'comment_date' => $this->date,
            'comment_parent' => $this->parent,
            'comment_type' => self::COMMENT_TYPE,
        );

        if (NULL === $this->id) {
            $result = wp_insert_comment($args);

            if ($result) {
                $this->id = $result;
            } else {
                return false;
            }
        } else {
            $args['comment_ID'] = $this->id;
            $result = wp_update_comment($args);
        }

        if ($result) {
            update_comment_meta($this->id, 'rating', $this->rating);
        }

        return $result;
    }

    /**
     * @param $parent_id
     * @return int
     */
    private static function getProductIdByParentId($parent_id)
    {
        $parent = get_comment($parent_id);

        return $parent->comment_post_ID;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        if ($this->parent && NULL === $this->product_id) {
            $this->product_id = self::getProductIdByParentId($this->parent);
        }

        return $this->product_id;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        if (null === $this->product) {
            $this->product = new Product($this->getProductId());
        }

        return $this->product;
    }

    /**
     * @return Review[]
     */
    public static function all()
    {
        $wp_comments = get_comments(['type' => self::COMMENT_TYPE, 'status' => 'any']);
        $comments = array();
        foreach ($wp_comments as $wp_comment) {
            $comments[] = new self($wp_comment);
        }

        return $comments;
    }

    /**
     * @param $id
     * @return bool
     */
    public static function delete($id)
    {
        return wp_delete_comment($id, true);
    }

    /**
     * @param $id
     * @param $status
     * @return bool|false|int
     */
    public static function changeStatus($id, $status)
    {
        $review = new self($id);

        return $review->setApproved($status)->save();
    }
}