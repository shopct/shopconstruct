<?php

namespace ShopCT\Models;


use ShopCT\Lib\Setter;

/**
 * Class ProductAttribute
 * @package ShopCT\Models
 *
 * todo: refactor
 */
class ProductAttribute
{
    use Setter;

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $slug;

    /**
     * @var string
     */
    public $type = 'text';

    /**
     * @var string
     */
    public $order_by = 'term_id';

    /**
     * @var int
     */
    private $public = 1;

    /**
     * @var ProductAttributeTerm[]|int|\WP_Error
     */
    public $terms = array();

    /**
     * constructor.
     *
     * @param null $id
     * @param array $args
     */
    public function __construct($id = NULL, $args = array())
    {
        $attribute = null;
        global $wpdb;
        if (null !== $id && is_numeric($id)) {


            $id = absint($id);
            $attribute = $wpdb->get_row('SELECT * FROM ' . self::getTableName() . ' WHERE id = ' . $id, ARRAY_A);


        } elseif (!empty($args)) {
            $where = '';
            if (isset($args['slug'])) {
                $where .= (empty($where) ? ' where ' : ' ') . 'slug="' . $args['slug'] . '"';
            }

            $attribute = $wpdb->get_row('SELECT * FROM ' . self::getTableName() . ' ' . $where, ARRAY_A);
        }


        if (null !== $attribute) {
            $this->id = null !== $id ? $id : $attribute['id'];
            $this->set($attribute);

            $term_ids = get_terms(array('taxonomy' => $this->slug, 'hide_empty' => false, 'fields' => 'ids'));

            foreach ($term_ids as $term_id) {
                $this->terms[$term_id] = new ProductAttributeTerm($term_id);
            }
        }
    }

    public static function getTableName()
    {
        return $GLOBALS['wpdb']->prefix . 'shop_ct_attributes';
    }

    public function addTerm(ProductAttributeTerm $term)
    {
        $term->setTaxonomy($this);
        if (null === $term->id) {
            $term->save();
        }
        $this->terms[$term->id] = $term;
    }

    private function canBeSaved()
    {
        return !empty($this->name);
    }

    public function save()
    {
        if (!$this->canBeSaved()) {
            return false;
        }

        global $wpdb;

        if(null === $this->id && empty($this->slug)){
            $this->slug = sanitize_title($this->name);
        }

        $data = array(
            'name' => $this->name,
            'slug' => $this->slug,
            'type' => $this->type,
            'order_by' => $this->order_by,
            'public' => $this->public,
        );

        $result = NULL === $this->id
            ? $wpdb->insert(self::getTableName(), $data)
            : $wpdb->update(self::getTableName(), $data, array('id' => $this->id));

        $term_result = array();
        if (false !== $result) {
            if (null === $this->id) {
                /**
                 * this action will handle taxonomy registration process. See:com\observers\common.observers.php
                 */
                do_action('shop_ct_new_attribute_created');
                $this->id = $wpdb->insert_id;
            }
            foreach ($this->terms as $term) {
                $term_result[] = $term->save();
            }
        }

        return true;
    }

    /**
     * @return array|null|object
     */
    public static function getAttributesWithoutTerms()
    {
        global $wpdb;

        return $wpdb->get_results('SELECT * FROM ' . self::getTableName());
    }


    /**
     * @return self[]
     */
    public static function getAll()
    {
        global $wpdb;

        $ids = $wpdb->get_results('SELECT id FROM ' . self::getTableName(), ARRAY_A);
        $attributes = array();

        foreach ($ids as $data) {
            $attributes[] = new self($data['id']);
        }

        return $attributes;
    }

    /**
     * @param $taxonomy
     * @return bool|ProductAttribute
     */
    public static function getByTaxonomy($taxonomy)
    {
        global $wpdb;
        $id = $wpdb->get_row($wpdb->prepare('SELECT id FROM ' . self::getTableName() . ' WHERE slug=%s LIMIT 1', $taxonomy));

        if (null !== $id) {
            return new ProductAttribute($id);
        }
        return false;
    }

    /**
     * @param $id
     * @return bool
     */
    public static function delete($id)
    {
        $id = absint($id);
        $taxonomy = $GLOBALS['wpdb']->get_var('SELECT slug FROM ' . self::getTableName() . ' WHERE id = ' . $id);

        if (NULL !== $taxonomy) {
            $term_ids = get_terms(array(
                'taxonomy' => $taxonomy,
                'fields' => 'ids',
                'hide_empty' => false,
            ));

            foreach ($term_ids as $term_id) {
                /*$result[$term_id] = */
                wp_delete_term($term_id, $taxonomy);
            }

            $result = $GLOBALS['wpdb']->query('DELETE FROM ' . self::getTableName() . ' WHERE id = ' . $id);
        }

        return isset($result) ? $result : false;
    }
}