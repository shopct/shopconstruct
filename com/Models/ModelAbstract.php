<?php

namespace ShopCT\Models;


use ShopCT\Core\Collection;
use ShopCT\Lib\Formatting;

abstract class ModelAbstract
{
    /**
     * @var string
     */
    protected static $table_name;
    /**
     * @var string
     */
    protected static $primary_key = 'id';
    /**
     * @var array database fields that need to be queried when creating instance
     */
    protected static $fields = array();
    /**
     * @var int
     */
    protected static $all_items_count;

    /**
     * To query the model from database use find() method
     * ModelAbstract constructor.
     * @param array $args
     */
    public function __construct($args = array())
    {
        if (!empty($args)) {
            foreach ($args as $key => $value) {
                $this->$key = $value;
            }
        }
    }

    public function __get($name)
    {
        if (in_array($name, static::$fields)) {
            $pascalCaseName = Formatting::snakeCaseToPascalCase($name);
            if (method_exists($this, 'get' . $pascalCaseName)) {
                return call_user_func(array($this, 'get' . $pascalCaseName));
            }
            return $this->$name;
        }
    }

    /**
     * @param array $args
     * @return static[]
     */
    public function get($args = array())
    {
        global $wpdb;
        $args = wp_parse_args($args, [
            'orderby' => 'Id',
            'order' => 'ASC',
            'per_page' => false,
            'paged' => 1,
            'search' => false,
            'search_target' => 'Name',
            'where' => array(),
            'where_operator' => 'AND',
        ]);
        $primaryKey = static::$primary_key;
        $TableName = static::getTableName();
        /** Count all items */
        if (null !== static::$all_items_count) {
            $count = static::$all_items_count;
        } else {
            static::$all_items_count = $count = $wpdb->get_var("select count(*) from " . static::getTableName());
        }
        if ($count == 0) return array();
        $paginate = '';
        $where = '';

        /* Pagination */
        if (false !== $args['per_page']) {
            $num = $args['per_page'] == '' ? $count : $args['per_page'];
            $page = $args['paged'];
            $total = intval(($count - 1) / $num) + 1;
            $page = intval($page);
            if (empty($page) or $page <= 0) $page = 1;
            if ($page > $total) $page = $total;
            $start = $page * $num - $num;
            $paginate = " LIMIT $start, $num";
        }

        if(!empty($args['where'])){
            $operator = !empty($args['where_operator']) ? $args['where_operator'] : 'AND';
            foreach($args['where'] as $where_key=>$where_value){
                $where .= ( $where === "" ? " WHERE ": " ".$operator." " ) . $where_key."='".$where_value."'" ;
            }
        }

        /* Search */
        if ($args['search'] != "") {
            // First, escape the search string for use in a LIKE statement.
            $search = $wpdb->esc_like($args['search']);
            // Add wildcards, since we are searching within text.
            $search = '%' . $search . '%';
            if($where === ''){
                $where = $wpdb->prepare(" WHERE %s LIKE %s", $args['search_target'], $search, $search);
            }else{
                $where .= $wpdb->prepare(" AND %s LIKE %s", $args['search_target'], $search, $search);
            }

        }

        /* Ordering */
        $ordering = " ORDER BY ".$args['orderby']." ".$args['order'];

        $fields = '`' . implode('`,`', static::$fields) . '`';

        /* The Query */
        $query = "SELECT ".$fields." FROM {$TableName}{$where}{$ordering}{$paginate}";
        /* And the main query to retrieve The Items */
        $items = $wpdb->get_results($query, ARRAY_A);

        /* Return actual objects */
        $ItemObjs = [];
        if (null !== $items) {
            foreach ($items as $item) {
                $ItemObjs[$item[$primaryKey]] = new static($item);
            }
        }

        return $ItemObjs;
    }


    /**
     * @return int
     */
    public static function getAllItemsCount(){
        if (null === static::$all_items_count) {
            global $wpdb;
            static::$all_items_count = $wpdb->get_var("select count(*) from " . static::getTableName());
        }

        return static::$all_items_count;
    }

    /**
     * @return string
     */
    public static function getTableName()
    {
        return $GLOBALS['wpdb']->prefix . static::$table_name;
    }

    /**
     * @param $primaryKeyValue
     * @return static
     */
    public static function find($primaryKeyValue)
    {
        $fields = '`' . implode('`,`', static::$fields) . '`';

        $dbResult = $GLOBALS['wpdb']->get_row('SELECT '. $fields .' FROM ' . static::getTableName() . ' WHERE ' . static::$primary_key . ' = ' . $primaryKeyValue, ARRAY_A);

        if(!empty($dbResult)) {
            return new static($dbResult);
        } else {
            return null;
        }
    }

    /**
     * Prepare data to be saved to database
     *
     * @param int $id
     * @param array $preferredData
     * @return array
     */
    protected function prepareSaveData($id = null, $preferredData=array())
    {
        $data = array();

        if(!empty($preferredData)){
            foreach($preferredData as $k=> $v){
                if($v!==null){
                    $data[$k]=$v;
                }
            }
            return $data;
        }

        if(empty(static::$fields)){
            return $data;
        }

        foreach(static::$fields as $fieldName){
            $fName = 'get'.$fieldName;

            if(method_exists($this,$fName)){
                $value = call_user_func(array($this,$fName));
                if($value !== null){
                    $data[$fieldName] = $value;
                }
            } else {
                $data[$fieldName] = $this->$fieldName;
            }
        }

        if($id !== null){
            $data['id'] = $id;
        }

        return $data;
    }


    /**
     * @param null $id
     * @return bool
     */
    public function save( $id = null )
    {
        global $wpdb;

        $key = static::$primary_key;

        $data = $this->prepareSaveData( $id );

        if ( null === $this->$key ) {
            $result = $wpdb->insert(static::getTableName(), $data);
        } else {
            $result = $wpdb->update(static::getTableName(), $data, array($key => $this->$key));
        }

        if(false !== $result){
            if(null === $this->$key) {
                $this->$key = $wpdb->insert_id;
            }
            return $this->$key;
        }

        return false;
    }

}