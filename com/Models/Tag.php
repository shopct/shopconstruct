<?php


namespace ShopCT\Models;

use ShopCT\Core\Term;

class Tag extends Term
{
    const SHOW_PER_PAGE = 10;
    public static $taxonomy = 'shop_ct_product_tag';
}