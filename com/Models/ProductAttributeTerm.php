<?php

namespace ShopCT\Models;


use Exception;
use ShopCT\Core\Term;

class ProductAttributeTerm extends Term
{
    /**
     * @var string
     */
    protected $taxonomy;

    public $attribute;

    /**
     * Shop_CT_Product_Attribute_Term constructor.
     *
     * @param int $id
     * @param null $taxonomy
     */
    public function __construct($id = NULL, $taxonomy = NULL)
    {
        if (NULL !== $id && is_numeric($id)) {
            $id = absint($id);

            $term = get_term($id, $taxonomy);

            if ($term instanceof \WP_Term) {
                $this->id = $id;
                $this->taxonomy = $term->taxonomy;
                $this->name = $term->name;
                $this->slug = $term->slug;
                $this->description = $term->slug;
            }
        } elseif (NULL !== $taxonomy && taxonomy_exists($taxonomy)) {
            $this->taxonomy = $taxonomy;
        }
    }

    /**
     * @param $attribute ProductAttribute
     */
    public function setTaxonomy($attribute)
    {
        $this->taxonomy = $attribute->slug;
        $this->attribute = $attribute;
    }

    /**
     * @return bool|ProductAttribute
     */
    public function getAttribute()
    {
        if (is_null($this->attribute)) {
            $this->attribute = ProductAttribute::getByTaxonomy($this->taxonomy);
        }
        return $this->attribute;
    }

    /**
     * @return bool
     */
    public function save()
    {
        $args = array(
            'description' => $this->description,
            'slug' => $this->slug,
        );

        if (NULL !== $this->id) {
            $args['name'] = $this->name;
        }

        $result = NULL === $this->id
            ? wp_insert_term($this->name, $this->taxonomy, $args)
            : wp_update_term($this->id, $this->taxonomy, $args);

        if (!$result || is_wp_error($result)) {
            var_dump($result);die;
            return false;
        } elseif (null === $this->id) {
            $this->id = $result['term_id'];
        }

        return true;
    }

    /**
     * @param array $args
     * @return static[]|null
     */
    public static function get($args = array())
    {
        $args = wp_parse_args($args, array(
            'taxonomy' => static::$taxonomy,
            'hide_empty' => true, //don't show empty terms
            'number' => 0, //get all,
            'orderby' => 'term_id',
            'order' => 'DESC'
        ));

        $terms = get_terms($args);

        if (empty($terms)) {
            return null;
        }

        $result = array();
        foreach ($terms as $term) {
            $result[] = new static($term->term_id);
        }

        return $result;
    }

    public function getBy($field, $value, $taxonomy)
    {

    }
}