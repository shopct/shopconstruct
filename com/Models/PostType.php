<?php

namespace ShopCT\Models;


use ShopCT\Lib\Formatting;
use WP_Query;

abstract class PostType
{
    /**
     * @var string
     */
    public static $post_type;

    /**
     * @var string[]
     */
    public static $post_params;

    /**
     * @var bool
     */
    public static $has_meta = false;

    /**
     * @var
     */
    public static $meta_keys;

    /**
     * @var Meta
     */
    public $meta;

    /**
     * @var int
     */
    public $id;

    /**
     * @var \WP_Post
     */
    public $post;

    /**
     * PostType constructor.
     * @param null $param
     */
    public function __construct($param = null)
    {
        if (null !== $param && is_numeric($param)) {
            /*$query = new WP_Query(array(
                'p' => $param,
                'post_type' => static::$postType,
                'post_status' => 'any'
            ));*/
            $post = \WP_Post::get_instance($param);

            if ($post instanceof \WP_Post) {
                $this->post = $post;
                $this->id = $post->ID;
            }

        }


        if (static::$has_meta) {
            if (!is_null($this->meta()) && !empty(static::$meta_keys) && !is_null($this->id)) {
                foreach (static::$meta_keys as $metaKey) {
                    $this->$metaKey = $this->meta()->get($this->id, $metaKey);
                }
            }
        }
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        if (in_array($name, static::$meta_keys)) {
            $pascalCaseName = Formatting::snakeCaseToPascalCase($name);
            if (method_exists($this, 'get' . $pascalCaseName)) {
                return call_user_func(array($this, 'get' . $pascalCaseName),$this->$name);
            }
            return $this->$name;
        } elseif (key_exists($name, static::$post_params)) {
            $pascalCaseName = Formatting::snakeCaseToPascalCase($name);
            if (method_exists($this, 'get' . $pascalCaseName)) {
                return call_user_func(array($this, 'get' . $pascalCaseName),$this->post->$name);
            }
            return $this->post->{static::$post_params[$name]};
        } else {
            $pascalCaseName = Formatting::snakeCaseToPascalCase($name);
            if (method_exists($this, 'get' . $pascalCaseName)) {
                return call_user_func(array($this, 'get' . $pascalCaseName),$this->$name);
            }
            return $this->$name;
        }
    }

    /**
     * @return Meta
     */
    abstract function metaInstance();

    /**
     * @return Meta
     */
    public function meta()
    {
        if (is_null($this->meta)) {
            $this->meta = $this->metaInstance();
        }

        return $this->meta;
    }

    /**
     * @return static[]
     */
    public static function all()
    {
        return static::get();
    }

    /**
     * @param array $args arguments for WP_Query
     * @return static[]
     */
    public static function get($args = array())
    {
        $args = wp_parse_args($args, array(
            'post_type' => static::$post_type,
            'posts_per_page' => -1,
            'post_status' => 'any'
        ));

        $query = new WP_Query($args);
        $objects = array();

        if ($query->have_posts()):
            $objectPosts = $query->get_posts();
            foreach ($objectPosts as $objectPost) {
                $objects[] = new static($objectPost->ID);
            }
        endif;

        return $objects;
    }

    /**
     * @throws \Exception
     * @return bool
     */
    public function save()
    {
        if (!empty(static::$post_params)) {
            $postData = array();

            foreach (static::$post_params as $param) {
                $postData[$param] = $this->post->$param;
            }

            $result = is_null($this->id)
                ? wp_insert_post($postData)
                : wp_update_post($this->post);

            if (!$result) {
                throw new \Exception('failed to save product');
            } elseif ($result && is_null($this->id)) {
                $this->id = $result;
            }

            if (static::$has_meta) {
                foreach (static::$meta_keys as $metaKey) {
                    if ($this->$metaKey !== $this->meta()->get($this->id, $metaKey)) {
                        $this->meta()->set($this->id, $metaKey, $this->$metaKey);
                    }
                }
            }

            return true;
        }

        return false;
    }

    public static function countPages($args = array())
    {
        $args = wp_parse_args($args, array(
            'post_type' => static::$post_type,
            'posts_per_page' => -1,
            'post_status' => 'any',
            'fields' => 'ids',
        ));

        $query = new WP_Query($args);
        return $query->max_num_pages;
    }

    public static function countAll()
    {
        $result = $GLOBALS['wpdb']->get_var('select count(*) from ' . $GLOBALS['wpdb']->posts . ' where post_type=\'' . static::$post_type . '\' and post_status<>\'auto-draft\'');
        return $result;
    }
}