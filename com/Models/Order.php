<?php

namespace ShopCT\Models;


/**
 * Class Order
 * @package ShopCT\Models
 *
 * @property int $customer Customer id
 * @property string $date Ordered date, format:Y-m-d H:i:s
 * shipping_first_name
 * @property string $shipping_last_name
 * @property string $shipping_company
 * @property string $shipping_address_1
 * @property string $shipping_address_2
 * @property string $shipping_country
 * @property string $shipping_city
 * @property string $shipping_postcode
 * @property string $shipping_state
 * @property string $shipping_customer_note
 * @property string $billing_first_name
 * @property string $billing_last_name
 * @property string $billing_company
 * @property string $billing_address_1
 * @property string $billing_address_2
 * @property string $billing_country
 * @property string $billing_city
 * @property string $billing_postcode
 * @property string $billing_state
 * @property string $billing_email
 * @property string $billing_phone
 * @property string $payment_method
 * @property string $transaction_id
 * @property string $notes
 */
class Order extends PostType
{
    const SHOW_PER_PAGE = 5;
    /**
     * @var string
     */
    public static $post_type = 'shop_ct_order';
    public static $post_params = array(
        'status' => 'post_status',
        'post_title' => 'post_title',
        'post_date' => 'post_date',
        'post_date_gmt' => 'post_date_gmt',
        'post_author' => 'post_author',
    );
    /**
     * @var bool
     */
    public static $has_meta = true;
    /**
     * @var array
     */
    public static $meta_keys = array(
        'shipping_first_name',
        'shipping_last_name',
        'shipping_company',
        'shipping_address_1',
        'shipping_address_2',
        'shipping_country',
        'shipping_city',
        'shipping_postcode',
        'shipping_state',
        'shipping_customer_note',
        'billing_first_name',
        'billing_last_name',
        'billing_company',
        'billing_address_1',
        'billing_address_2',
        'billing_country',
        'billing_city',
        'billing_postcode',
        'billing_state',
        'billing_email',
        'billing_phone',
        'payment_method',
        'transaction_id',
        'customer',
        'date',
        'notes',
    );
    /**
     * array(
     *  'cost' => float,
     *  'object' => Product,
     *  'quantity' => int,
     * )
     *
     *
     * @var array|mixed
     */
    public $products = array();
    /**
     * @var array
     */
    private $removed_products = array();
    private $changed_products = array();
    private $added_products = array();
    
    /**
     * @return OrderMeta
     */
    public function metaInstance()
    {
        return OrderMeta::getInstance($GLOBALS['wpdb']->prefix . 'shop_ct_order_meta');
    }

    /**
     * @return array
     */
    public static function getAvailableStatuses()
    {
        return array(
            'shop-ct-pending'    => __( 'Pending Payment', 'shop_ct' ),
            'shop-ct-processing' => __( 'Processing', 'shop_ct' ),
            'shop-ct-on-hold'    => __( 'On Hold', 'shop_ct' ),
            'shop-ct-completed'  => __( 'Completed', 'shop_ct' ),
            'shop-ct-cancelled'  => __( 'Cancelled', 'shop_ct' ),
            'shop-ct-refunded'   => __( 'Refunded', 'shop_ct' ),
            'shop-ct-failed'     => __( 'Failed', 'shop_ct' ),
        );
    }

    /**
     * @return array
     */
    public function getProducts()
    {
        if(empty($this->products)) {
            $this->products = $this->getProductsFromDB();
        }

        return $this->products;
    }

    /**
     * @param Product $product
     * @param int $quantity
     * @param null $cost
     * @return bool
     */
    public function addProduct(Product $product, $quantity = 1, $cost = null)
    {
        if ($cost === null) {
           $cost = $product->getPrice();
        }

        if (isset($this->products[$product->id])) {
           $this->products[$product->id]['quantity'] += $quantity;
           $this->products[$product->id]['cost'] = $cost;
           $this->changed_products[] = $product->id;
        } else {
            $this->products[$product->id] = array(
                'object' => $product,
                'cost' => $cost,
                'quantity' => $quantity
            );
            $this->added_products[] = $product->id;
        }

        return true;
    }

    /**
     * @param Product $product
     * @return bool
     */
    public function removeProduct(Product $product)
    {
        if (!isset($this->products[$product->id])) {
           return false;
        }

        unset($this->products[$product->id]);
        $this->removed_products[] = $product->id;

        return true;
    }

    /**
     * @return float|int
     */
    public function getShippingCost()
    {
        if (null !== $this->shipping_cost) {
            return $this->shipping_cost;
        }
        if ($this->requiresDelivery()) {
            $zone = ShippingZone::getZoneByLocation($this->shipping_country);
            $is_in_zone = $zone instanceof ShippingZone;
            if ($is_in_zone) {
                return $this->shipping_cost = $zone->cost;
            } elseif (ShippingZone::restOfTheWorldEnabled()) {
                $zone = ShippingZone::find(1);
                return $this->shipping_cost = $zone->cost;
            } else {
                return $this->shipping_cost = 0;
            }
        }
        return $this->shipping_cost = 0;
    }

    /**
     * If order requires delivery. True if at least 1 product in order needs delivery.
     *
     * @return bool
     */
    public function requiresDelivery()
    {
        foreach ($this->products as $product) {
            /** @var Product $productObject */
            $productObject = $product['object'];
            if ($productObject->needsShipping()) {
                return true;
            }
        }
        return false;
    }

    public function getProductsFromDB()
    {
        global $wpdb;
        $rows = $wpdb->get_results('SELECT product_id, quantity, cost FROM ' . self::getOrderProductsTableName() . ' WHERE order_id = ' . $this->id);
        $products = array();
        foreach ($rows as $row) {
            $product = new Product($row->product_id);
            $products[$product->id]['cost'] = $row->cost;
            $products[$product->id]['object'] = $product;
            $products[$product->id]['quantity'] = $row->quantity;
        }
        return $products;
    }

    public function getOrderProductsTableName()
    {
        return $GLOBALS['wpdb']->prefix.'shop_ct_order_products';
    }

    public function getProductsTotalCost()
    {
        $products = $this->getProducts();

        $total = 0;

        foreach ($products as $product) {
            $total += $product['cost'] * $product['quantity'];
        }

        return $total;
    }

    public function getTotal()
    {
        $productsTotal = $this->getProductsTotalCost();
        $shippingTotal = $this->getShippingCost();

        return $productsTotal + $shippingTotal;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function save()
    {
        $saved = parent::save();

        if ($saved) {
            global $wpdb;

            if(!empty($this->removed_products)){
                foreach ($this->removed_products as $removedProductId) {
                    $wpdb->delete(self::getOrderProductsTableName(), array('order_id' => $this->id, 'product_id' => $removedProductId));
                }
            }

            if (!empty($this->added_products)) {
                foreach ($this->added_products As $addedProductId) {
                    $wpdb->insert(self::getOrderProductsTableName(), array(
                        'order_id' => $this->id,
                        'product_id' => $addedProductId,
                        'cost' => $this->products[$addedProductId]['cost'],
                        'quantity' => $this->products[$addedProductId]['quantity'],
                    ));
                }
            }

            if (!empty($this->changed_products)) {
               foreach($this->changed_products as $changedProductId) {
                   $wpdb->update(self::getOrderProductsTableName(), array(
                      'quantity' => $this->products[$changedProductId]['quantity'],
                      'cost' => $this->products[$changedProductId]['cost'],
                   ), array(
                       'order_id', $this->id,
                       'product_id' => $changedProductId
                   ));
               }
            }

        } else {
            return $saved;
        }

        return true;
    }
}