<?php
/**
 * Created by PhpStorm.
 * User: Rose
 * Date: 31-May-18
 * Time: 10:15 PM
 */

namespace ShopCT\Models\Settings;


class ProductSettings extends SettingsAbstract
{
    public static $data = array(
        'weight_unit' => 'kg',
        'dimension_unit' => 'm',
        'enable_review_rating' => 'yes',
        'review_rating_required' => 'yes',
//        'review_rating_verification_label' => 'yes',
//        'review_rating_verification_required' => 'yes',
        'manage_stock' => 'yes',
//        'stock_email_recipient' => '',
        'notify_low_stock_amount' => '2',
        'notify_no_stock_amount' => '0',
        'hide_out_of_stock_items' => 'no',
        'stock_format' => '',
        'file_download_method' => 'force',
        'downloads_require_login' => 'no',
        'downloads_grant_access_after_payment' => 'yes'
    );

    public function getNotifyNoStock($value)
    {
        return (int)$value;
    }

    public function getNotifyLowStockAmount($value)
    {
        return (int)$value;
    }
}