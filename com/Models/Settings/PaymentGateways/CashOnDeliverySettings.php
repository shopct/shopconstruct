<?php

namespace ShopCT\Models\Settings\PaymentGateways;


use ShopCT\Models\Settings\SettingsAbstract;

class CashOnDeliverySettings extends SettingsAbstract
{
    public static $id = 'cod';
    public static $data = array(
        'enabled' => 'no',
    );
}