<?php

namespace ShopCT\Models\Settings\PaymentGateways;


use ShopCT\Models\Settings\SettingsAbstract;

class PayPalSettings extends SettingsAbstract
{
    public static $id = 'paypal';
    public static $data = array(
        'enabled' => 'no',
    );
}