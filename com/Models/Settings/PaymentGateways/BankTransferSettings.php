<?php

namespace ShopCT\Models\Settings\PaymentGateways;


use ShopCT\Models\Settings\SettingsAbstract;

class BankTransferSettings extends SettingsAbstract
{
    public static $id = 'bacs';
    public static $data = array(
        'enabled' => 'no',
    );
}