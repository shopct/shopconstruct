<?php

namespace ShopCT\Models\Settings;


use ShopCT\Lib\Formatting;

class SettingsAbstract
{
    public static $pluginId = 'shop_ct';
    public static $id;
    protected static $data = array();

    /**
     * @var static
     */
    protected static $_instance;


    /**
     *
     * @return static
     */
    public static function instance()
    {
        $className = static::getClassName();
        if (!(self::$_instance instanceof $className)) {
            self::$_instance = new $className();
        }

        return self::$_instance;
    }

    final protected static function getClassName()
    {
        return get_called_class();
    }

    public function __get($key)
    {
        if (array_key_exists($key, static::$data)) {
            if (static::$id) {
                $this->$key = get_option(static::getOptionName($key), static::$data[$key]);
            } else {
                $this->$key = get_option(static::getOptionName($key), static::$data[$key]);
            }
        }

        $pascalCaseName = Formatting::snakeCaseToPascalCase($key);

        if (method_exists($this, 'get' . $pascalCaseName)) {
            return call_user_func(array($this, 'get' . $pascalCaseName));
        }

        return $this->$key;
    }

    public static function getOptionName($key)
    {
        if (static::$id) {
            return static::$pluginId . "_" . static::$id . "_" . $key;
        } else {
            return static::$pluginId . "_" . $key;
        }
    }

    public function save()
    {
        foreach (static::$data as $key=>$defaultValue) {
            $optionName = static::getOptionName($key);

            if (get_option($optionName) !== false) {
                // The option already exists, so update it.
                update_option($key, $this->$key);
            } else {
                add_option($key, $this->$key);
            }

        }

        return true;
    }
}