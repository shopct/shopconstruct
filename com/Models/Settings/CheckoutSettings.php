<?php

namespace ShopCT\Models\Settings;


class CheckoutSettings extends SettingsAbstract
{
    public static $data = array(
        'enable_guest_checkout' => 'yes',
        'force_ssl_checkout' => 'no',
        'checkout_page_id' => null,
        'terms_page_id' => null,
//        'checkout_pay_endpoint' => 'order-pay',
//        'checkout_order_received_endpoint' => 'order-received',
    );
}