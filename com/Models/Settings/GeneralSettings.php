<?php

namespace ShopCT\Models\Settings;


class GeneralSettings extends SettingsAbstract
{
    protected static $data = array(
        'base_country' => '',
        'allowed_countries' => 'all',
        'specific_ship_to_countries' => '',
        'default_customer_address' => 'geolocation',
        'currency' => 'USD',
        'currency_pos' => 'left',
        'price_thousand_sep' => ',',
        'price_decimal_sep' => '.',
        'price_num_decimals' => 2,
        'custom_css' => ''
    );
}