<?php

namespace ShopCT\Models;

/**
 * Class ShippingZone
 * @package ShopCT\Models
 *
 * @property int $id
 * @property string $name
 * @property string $status
 * @property float $cost
 */
class ShippingZone extends ModelAbstract
{
    protected static $table_name = 'shop_ct_shipping_zones';

    protected static $fields = array(
        'id',
        'name',
        'status',
        'cost'
    );

    public static function getCountriesTableName() {
        return $GLOBALS['wpdb']->prefix . 'shop_ct_shipping_zone_countries';
    }

    /**
     * @param $code
     *
     * @return bool|ShippingZone
     */
    public static function getZoneByLocation($code) {
        if (2 !== strlen($code)) {
            return false;
        }

        global $wpdb;

        $zones = self::getTableName();
        $counties = self::getCountriesTableName();

        $pairs = $wpdb->get_results("SELECT `id`, `country_iso_code`, `name` , `status` FROM $zones INNER JOIN $counties ON $zones.id = $counties.zone_id");

        foreach ($pairs as $pair) {
            if ($pair->country_iso_code === $code && absint($pair->status) === 1 ) {
                return ShippingZone::find($pair->id);
            }
        }

        return (self::restOfTheWorldEnabled() ? ShippingZone::find(1) : false);
    }

    /**
     * @return bool
     */
    public static function restOfTheWorldEnabled() {
        return (bool)$GLOBALS['wpdb']->get_var('SELECT status FROM ' . self::getTableName() . ' WHERE id = 1');
    }
}