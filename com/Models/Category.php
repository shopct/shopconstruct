<?php

namespace ShopCT\Models;


use ShopCT\Core\Term;

class Category extends Term
{
    const SHOW_PER_PAGE = 10;
    /**
     * @var string
     */
    public static $taxonomy = 'shop_ct_product_category';
    /**
     * @var string
     */
    private static $thumbnail_id_key = 'thumbnail_id';
    /**
     * @var int
     */
    public $thumbnail_id;
    /**
     * @var string
     */
    private $thumbnail_url;

    /**
     * Category constructor.
     * @param int $id
     */
    public function __construct($id = null)
    {
        parent::__construct($id);

        if (null !== $this->id) {
            $this->thumbnail_id = get_term_meta($this->id, static::$thumbnail_id_key, true);
        }

    }

    /**
     * @param string $size
     * @param bool $placeholder
     * @return mixed
     */
    public function getThumbnailUrl($size = 'thumbnail', $placeholder = false)
    {
        if (null === $this->thumbnail_url) {
            if (!empty($this->thumbnail_id)) {
                $this->thumbnail_url = wp_get_attachment_image_src($this->thumbnail_id, $size)[0];
            } else if ($placeholder) {
                return SHOP_CT_PUBLIC_URL . "images/placeholder.png";
            }

        }

        return $this->thumbnail_url;
    }

    public function hasThumbnail()
    {
        return !empty($this->getThumbnailUrl());
    }

    /**
     * @return bool
     */
    public function save()
    {
        $result = parent::save();
        if (!is_wp_error($result) && !empty($this->thumbnail_id)) {
            $this->id = $result['term_id'];
            $result = update_term_meta($this->id, static::$thumbnail_id_key, $this->thumbnail_id);

            return $result;
        } else {
            return $result;
        }
    }

    public function getShortcode()
    {
        return '[ShopConstruct_category id="' . $this->id . '"]';
    }
}