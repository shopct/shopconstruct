<?php
/* Database Migration */
shopCTEventManager()->add('init', '\ShopCT\Database\Migrations\DatabaseMigration@run');

/* Register Post Types */
shopCTEventManager()->add('init', 'PostTypesController@register');

//shopCTEventManager()->add('init', 'PostTypesController@supportJetpackOmnisearch');

/* Register Taxonomies */
shopCTEventManager()->add('init', 'TaxonomiesController@register');

/* Taxonomies */
shopCTEventManager()->add('shop_ct_new_attribute_created', 'TaxonomiesController@registerAttributes');


/* Products */
shopCTEventManager()->add('wp_ajax_shop_ct_get_products_page', 'ProductsController@index');
shopCTEventManager()->add('wp_ajax_shop_ct_edit_product', 'ProductsController@edit');
shopCTEventManager()->add('wp_ajax_shop_ct_update_product', 'ProductsController@update');
shopCTEventManager()->add('wp_ajax_shop_ct_trash_product', 'ProductsController@trash');
shopCTEventManager()->add('wp_ajax_shop_ct_delete_product', 'ProductsController@destroy');
shopCTEventManager()->add('wp_ajax_shop_ct_restore_product', 'ProductsController@restore');


/* Categories */
shopCTEventManager()->add('wp_ajax_shop_ct_get_categories_page', 'CategoriesController@index');
shopCTEventManager()->add('wp_ajax_shop_ct_edit_category', 'CategoriesController@edit');
shopCTEventManager()->add('wp_ajax_shop_ct_update_category', 'CategoriesController@update');
shopCTEventManager()->add('wp_ajax_shop_ct_delete_category', 'CategoriesController@destroy');

shopCTEventManager()->add('wp_ajax_shop_ct_get_category_checklist', 'CategoriesController@getChecklist');

/* Tags */
shopCTEventManager()->add('wp_ajax_shop_ct_get_tags_page', 'TagsController@index');
shopCTEventManager()->add('wp_ajax_shop_ct_edit_tag', 'TagsController@edit');
shopCTEventManager()->add('wp_ajax_shop_ct_update_tag', 'TagsController@update');

/* Reviews */
shopCTEventManager()->add('wp_ajax_shop_ct_get_reviews_page', 'ReviewsController@index');
shopCTEventManager()->add('wp_ajax_shop_ct_update_review', 'ReviewsController@update');
shopCTEventManager()->add('wp_ajax_shop_ct_delete_review', 'ReviewsController@destroy');

/* Attributes */
shopCTEventManager()->add('wp_ajax_shop_ct_get_attributes_page', 'AttributesController@index');
shopCTEventManager()->add('wp_ajax_shop_ct_edit_product_attribute', 'AttributesController@editForProduct');
shopCTEventManager()->add('wp_ajax_shop_ct_edit_attribute', 'AttributesController@edit');
shopCTEventManager()->add('wp_ajax_shop_ct_update_attribute', 'AttributesController@update');

/* Orders */
shopCTEventManager()->add('wp_ajax_shop_ct_edit_order', 'OrdersController@edit');
shopCTEventManager()->add('wp_ajax_shop_ct_update_order', 'OrdersController@update');
shopCTEventManager()->add('wp_ajax_shop_ct_get_orders_page', 'OrdersController@index');