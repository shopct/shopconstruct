<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}


/**
 * todo: move to customer model
 */
function shopCTGetCustomersForSelectbox()
{
    $sql = "SELECT id, user_nicename, user_email FROM " . $GLOBALS['wpdb']->users;
    $nameEmail = array(
        0 => 'Guest'
    );
    $result = $GLOBALS['wpdb']->get_results($sql);

    foreach ($result as $key => $object) {
        $nameEmail[$object->id] = $object->user_nicename . ' (' . $object->user_email . ')';
    }

    return $nameEmail;
}