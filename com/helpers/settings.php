<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}


/**
 * @return \ShopCT\Models\Settings\GeneralSettings
 */
function shopCTGeneralSettings()
{
    return \ShopCT\Models\Settings\GeneralSettings::instance();
}

/**
 * @return \ShopCT\Models\Settings\ProductSettings
 */
function shopCTProductSettings()
{
    return \ShopCT\Models\Settings\ProductSettings::instance();
}

/**
 * @return \ShopCT\Models\Settings\CheckoutSettings
 */
function shopCTCheckoutSettings()
{
    return \ShopCT\Models\Settings\CheckoutSettings::instance();
}
