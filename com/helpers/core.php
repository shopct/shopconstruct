<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}


/**
 * @return \ShopCT\Core\EventManager
 */
function shopCTEventManager()
{
    return \ShopCT\Core\EventManager::instance();
}

/**
 * @param $view
 * @param $params
 * @return string
 */
function shopCTView($view, $params = array())
{
    return \ShopCT\Core\View::get($view, $params);
}

/**
 * @param $view
 * @param $params
 */
function shopCTViewRender($view, $params = array())
{
    echo \ShopCT\Core\View::get($view, $params);
}

/**
 * todo: remove in future, when we have customer model
 */
require_once __DIR__ . DIRECTORY_SEPARATOR . 'customer.php';