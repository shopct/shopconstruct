<?php
/**
 * @var $attribute \ShopCT\Models\ProductAttribute
 */

?>
<form id="shop-ct-attribute-popup-form" action="#" method="post">
    <?php wp_nonce_field('shop_ct', 'nonce'); ?>
    <?php if (!empty($attribute->id)): ?>
        <input type="hidden" id="attribute_id" name="attribute_id" value="<?php echo $attribute->id; ?>"/>
    <?php endif; ?>
    <input type="hidden" name="action" value="shop_ct_update_attribute"/>
    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input name="name" <?php if (null !== $attribute->id) {
            echo 'readonly="readonly"';
        } ?> id="attribute-name" value="<?= $attribute->name; ?>"/>
        <label for="attribute-name"><?php _e('Name', 'shop_ct'); ?></label>
        <span></span>
    </div>
    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input name="slug" <?php if (null !== $attribute->id) {
            echo 'readonly="readonly"';
        } ?> id="attribute-slug" value="<?= $attribute->slug; ?>"/>
        <label for="attribute-slug"><?php _e('Slug', 'shop_ct'); ?></label>
        <span></span>
    </div>
    <h4><?php _e('Attribute Terms', 'shop_ct'); ?><a href="#"
                                                     class="shop-ct-add-new-attribute-term shop-ct-margin-left-15  shop-ct-mat-button shop-ct-mat-button--raised shop-ct-mat-button--secondary"><i
                    class="material-icons">add</i></a></h4>
    <table class="widefat striped">
        <thead>
        <tr>
            <th><?php _e('Term', 'shop_ct'); ?></th>
            <th><?php _e('Actions', 'shop_ct'); ?></th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <td><?php _e('Term', 'shop_ct'); ?></td>
            <td><?php _e('Actions', 'shop_ct'); ?></td>
        </tr>
        </tfoot>
        <tbody id="shop-ct-attribute-terms"
               data-empty-row="<?php echo esc_attr(\ShopCT\Core\View::get('admin/attributes/edit/attribute-term-row-empty')); ?>"
               data-row="<?php echo esc_attr(\ShopCT\Core\View::get('admin/attributes/edit/attribute-term-row')); ?>">
        <?php
        if (!empty($attribute->terms)):
            foreach ($attribute->terms as $term):
                echo \ShopCT\Core\View::get('admin/attributes/edit/attribute-term-row', compact('term'));
            endforeach;
        else:
            echo \ShopCT\Core\View::get('admin/attributes/edit/attribute-term-row-empty');
        endif; ?>
        </tbody>
    </table>
</form>