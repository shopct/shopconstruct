<?php
/**
 * @var $term \ShopCT\Models\ProductAttributeTerm
 */
if (!isset($term)) {
    $term = new \ShopCT\Models\ProductAttributeTerm();
}
?>
<tr>
    <td>
        <input type="text" name="attribute-terms[]" <?php if (null !== $term->id) {
            echo 'readonly="readonly"';
        } ?> placeholder="<?php _e('Name', 'shop_ct'); ?>"
               value="<?php echo $term->name; ?>"/>
    </td>
    <td>
        <a href="#" target="_blank"
           class="shop-ct-mat-button shop-ct-mat-button--secondary shop-ct-remove-attribute-term <?php if (null !== $term->id) {
               echo 'shop-ct-disabled';
           } ?>"
           title="<?php _e('Remove', 'shop_ct'); ?>">
            <i class="material-icons">delete_forever</i>
        </a>
    </td>
</tr>