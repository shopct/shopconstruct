<?php
/**
 * @var $attributes \ShopCT\Models\ProductAttribute[]
 */

foreach ($attributes as $attribute):
    $termNames = array_map(function (\ShopCT\Models\ProductAttributeTerm $term) {
        return $term->name;
    }, $attribute->terms);

    ?>
    <tr data-attribute-id="<?php echo $attribute->id; ?>">
        <td data-title="<?php _e('ID', 'shop_ct'); ?>"><?php echo $attribute->id ?></td>
        <td data-title="<?php _e('Name', 'shop_ct'); ?>">
            <a href="#" class="shop-ct-edit-attribute"><?php echo $attribute->name; ?></a>
        </td>
        <td data-title="<?php _e('Slug', 'shop_ct'); ?>"><?php echo $attribute->slug; ?></td>
        <td data-title="<?php _e('Terms', 'shop_ct'); ?>">
            <?php echo implode(', ', $termNames); ?>
        </td>
        <td data-title="<?php _e('Actions', 'shop_ct'); ?>">
            <a href="#" target="_blank"
               class="shop-ct-mat-button shop-ct-mat-button--secondary shop-ct-edit-attribute"
               title="<?php _e('Edit', 'shop_ct'); ?>">
                <i class="material-icons">edit</i>
            </a>
            <a href="#" class="shop-ct-mat-button shop-ct-mat-button--secondary shop-ct-remove-attribute <?php if(!empty($attribute->terms)) { echo 'shop-ct-disabled'; } ?>"
               title="<?php _e('Remove', 'shop_ct'); ?>">
                <i class="material-icons">delete</i>
            </a>
        </td>
    </tr>
<?php endforeach;