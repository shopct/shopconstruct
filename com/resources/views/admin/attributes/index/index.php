<?php
/**
 * @var $attributes \ShopCT\Models\ProductAttribute[]
 */
?>

<div id="shop-ct-attributes">

    <?php
    shopCTViewRender('admin/common/resource/header', array(
        'showSearch' => false,
        'showFixedAction' => true,
        'showAddNewButton' => true,
        'addNewClassName' => 'shop-ct-add-new-attribute',
        'fixedActionClassName' => 'shop-ct-add-new-attribute',
        'title' => __('Product Attrbiutes', 'shop_ct'),
    ));
    ?>

    <div id="shop-ct-attributes-list-wrap" class="shop-ct-mat-table-responsive-vertical shop-ct-shadow-z-1">
        <table class="shop-ct-mat-table shop-ct-mat-table-striped shop-ct-mat-table-condensed shop-ct-mat-table-responsive-vertical">
            <thead>
            <tr>
                <th><?php _e('ID', 'shop_ct'); ?></th>
                <th><?php _e('Name', 'shop_ct'); ?></th>
                <th><?php _e('Slug', 'shop_ct'); ?></th>
                <th><?php _e('Terms', 'shop_ct'); ?></th>
                <th><?php _e('Actions', 'shop_ct'); ?></th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <td><?php _e('ID', 'shop_ct'); ?></td>
                <td><?php _e('Name', 'shop_ct'); ?></td>
                <td><?php _e('Slug', 'shop_ct'); ?></td>
                <td><?php _e('Terms', 'shop_ct'); ?></td>
                <td><?php _e('Actions', 'shop_ct'); ?></td>
            </tr>
            </tfoot>
            <tbody>
            <?php
            if (empty($attributes)):
                echo '<tr><td colspan="7">' . __('no items', 'shop_ct') . '</td></tr>';
            else:
                shopCTViewRender('admin/attributes/index/items', compact('attributes'));
            endif;
            ?>
            </tbody>
        </table>
    </div>
</div>
