<?php
/**
 * @var $tag \ShopCT\Models\Tag
 */

?>
<form id="shop-ct-tag-popup-form" action="#" method="post">
    <?php wp_nonce_field('shop_ct', 'nonce'); ?>
    <?php if (!empty($tag->id)): ?>
        <input type="hidden" id="tag_id" name="tag_id" value="<?php echo $tag->id; ?>"/>
    <?php endif; ?>
    <input type="hidden" name="action" value="shop_ct_update_tag"/>
    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input name="name" id="tag-name" value="<?= $tag->name; ?>"/>
        <label for="tag-name"><?php _e('Name', 'shop_ct'); ?></label>
        <span></span>
    </div>
    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input name="slug" id="tag-slug" value="<?= $tag->slug; ?>"/>
        <label for="tag-slug"><?php _e('Slug', 'shop_ct'); ?></label>
        <span></span>
    </div>
    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
            <textarea id="tag-description"
                      name="description"><?= $tag->description; ?></textarea>
        <label for="tag-description"><?php _e('Description', 'shop_ct'); ?></label>
        <span></span>
    </div>
</form>