<?php
/**
 * @var $tags \ShopCT\Models\Tag
 * @var $paged int
 * @var $totalCount int
 */
?>
<div id="shop-ct-tags">
    <?php
    shopCTViewRender('admin/common/resource/header', array(
        'showSearch' => true,
        'title' => __('Tags', 'shop_ct'),
        'addNewClassName' => 'shop-ct-add-new-tag',
        'showFixedAction' => true,
        'fixedActionClassName' => 'shop-ct-add-new-tag'
    ));
    ?>

    <div id="shop-ct-tags-list-wrap" class="shop-ct-mat-table-responsive-vertical shop-ct-shadow-z-1">
        <table class="shop-ct-mat-table shop-ct-mat-table-striped shop-ct-mat-table-condensed shop-ct-mat-table-responsive-vertical">
            <thead>
            <tr>
                <th><?php _e('ID', 'shop_ct'); ?></th>
                <th><?php _e('Title', 'shop_ct'); ?></th>
                <th><?php _e('Count', 'shop_ct'); ?></th>
                <th><?php _e('Actions', 'shop_ct'); ?></th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <td><?php _e('ID', 'shop_ct'); ?></td>
                <td><?php _e('Title', 'shop_ct'); ?></td>
                <td><?php _e('Count', 'shop_ct'); ?></td>
                <td><?php _e('Actions', 'shop_ct'); ?></td>
            </tr>
            </tfoot>
            <tbody>
            <?php
            if (empty($tags)):
                echo '<tr><td colspan="7">' . __('no items', 'shop_ct') . '</td></tr>';
            else:
                shopCTViewRender('admin/tags/index/items', compact('tags'));
            endif;
            ?>
            </tbody>
        </table>
    </div>

</div>
