<?php
/**
 * @var $tags \ShopCT\Models\Category[]
 */
foreach ($tags as $tag): ?>
    <tr data-category-id="<?php echo $tag->id; ?>">
        <td data-title="<?php _e('ID', 'shop_ct'); ?>"><?php echo $tag->id ?></td>
        <td data-title="<?php _e('Title', 'shop_ct'); ?>">
            <a href="#" class="shop-ct-edit-category"><?php echo $tag->name ?></a>
        </td>
        <td data-title="<?php _e('Count', 'shop_ct'); ?>"><?php echo $tag->term->count; ?></td>
        <td data-title="<?php _e('Actions', 'shop_ct'); ?>">
            <a href="#"
               class="shop-ct-mat-button shop-ct-mat-button--secondary shop-ct-edit-tag"
               title="<?php _e('Edit', 'shop_ct'); ?>">
                <i class="material-icons">edit</i>
            </a>
            <a href="<?php echo $tag->getPermalink(); ?>" target="_blank"
               class="shop-ct-mat-button shop-ct-mat-button--secondary"
               title="<?php _e('View', 'shop_ct'); ?>">
                <i class="material-icons">remove_red_eye</i>
            </a>
            <a href="#" class="shop-ct-mat-button shop-ct-mat-button--secondary shop-ct-remove-tag"
               title="<?php _e('Remove', 'shop_ct'); ?>">
                <i class="material-icons">delete</i>
            </a>
        </td>
    </tr>
<?php endforeach;