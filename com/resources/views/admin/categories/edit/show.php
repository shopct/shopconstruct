<?php
/**
 * @var $category \ShopCT\Models\Category
 */
$thumbnail = $category->getThumbnailUrl('full');
$hasThumbnail = !empty($thumbnail);

?>
<form id="shop-ct-category-popup-form" action="#" method="post">
    <?php wp_nonce_field('shop_ct', 'nonce'); ?>
    <?php if (!empty($category->id)): ?>
        <input type="hidden" id="category_id" name="category_id" value="<?php echo $category->id; ?>"/>
    <?php endif; ?>
    <input type="hidden" name="action" value="shop_ct_update_category"/>
    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input name="name" id="category-name" value="<?= $category->name; ?>"/>
        <label for="category-name"><?php _e('Name', 'shop_ct'); ?></label>
        <span></span>
    </div>
    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input name="slug" id="category-slug" value="<?= $category->slug; ?>"/>
        <label for="category-slug"><?php _e('Slug', 'shop_ct'); ?></label>
        <span></span>
    </div>
    <div class="shop-ct-field shop-ct-mat-input-select shop-ct-full-width">
        <?php
        $args = array(
            'show_option_all' => '',
            'show_option_none' => __("Select Parent", "shop_ct"),
            'option_none_value' => '-1',
            'orderby' => 'ID',
            'order' => 'DESC',
            'show_count' => 0,
            'hide_empty' => 0,
            'echo' => 1,
            'selected' => $category->parent,
            'hierarchical' => 1,
            'name' => 'parent',
            'id' => 'category-parent',
            'taxonomy' => \ShopCT\Models\Category::$taxonomy,
            'hide_if_empty' => false,
            'value_field' => 'term_id',
            'exclude' => $category->id,
        );
        wp_dropdown_categories($args) ?>
    </div>
    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
            <textarea id="category-description"
                      name="description"><?= $category->description; ?></textarea>
        <label for="category-description"><?php _e('Description', 'shop_ct'); ?></label>
        <span></span>
    </div>
    <div class="shop-ct-field">
        <a href="#" id="shop-ct-category-add-image" <?php
        if ($hasThumbnail) {
            echo 'class="shop-ct-hide"';
        }
        ?>><?php _e('Set featured image', 'shop_ct'); ?></a>
        <a href="#" id="shop-ct-category-change-image" <?php
        if (!$hasThumbnail) {
            echo 'class="shop-ct-hide"';
        }
        ?>><img id="shop-ct-category-image" src="<?php echo $thumbnail; ?>" alt=""/></a>
        <input type="hidden" name="shop-ct-category-image" id="shop-ct-category-image-value"
               value="<?php echo $category->thumbnail_id; ?>"/>
        <a href="#" id="shop-ct-category-remove-image" <?php
        if (!$hasThumbnail) {
            echo 'class="shop-ct-hide"';
        }
        ?>><?php _e('Remove feaured image', 'shop_ct'); ?></a>
    </div>
</form>