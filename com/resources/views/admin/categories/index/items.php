<?php
/**
 * @var $categories \ShopCT\Models\Category[]
 */
foreach ($categories as $category): ?>
    <tr data-category-id="<?php echo $category->id; ?>">
        <td data-title="<?php _e('ID', 'shop_ct'); ?>"><?php echo $category->id ?></td>
        <td data-title="<?php _e('Title', 'shop_ct'); ?>">
            <a href="#" class="shop-ct-edit-category"><?php echo $category->name ?></a>
        </td>
        <td data-title="<?php _e('Image', 'shop_ct'); ?>">
            <img src="<?php echo $category->getThumbnailUrl('thumbnail', true); ?>" alt=""/>
        </td>
        <td data-title="<?php _e('Count', 'shop_ct'); ?>"><?php echo $category->term->count; ?></td>
        <td data-title="<?php _e('Shortcode', 'shop_ct'); ?>">
            <input type="text" title="<?php _e('Shortcode', 'shop_ct'); ?>"
                   value="<?php echo esc_attr($category->getShortcode()); ?>"
                   class="shop-ct-shortcode-input"/>
            <a href="#"
               class="shop-ct-mat-button shop-ct-mat-button--secondary shop-ct-copy-category-shortcode"
               title="<?php _e('Copy Shortcode to clipboard', 'shop_ct'); ?>">
                <i class="material-icons">content_copy</i>
        </td>
        <td data-title="<?php _e('Actions', 'shop_ct'); ?>">
            <a href="#" target="_blank"
               class="shop-ct-mat-button shop-ct-mat-button--secondary shop-ct-edit-category"
               title="<?php _e('Edit', 'shop_ct'); ?>">
                <i class="material-icons">edit</i>
            </a>
            <a href="<?php echo $category->getPermalink(); ?>" target="_blank"
               class="shop-ct-mat-button shop-ct-mat-button--secondary"
               title="<?php _e('View', 'shop_ct'); ?>">
                <i class="material-icons">remove_red_eye</i>
            </a>
            <a href="#" class="shop-ct-mat-button shop-ct-mat-button--secondary shop-ct-remove-category"
               title="<?php _e('Remove', 'shop_ct'); ?>">
                <i class="material-icons">delete</i>
            </a>
        </td>
    </tr>
<?php endforeach;