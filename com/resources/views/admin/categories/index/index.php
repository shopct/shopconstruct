<?php
/**
 * @var $categories \ShopCT\Models\Category[]
 * @var $paged int
 * @var $totalCount int
 */
?>

<div id="shop-ct-categories">
    <?php

    shopCTViewRender('admin/common/resource/header', array(
        'showSearch' => true,
        'title' => __('Categories', 'shop_ct'),
        'addNewClassName' => 'shop-ct-add-new-category',
        'showFixedAction' => true,
        'fixedActionClassName' => 'shop-ct-add-new-category'
    ));

    ?>

    <div id="shop-ct-categories-list-wrap" class="shop-ct-mat-table-responsive-vertical shop-ct-shadow-z-1">
        <table class="shop-ct-mat-table shop-ct-mat-table-striped shop-ct-mat-table-condensed shop-ct-mat-table-responsive-vertical">
            <thead>
            <tr>
                <th><?php _e('ID', 'shop_ct'); ?></th>
                <th><?php _e('Title', 'shop_ct'); ?></th>
                <th></th>
                <th><?php _e('Count', 'shop_ct'); ?></th>
                <th><?php _e('Shortcode', 'shop_ct'); ?></th>
                <th><?php _e('Actions', 'shop_ct'); ?></th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <td><?php _e('ID', 'shop_ct'); ?></td>
                <td><?php _e('Title', 'shop_ct'); ?></td>
                <td></td>
                <td><?php _e('Count', 'shop_ct'); ?></td>
                <td><?php _e('Shortcode', 'shop_ct'); ?></td>
                <td><?php _e('Actions', 'shop_ct'); ?></td>
            </tr>
            </tfoot>
            <tbody data-paged="<?php echo $paged; ?>" data-total-count="<?php echo $totalCount; ?>">
            <?php
            if (empty($categories)):
                echo '<tr><td colspan="7">' . __('no items', 'shop_ct') . '</td></tr>';
            else:
                shopCTViewRender('admin/categories/index/items', compact('categories'));
            endif;
            ?>
            </tbody>
        </table>
    </div>

</div>
