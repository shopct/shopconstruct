<?php
/**
 * @var $availablePostStatuses []
 * @var $postsCount []
 * @var $defaultStatus string
 */
?>
<div class="shop-ct-resource-statuses">
    <?php
    $statusLinks = array();

    $defaultUrl = admin_url('admin.php');

    foreach ($_GET as $param => $value) {
        if ($param !== 'action' && $param !== 'post_status' && $param !== 'paged') {
            $defaultUrl = add_query_arg(array($param => $value), $defaultUrl);
        }
    }

    $currentFound = false;

    foreach ($availablePostStatuses as $status => $statusLabel) {
            $classes = "";
            if (isset($_GET['post_status'])) {
                if ($_GET['post_status'] == $status) {
                    $currentFound = true;
                    $classes = ' class="current"';
                }

            } elseif (!$currentFound) {
                $currentFound = true;
                $classes = ' class="current"';
            }

            $urlArgs = array(
                "post_status" => $status,
            );

            $url = add_query_arg($urlArgs, $defaultUrl);


            $label = $statusLabel . sprintf(' <span class="count">(%s)</span>', number_format_i18n($postsCount->$status));

            $statusLinks[$status] = sprintf('<a href="%s" %s >%s</a>', $url, $classes, $label);
    }

    if (!empty($statusLinks)) {
        echo "<ul class='subsubsub shop-ct-post-status'>\n";
        foreach ($statusLinks as $class => $view) {
            $statusLinks[$class] = "\t<li class='$class'>$view";
        }
        echo implode(" |</li>\n", $statusLinks) . "</li>\n";
        echo "</ul>";
    }

    ?>
</div>
