<?php
/**
 * @var $showSearch bool
 * @var $showAddNewButton bool
 * @var $title string
 * @var $addNewClassName string
 * @var $showFixedAction bool
 * @var $fixedActionClassName string
 */
?>

<div class="shop-ct-page-heading">
    <div class="shop-ct-page-meta">
        <h1 class="shop-ct-page-title"> <?php echo $title; ?></h1>
        <?php if (!isset($showAddNewButton) || true === $showAddNewButton): ?>
            <a href="#"
               class="<?php echo $addNewClassName; ?> shop-ct-mat-button shop-ct-mat-button--raised shop-ct-mat-button--primary"><?php _e('Add New', 'shop_ct'); ?></a>
        <?php endif; ?>
    </div>
    <?php if ($showSearch): ?>
        <div class="shop-ct-page-actions">
            <form action="" method="get">
                <?php foreach ($_GET as $paramName => $paramValue):
                    if ($paramName !== 'action' && $paramName !== 'paged'): ?>
                        <input type="hidden" name="<?php echo $paramName; ?>" value="<?php echo $paramValue; ?>"/>
                    <?php endif;
                endforeach; ?>
                <div class="shop-ct-field shop-ct-mat-input-text full-width">
                    <input name="search" id="shop-ct-search"
                           value="<?php echo isset($_GET['search']) ? $_GET['search'] : ''; ?>"/>
                    <label for="shop-ct-search"><?php _e('keyword', 'shop_ct'); ?></label>
                    <span></span>
                </div>
                <?php if (isset($_GET['search']) && !empty($_GET['search'])): ?>
                    <a href="<?php echo esc_url(remove_query_arg('search')) ?>"
                       class="shop-ct-mat-button"><?php _e('Reset', 'shop_ct'); ?></a>
                <?php endif; ?>
                <button type="submit"
                        class="shop-ct-mat-button shop-ct-mat-button--raised shop-ct-mat-button--primary"><?php _e('Search', 'shop_ct'); ?></button>
            </form>
        </div>
    <?php endif; ?>

    <?php if ($showFixedAction): ?>
        <div class="shop-ct-fixed-actions">
            <a href="#"
               class="<?php echo $fixedActionClassName; ?> shop-ct-mat-button shop-ct-mat-button--round shop-ct-mat-button--raised shop-ct-mat-button--primary"><i
                        class="material-icons">add</i></a>
        </div>
    <?php endif; ?>
</div>
