<?php
/**
 * @var $order \ShopCT\Models\Order
 * @var $publishedProducts \ShopCT\Models\Product[]
 */

?>
<div class="shop-ct-grid-item shop-ct-mat-card">
    <span class="shop-ct-mat-card-title"><?php _e('Items', 'shop_ct'); ?></span>

    <div class="shop-ct-flex shop-ct-justify-space-around shop-ct-align-center">
        <select id="shop-ct-order-product" title="<?php _e('Select Product', 'shop_ct'); ?>">
            <option value="">&#8212;<?php _e('Select Product', 'shop_ct'); ?>&#8212;</option>
            <?php foreach ($publishedProducts as $publishedProduct): ?>
                <option value="<?php echo $publishedProduct->id; ?>" data-image-url="<?php echo $publishedProduct->getImageUrl(); ?>" data-price="<?php echo esc_attr(\ShopCT\Lib\Formatting::getPriceHtml($publishedProduct)); ?>" ><?php echo $publishedProduct->getTitle(); ?></option>
            <?php endforeach; ?>
        </select>
        <input id="shop-ct-order-product-quantity" type="number" value="1" min="1"
               title="<?php _e('Product Quantity', 'shop_ct'); ?>"/>
        <a href="#" id="shop-ct-add-order-product"
           class="shop-ct-add-order-product shop-ct-mat-button shop-ct-mat-button--raised"><?php _e('Add Product', 'shop_ct'); ?></a>
    </div>

    <ul id="shop-ct-order-items-list" class="shop-ct-order-items-list shop-ct-mat-list shop-ct-margin-top-15"
        data-row="<?php echo esc_attr(shopCTView('admin/orders/edit/item')); ?>">
        <?php
        $products = $order->getProducts();
        if (empty($products)) {
            echo '<p class="shop-ct-order-items-list-empty">' . __('No items', 'shop_ct') . '</p>';
        } else {
            foreach ($order->getProducts() as $product) {
                shopCTViewRender('admin/orders/edit/item', array(
                    'product' => $product['product'],
                    'quantity' => $product['quantity'],
                    'cost' => $product['cost']
                ));
            }
        }
        ?>
    </ul>
</div>