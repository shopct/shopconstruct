<?php
/**
 * @var $order \ShopCT\Models\Order
 * @var $autoDraft bool
 * @var $availableStatuses array
 * @var $customers
 * @var $countries
 * @var $publishedProducts \ShopCT\Models\Product[]
 */
?>
<form id="shop-ct-order-popup-form" action="#" method="post">
    <div class="shop-ct-grid shop-ct-popup-grid">
        <?php wp_nonce_field('shop_ct', 'nonce'); ?>
        <input type="hidden" id="order_id" name="order_id" value="<?php echo $order->id; ?>"/>
        <input type="hidden" id="order_autodraft" name="order_autodraft" value="<?php echo intval($autoDraft); ?>"/>
        <?php if($autoDraft): ?>
            <input type="hidden" name="post_data[post_title]" value="<?php _ex('Order', 'noun', 'shop_ct'); ?> #<?php echo $order->id; ?>"
        <?php endif; ?>
        <input type="hidden" name="post_data[post_status]" value="publish"/>
        <input type="hidden" name="action" value="shop_ct_update_order"/>
        <?php shopCTViewRender('admin/orders/edit/general', compact('order', 'customers')); ?>
        <?php shopCTViewRender('admin/orders/edit/items', compact('order', 'publishedProducts')); ?>
        <?php shopCTViewRender('admin/orders/edit/billing', compact('order', 'countries')); ?>
        <?php shopCTViewRender('admin/orders/edit/shipping', compact('order', 'countries')); ?>
    </div>
</form>
