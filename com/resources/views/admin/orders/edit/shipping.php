<?php
/**
 * @var $order \ShopCT\Models\Order
 * @var $countries array
 */
?>
<div class="shop-ct-grid-item shop-ct-mat-card">
    <span class="shop-ct-mat-card-title"><?php _e('Shipping Details', 'shop_ct'); ?></span>
    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input name="post_meta[shipping_first_name]" id="shop-ct-order-shipping-first-name" value="<?= $order->shipping_first_name; ?>"/>
        <label for="shop-ct-order-shipping-first-name"><?php _e('First Name', 'shop_ct'); ?></label>
        <span></span>
    </div>
    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input name="post_meta[shipping_last_name]" id="shop-ct-order-shipping-last-name" value="<?= $order->shipping_last_name; ?>"/>
        <label for="shop-ct-order-shipping-last-name"><?php _e('Last Name', 'shop_ct'); ?></label>
        <span></span>
    </div>
    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input name="post_meta[shipping_company]" id="shop-ct-order-shipping-company" value="<?= $order->shipping_company; ?>"/>
        <label for="shop-ct-order-shipping-company"><?php _e('Company', 'shop_ct'); ?></label>
        <span></span>
    </div>
    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input name="post_meta[shipping_address_1]" id="shop-ct-order-shipping-address-1" value="<?= $order->shipping_address_1; ?>"/>
        <label for="shop-ct-order-shipping-address-1"><?php _e('Address 1', 'shop_ct'); ?></label>
        <span></span>
    </div>
    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input name="post_meta[shipping_address_2]" id="shop-ct-order-shipping-address-2" value="<?= $order->shipping_address_2; ?>"/>
        <label for="shop-ct-order-shipping-address-2"><?php _e('Address 2', 'shop_ct'); ?></label>
        <span></span>
    </div>
    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input name="post_meta[shipping_city]" id="shop-ct-order-shipping-city" value="<?= $order->shipping_city; ?>"/>
        <label for="shop-ct-order-shipping-city"><?php _e('City', 'shop_ct'); ?></label>
        <span></span>
    </div>
    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input name="post_meta[shipping_postcode]" id="shop-ct-order-shipping-postcode" value="<?= $order->shipping_postcode; ?>"/>
        <label for="shop-ct-order-shipping-postcode"><?php _e('Postcode', 'shop_ct'); ?></label>
        <span></span>
    </div>

    <div class="shop-ct-field shop-ct-mat-input-select shop-ct-full-width">
        <label for="shop-ct-order-shipping-country"><?php _e('Country', 'shop_ct'); ?></label>
        <select name="post_meta[shipping_country]" id="shop-ct-order-shipping-country">
            <option value="">&#8212;<?php _e('Select Country', 'shop_ct'); ?>&#8212;</option>
            <?php foreach ($countries as $code => $name): ?>
                <option value="<?php echo $code; ?>" <?php selected($order->shipping_country, $code); ?>><?php echo $name; ?></option>
            <?php endforeach; ?>
        </select>
    </div>

    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input name="post_meta[shipping_state]" id="shop-ct-order-shipping-state" value="<?= $order->shipping_state; ?>"/>
        <label for="shop-ct-order-shipping-state"><?php _e('State', 'shop_ct'); ?></label>
        <span></span>
    </div>

    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
            <textarea id="shop-ct-order-shipping-notes"
                      name="post_meta[shipping_customer_note]"><?= $order->shipping_customer_note ?></textarea>
        <label for="shop-ct-order-shipping-notes"><?php _e('Additional Details/Notes', 'shop_ct'); ?></label>
        <span></span>
    </div>
</div>
