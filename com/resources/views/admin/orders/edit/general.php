<?php
/**
 * @var $order \ShopCT\Models\Order
 * @var $customers array
 */
?>
<div class="shop-ct-grid-item shop-ct-mat-card">
    <span class="shop-ct-mat-card-title"><?php _e('General', 'shop_ct'); ?></span>
    <div class="shop-ct-field shop-ct-mat-input-select shop-ct-full-width">
        <label for="post_data[post_status]"><?php _e('Status', 'shop_ct'); ?></label>
        <select name="post_data[post_status]" id="post_data[post_status]">
            <?php foreach(\ShopCT\Models\Order::getAvailableStatuses() as $status => $label): ?>
                <option value="<?php echo $status; ?>" <?php selected($order->status, $status); ?>><?php echo $label ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="shop-ct-field shop-ct-mat-input-select shop-ct-full-width">
        <label for="customer"><?php _e('Customer', 'shop_ct'); ?></label>
        <select name="post_meta[customer]" id="customer">
            <?php foreach($customers as $id => $name): ?>
                <option value="<?php echo $id; ?>" <?php selected($order->customer, $id); ?>><?php echo $name ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="shop-ct-field shop-ct-flex shop-ct-justify-between shop-ct-align-end">
        <div class="shop-ct-mat-input-text shop-ct-flex-4">
            <input placeholder="yy-mm-dd" class="shop-ct-order-datepicker"
                   name="date" id="shop-ct-order-date"
                   value="<?= !empty($order->date) ? date_i18n('Y-m-d', strtotime($order->date)) : ''; ?>"/>
            <span></span>
        </div>
        <span class="at shop-ct-flex-1 text-center">@</span>
        <div class="shop-ct-mat-input-text shop-ct-flex-3">
            <input type="number" placeholder="00" name="date-hours"
                   id="shop-ct-order-date-hours" size="2"
                   value="<?= !empty($order->date) ? date_i18n('H', strtotime($order->date)) : '00'; ?>"/>
            <span></span>
        </div>
        <div class="shop-ct-mat-input-text shop-ct-flex-3">
            <input type="number" placeholder="00" name="date-min"
                   id="shop-ct-order-date-min"
                   value="<?= !empty($order->date) ? date_i18n('i', strtotime($order->date)) : '00'; ?>"/>
            <span></span>
        </div>
    </div>
</div>
