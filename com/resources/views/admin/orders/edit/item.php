<?php
/**
 * @var $product \ShopCT\Models\Product
 * @var $quantity int
 * @var $cost float
 */
$productExists = isset($product);
$id = $productExists ? $product->id : '{productId}';
$imageUrl = $productExists ? $product->getImageUrl() : '{productImageUrl}';
$title = $productExists ? $product->getTitle() : '{productTitle}';
if ($productExists && isset($cost)) {
    $price = $cost;
} elseif ($productExists) {
    $price = \ShopCT\Lib\Formatting::getPriceHtml($product);
} else {
    $price = '{productPrice}';
}
$quantity = isset($quantity) ? $quantity : '{quantity}';

?>
<li class="shop-ct-mat-list-item shop-ct-order-items-list-item" data-product-id="<?php echo $id; ?>">
    <img class="shop-ct-mat-list-item--graphic" src="<?php echo $imageUrl; ?>" alt=""/>
    <?php echo $title; ?>
    <span class="shop-ct-mat-list-item--meta">
        <input type="number" min="1" class="shop-ct-order-product-qty-input" name="order_products[<?php echo $id; ?>][quantity]" value="<?php echo $quantity; ?>" title="<?php _e('Quantity','shop_ct'); ?>" />
        <span class="order-product-cost"><?php echo $price; ?></span>
        <span class="delete-order-item shop-ct-on-click--scale fa fa-times"></span>
    </span>
    <input type="hidden" name="order_products[<?php echo $id; ?>][product]" value="<?php echo $id; ?>"/>
</li>
