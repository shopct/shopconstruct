<?php
/**
 * @var $order \ShopCT\Models\Order
 * @var $countries array
 */
?>
<div class="shop-ct-grid-item shop-ct-mat-card">
    <span class="shop-ct-mat-card-title"><?php _e('Billing Details', 'shop_ct'); ?></span>
    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input name="post_meta[billing_first_name]" id="shop-ct-order-billing-first-name" value="<?= $order->billing_first_name; ?>"/>
        <label for="shop-ct-order-billing-first-name"><?php _e('First Name', 'shop_ct'); ?></label>
        <span></span>
    </div>
    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input name="post_meta[billing_last_name]" id="shop-ct-order-billing-last-name" value="<?= $order->billing_last_name; ?>"/>
        <label for="shop-ct-order-billing-last-name"><?php _e('Last Name', 'shop_ct'); ?></label>
        <span></span>
    </div>
    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input name="post_meta[billing_company]" id="shop-ct-order-billing-company" value="<?= $order->billing_company; ?>"/>
        <label for="shop-ct-order-billing-company"><?php _e('Company', 'shop_ct'); ?></label>
        <span></span>
    </div>
    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input name="post_meta[billing_address_1]" id="shop-ct-order-billing-address-1" value="<?= $order->billing_address_1; ?>"/>
        <label for="shop-ct-order-billing-address-1"><?php _e('Address 1', 'shop_ct'); ?></label>
        <span></span>
    </div>
    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input name="post_meta[billing_address_2]" id="shop-ct-order-billing-address-2" value="<?= $order->billing_address_2; ?>"/>
        <label for="shop-ct-order-billing-address-2"><?php _e('Address 2', 'shop_ct'); ?></label>
        <span></span>
    </div>
    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input name="post_meta[billing_city]" id="shop-ct-order-billing-city" value="<?= $order->billing_city; ?>"/>
        <label for="shop-ct-order-billing-city"><?php _e('City', 'shop_ct'); ?></label>
        <span></span>
    </div>
    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input name="post_meta[billing_postcode]" id="shop-ct-order-billing-postcode" value="<?= $order->billing_postcode; ?>"/>
        <label for="shop-ct-order-billing-postcode"><?php _e('Postcode', 'shop_ct'); ?></label>
        <span></span>
    </div>

    <div class="shop-ct-field shop-ct-mat-input-select shop-ct-full-width">
        <label for="shop-ct-order-billing-country"><?php _e('Country', 'shop_ct'); ?></label>
        <select name="post_meta[billing_country]" id="shop-ct-order-billing-country">
            <option value="">&#8212;<?php _e('Select Country', 'shop_ct'); ?>&#8212;</option>
            <?php foreach ($countries as $code => $name): ?>
                <option value="<?php echo $code; ?>" <?php selected($order->billing_country, $code); ?>><?php echo $name; ?></option>
            <?php endforeach; ?>
        </select>
    </div>

    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input name="post_meta[billing_state]" id="shop-ct-order-billing-state" value="<?= $order->billing_state; ?>"/>
        <label for="shop-ct-order-billing-state"><?php _e('State', 'shop_ct'); ?></label>
        <span></span>
    </div>

    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input type="email" name="post_meta[billing_email]" id="shop-ct-order-billing-email" value="<?= $order->billing_email; ?>"/>
        <label for="shop-ct-order-billing-email"><?php _e('Email', 'shop_ct'); ?></label>
        <span></span>
    </div>

    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input type="email" name="post_meta[billing_phone]" id="shop-ct-order-billing-phone" value="<?= $order->billing_phone; ?>"/>
        <label for="shop-ct-order-billing-phone"><?php _e('Phone', 'shop_ct'); ?></label>
        <span></span>
    </div>
</div>
