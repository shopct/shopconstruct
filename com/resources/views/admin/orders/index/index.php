<?php
/**
 * @var $orders \ShopCT\Models\Order[]
 * @var $availablePostStatuses []
 * @var $defaultStatus
 * @var $postsCount []
 * @var $totalCount int
 * @var $paged int
 */

?>
<div id="shop-ct-orders">


    <?php

    shopCTViewRender('admin/common/resource/header', array(
        'showSearch' => true,
        'title' => __('Oredrs', 'shop_ct'),
        'addNewClassName' => 'shop-ct-add-new-order',
        'showFixedAction' => true,
        'fixedActionClassName' => 'shop-ct-add-new-order'
    ));

    shopCTViewRender('admin/common/resource/post-type/statuses', compact('availablePostStatuses', 'postsCount', 'defaultStatus'));

    ?>

    <div id="shop-ct-orders-list-wrap" class="shop-ct-mat-table-responsive-vertical shop-ct-shadow-z-1" >
        <table class="shop-ct-mat-table shop-ct-mat-table-striped shop-ct-mat-table-condensed shop-ct-mat-table-responsive-vertical">
            <thead>
            <tr>
                <th><?php _e('Order', 'shop_ct'); ?></th>
                <th><?php _e('Purchased', 'shop_ct'); ?></th>
                <th><?php _e('Ship to', 'shop_ct'); ?></th>
                <th><?php _e('Date', 'shop_ct'); ?></th>
                <th><?php _e('Total', 'shop_ct'); ?></th>
                <th><?php _e('Actions', 'shop_ct'); ?></th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <td><?php _e('Order', 'shop_ct'); ?></td>
                <td><?php _e('Purchased', 'shop_ct'); ?></td>
                <td><?php _e('Ship to', 'shop_ct'); ?></td>
                <td><?php _e('Date', 'shop_ct'); ?></td>
                <td><?php _e('Total', 'shop_ct'); ?></td>
                <td><?php _e('Actions', 'shop_ct'); ?></td>
            </tr>
            </tfoot>
            <tbody data-paged="<?php echo $paged ?>" data-total-count="<?php echo $totalCount; ?>" >
            <?php
            if (empty($orders)):
                echo '<tr><td colspan="6">' . __('no items', 'shop_ct') . '</td></tr>';
            else:
                shopCTViewRender('admin/orders/index/items', compact('orders'));
            endif;
            ?>
            </tbody>
        </table>

        <?php if ($paged < $totalCount): ?>
            <div class="shop-ct-text-center">
                <a href="#"
                   class="shop-ct-load-more shop-ct-mat-button shop-ct-mat-button--raised shop-ct-mat-button--primary"><?php _e('Load More', 'shop_ct'); ?></a>
            </div>
        <?php endif; ?>
    </div>

</div>