<?php
/**
 * @var $orders \ShopCT\Models\Order[]
 */
foreach ($orders as $order): ?>
    <tr data-product-id="<?php echo $order->id; ?>">
        <td data-title="<?php _e('Order', 'shop_ct'); ?>">#<?php echo $order->id; ?></td>
        <td data-title="<?php _e('Purchased', 'shop_ct'); ?>">
            <?php
            $products = $order->getProducts();
            $productNames = array_map(function($product){
                return '<span class="shop-ct-order-product-name">' . $product['object']->getTitle() . '</span> <span class="shop-ct-quantity">(' . $product['quantity'] . ')</span>';
            },$products);

            echo implode('<br />', $productNames);
            ?>
        </td>
        <td data-title="<?php _e('Ship to', 'shop_ct'); ?>">
            <?php echo \ShopCT\Lib\Formatting::formatShippingAddresses($order); ?>
        </td>
        <td data-title="<?php _e('Date', 'shop_ct'); ?>">
            <?php echo $order->post->post_date; ?>
        </td>
        <td data-title="<?php _e('Total', 'shop_ct'); ?>">
            <?php echo \ShopCT\Lib\Formatting::formatPrice($order->total); ?>
        </td>
        <td data-title="<?php _e('Actions', 'shop_ct'); ?>">
            <?php var_dump($order->status); ?>
        </td>
    </tr>
<?php endforeach;