<?php
/**
 * @var $reviews \ShopCT\Models\Review[]
 */
?>

<div id="shop-ct-reviews">

    <?php
    shopCTViewRender('admin/common/resource/header', array(
        'showSearch' => false,
        'showFixedAction' => false,
        'showAddNewButton' => false,
        'title' => __('Product Reviews', 'shop_ct'),
    ));
    ?>

    <div id="shop-ct-reviews-list-wrap" class="shop-ct-mat-table-responsive-vertical shop-ct-shadow-z-1">
        <table class="shop-ct-mat-table shop-ct-mat-table-striped shop-ct-mat-table-condensed shop-ct-mat-table-responsive-vertical">
            <thead>
            <tr>
                <th><?php _e('ID', 'shop_ct'); ?></th>
                <th><?php _e('Author', 'shop_ct'); ?></th>
                <th><?php _e('Review', 'shop_ct'); ?></th>
                <th><?php _e('In response to', 'shop_ct'); ?></th>
                <th><?php _e('Rating', 'shop_ct'); ?></th>
                <th><?php _e('Submitted on', 'shop_ct'); ?></th>
                <th><?php _e('Actions', 'shop_ct'); ?></th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <td><?php _e('ID', 'shop_ct'); ?></td>
                <td><?php _e('Author', 'shop_ct'); ?></td>
                <td><?php _e('Review', 'shop_ct'); ?></td>
                <td><?php _e('In response to', 'shop_ct'); ?></td>
                <td><?php _e('Rating', 'shop_ct'); ?></td>
                <td><?php _e('Submitted on', 'shop_ct'); ?></td>
                <td><?php _e('Actions', 'shop_ct'); ?></td>
            </tr>
            </tfoot>
            <tbody>
            <?php
            if (empty($reviews)):
                echo '<tr><td colspan="7">' . __('no items', 'shop_ct') . '</td></tr>';
            else:
                shopCTViewRender('admin/reviews/index/items', compact('reviews'));
            endif;
            ?>
            </tbody>
        </table>
    </div>
</div>
