<?php
/**
 * @var $reviews \ShopCT\Models\Review[]
 */

foreach ($reviews as $review):
    $product = $review->getProduct();

    ?>
    <tr data-review-id="<?php echo $review->id; ?>">
        <td data-title="<?php _e('ID', 'shop_ct'); ?>"><?php echo $review->id ?></td>
        <td data-title="<?php _e('Author', 'shop_ct'); ?>">
            <b><?php echo $review->author; ?></b><br/>
            <a href="<?php echo $review->author_url; ?>"><?php echo $review->author_email; ?></a>
        </td>
        <td data-title="<?php _e('Review', 'shop_ct'); ?>"><?php echo $review->content; ?></td>
        <td data-title="<?php _e('In response to', 'shop_ct'); ?>">
            <a href="<?php echo $product->getPermalink(); ?>"><?php echo $product->getTitle(); ?></a>
        </td>
        <td data-title="<?php _e('Rating', 'shop_ct'); ?>">
            <?php echo $review->rating; ?>
        </td>
        <td data-title="<?php _e('Submitted on', 'shop_ct'); ?>">
            <?php echo $review->date; ?>
        </td>
        <td>
            <?php if ($review->getStatus() === 'approved'): ?>
                <a href="#" class="shop-ct-mat-button shop-ct-mat-button--secondary shop-ct-update-review-status"
                   data-status="0">
                    <i class="material-icons" title="Hold">pan_tool</i>
                </a>
            <?php elseif ($review->getStatus() === 'spam'): ?>
                <a href="#" class="shop-ct-mat-button shop-ct-mat-button--secondary shop-ct-update-review-status"
                   data-status="1">
                    <i class="material-icons" title="Approve">done</i>
                </a>
                <a href="#" class="shop-ct-mat-button shop-ct-mat-button--secondary shop-ct-update-review-status"
                   data-status="0">
                    <i class="material-icons" title="Hold">pan_tool</i>
                </a>
            <?php else: ?>
                <a href="#" class="shop-ct-mat-button shop-ct-mat-button--secondary shop-ct-update-review-status"
                   data-status="1">
                    <i class="material-icons" title="Approve">done</i>
                </a>
                <a href="#" class="shop-ct-mat-button shop-ct-mat-button--secondary shop-ct-update-review-status"
                   data-status="spam">
                    <i class="material-icons" title="Spam">error</i>
                </a>
            <?php endif; ?>
            <a href="#" class="shop-ct-mat-button shop-ct-mat-button--secondary shop-ct-remove-review"
               title="<?php _e('Remove', 'shop_ct'); ?>">
                <i class="material-icons">delete</i>
            </a>
        </td>
    </tr>
<?php endforeach;