<?php
/**
 * @var $products \ShopCT\Models\Product[]
 * @var $availablePostStatuses []
 * @var $postsCount []
 * @var $totalCount int
 * @var $paged int
 */

?>
<div id="shop-ct-products">


    <?php

    shopCTViewRender('admin/common/resource/header', array(
        'showSearch' => true,
        'title' => __('Products', 'shop_ct'),
        'addNewClassName' => 'shop-ct-add-new-product',
        'showFixedAction' => true,
        'fixedActionClassName' => 'shop-ct-add-new-product'
    ));

    shopCTViewRender('admin/common/resource/post-type/statuses', compact('availablePostStatuses', 'postsCount'));

    ?>

    <div id="shop-ct-products-list-wrap" class="shop-ct-mat-table-responsive-vertical shop-ct-shadow-z-1">
        <table class="shop-ct-mat-table shop-ct-mat-table-striped shop-ct-mat-table-condensed shop-ct-mat-table-responsive-vertical">
            <thead>
            <tr>
                <th><?php _e('Product ID', 'shop_ct'); ?></th>
                <th><?php _e('Title', 'shop_ct'); ?></th>
                <th></th>
                <th><?php _e('Stock', 'shop_ct'); ?></th>
                <th><?php _e('Price', 'shop_ct'); ?></th>
                <?php if (!isset($_GET['post_status']) || $_GET['post_status'] !== 'trash'): ?>
                    <th><?php _e('Shortcode', 'shop_ct'); ?></th>
                <?php endif; ?>
                <th><?php _e('Actions', 'shop_ct'); ?></th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <td><?php _e('Product ID', 'shop_ct'); ?></td>
                <td><?php _e('Title', 'shop_ct'); ?></td>
                <td></td>
                <td><?php _e('Stock', 'shop_ct'); ?></td>
                <td><?php _e('Price', 'shop_ct'); ?></td>
                <?php if (!isset($_GET['post_status']) || $_GET['post_status'] !== 'trash'): ?>
                    <td><?php _e('Shortcode', 'shop_ct'); ?></td>
                <?php endif; ?>
                <td><?php _e('Actions', 'shop_ct'); ?></td>
            </tr>
            </tfoot>
            <tbody  data-paged="<?php echo $paged ?>" data-total-count="<?php echo $totalCount; ?>" >
            <?php
            if (empty($products)):
                echo '<tr><td colspan="7">' . __('no items', 'shop_ct') . '</td></tr>';
            else:
                shopCTViewRender('admin/products/index/items', compact('products'));
            endif;
            ?>
            </tbody>
        </table>

        <?php if ($paged < $totalCount): ?>
            <div class="shop-ct-text-center">
                <a href="#"
                   class="shop-ct-load-more shop-ct-mat-button shop-ct-mat-button--raised shop-ct-mat-button--primary"><?php _e('Load More', 'shop_ct'); ?></a>
            </div>
        <?php endif; ?>
    </div>

</div>