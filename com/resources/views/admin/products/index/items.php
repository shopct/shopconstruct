<?php
/**
 * @var $products \ShopCT\Models\Product[]
 */
foreach ($products as $product): ?>
    <tr data-product-id="<?php echo $product->id; ?>">
        <td data-title="<?php _e('Product ID', 'shop_ct'); ?>"><?php echo $product->id; ?></td>
        <td data-title="<?php _e('Title', 'shop_ct'); ?>">
            <?php if ($product->post->post_status !== 'trash'): ?>
                <a href="#" class="shop-ct-edit-product"><?php echo $product->getTitle(); ?></a>
            <?php else:
                echo $product->getTitle();
            endif; ?>
        </td>
        <td data-title="<?php _e('Image', 'shop_ct'); ?>"><img
                    src="<?php echo $product->getImageUrl(); ?>"
                    alt="<?php echo $product->getTitle(); ?>"/></td>
        <td data-title="<?php _e('Stock', 'shop_ct'); ?>"><?php
            $availabilty = $product->getAvailability();
            $class = $availabilty['class'];
            $text = $availabilty['availability'];

            echo '<span class="' . $class . '">' . $text . '</span>';
            ?></td>
        <td data-title="<?php _e('Price', 'shop_ct'); ?>"><?php echo \ShopCT\Lib\Formatting::getPriceHtml($product); ?></td>
        <?php if (!isset($_GET['post_status']) || $_GET['post_status'] !== 'trash'): ?>
            <td data-title="<?php _e('Shortcode', 'shop_ct'); ?>">
                <input type="text" title="<?php _e('Shortcode', 'shop_ct'); ?>"
                       value="<?php echo esc_attr($product->getShortcode()); ?>"
                       class="shop-ct-shortcode-input"/>
                <a href="#"
                   class="shop-ct-mat-button shop-ct-mat-button--secondary shop-ct-copy-product-shortcode"
                   title="<?php _e('Copy Shortcode to clipboard', 'shop_ct'); ?>">
                    <i class="material-icons">content_copy</i>
                </a>
            </td>
        <?php endif; ?>
        <td data-title="<?php _e('Actions', 'shop_ct'); ?>">
            <?php if ($product->post->post_status !== 'trash'): ?>
                <a href="#" target="_blank"
                   class="shop-ct-mat-button shop-ct-mat-button--secondary shop-ct-edit-product"
                   title="<?php _e('Edit', 'shop_ct'); ?>">
                    <i class="material-icons">edit</i>
                </a>
                <a href="<?php echo $product->getPermalink(); ?>" target="_blank"
                   class="shop-ct-mat-button shop-ct-mat-button--secondary"
                   title="<?php _e('View', 'shop_ct'); ?>">
                    <i class="material-icons">remove_red_eye</i>
                </a>
                <a href="#" class="shop-ct-mat-button shop-ct-mat-button--secondary shop-ct-trash-product"
                   title="<?php _e('Move to Trash', 'shop_ct'); ?>">
                    <i class="material-icons">delete</i>
                </a>
            <?php else: ?>
                <a href="#" target="_blank"
                   class="shop-ct-mat-button shop-ct-mat-button--secondary shop-ct-untrash-product"
                   title="<?php _e('Restore', 'shop_ct'); ?>">
                    <i class="material-icons">restore</i>
                </a>
                <a href="#" target="_blank"
                   class="shop-ct-mat-button shop-ct-mat-button--secondary shop-ct-remove-product"
                   title="<?php _e('Delete Forever', 'shop_ct'); ?>">
                    <i class="material-icons">delete_forever</i>
                </a>
            <?php endif; ?>
        </td>
    </tr>
<?php endforeach;