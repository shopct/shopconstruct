<?php
/**
 * @var $product \ShopCT\Models\Product
 * @var $autoDraft bool
 */
?>
<form id="shop-ct-product-popup-form" action="#" method="post">
    <div class="shop-ct-grid shop-ct-popup-grid">
        <?php wp_nonce_field('shop_ct', 'nonce'); ?>
        <input type="hidden" id="product_id" name="product_id" value="<?php echo $product->id; ?>"/>
        <input type="hidden" id="product_autodraft" name="product_autodraft" value="<?php echo intval($autoDraft); ?>"/>
        <input type="hidden" name="post_data[post_status]" value="publish"/>
        <input type="hidden" name="action" value="shop_ct_update_product"/>
        <input type="hidden" id="product_name" name="post_data[post_name]"
               value="<?php echo $product->post->post_name; ?>"/>
        <?php shopCTViewRender('admin/products/edit/general', compact('product')); ?>
        <?php shopCTViewRender('admin/products/edit/info', compact('product')); ?>
        <?php shopCTViewRender('admin/products/edit/downloadable', compact('product')); ?>
        <?php shopCTViewRender('admin/products/edit/taxonomies', compact('product')); ?>
        <?php shopCTViewRender('admin/products/edit/attributes', compact('product')); ?>
        <?php shopCTViewRender('admin/products/edit/inventory', compact('product')); ?>
        <?php shopCTViewRender('admin/products/edit/shipping', compact('product')); ?>
        <?php shopCTViewRender('admin/products/edit/seo', compact('product')); ?>
    </div>
</form>
