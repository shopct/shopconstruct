<?php
/**
 * @var $product \ShopCT\Models\Product
 */
?>
<div class="shop-ct-grid-item shop-ct-mat-card">
    <span class="shop-ct-mat-card-title"><?php _e('Info', 'shop_ct'); ?></span>
    <div class="shop-ct-field">
            <textarea name="post_data[post_content]"
                      id="product_content"><?= wp_kses_post($product->post->post_content); ?></textarea>
    </div>
    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
            <textarea id="post_data[post_excerpt]"
                      name="post_data[post_excerpt]"><?= $product->post->post_excerpt; ?></textarea>
        <label for="product_excerpt"><?php _e('Short Description', 'shop_ct'); ?></label>
        <span></span>
    </div>
    <div class="shop-ct-field shop-ct-product-gallery">
        <span class="shop-ct-field-label"><?php _e('Image Gallery', 'shop_ct'); ?></span>
        <ul class="product-image-gallery">
            <li class="product-image-gallery-add">
                <div>
                    <i class="material-icons">add</i>
                    <span><?php _e('Add Image(s)', 'shop_ct'); ?></span>
                </div>
            </li>
            <?php foreach ($product->product_image_gallery as $gallery_item): ?>
                <li>
                    <div class="product-image-gallery-inner">
                        <img src="<?= wp_get_attachment_image_src($gallery_item)[0]; ?>"/>
                        <button class="product-image-gallery-delete"><i class="material-icons">delete</i></button>
                        <input type="hidden" name="post_meta[product_image_gallery][]"
                               id="post_meta[product_image_gallery][]"
                               value="<?= $gallery_item; ?>"/>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
