<?php
/**
 * @var $product \ShopCT\Models\Product
 */
?>
<div class="shop-ct-grid-item shop-ct-mat-card">
    <span class="shop-ct-mat-card-title"><?php _e('Inventory', 'shop_ct'); ?></span>
    <?php if (shopCTProductSettings()->manage_stock === 'yes'): ?>
        <div class="shop-ct-field shop-ct-mat-input-checkbox">
            <input type="hidden" name="product_manage_stock" value="0"/>
            <label class="shop-ct-mat-input-checkbox-slider">
                <input type="checkbox" id="product_manage_stock" name="post_meta[manage_stock]"
                       value="1" <?php checked($product->manage_stock); ?> />
                <span></span>
            </label>
            <label for="product_manage_stock"><?php _e('Manage Stock?', 'shop_ct'); ?></label>
        </div>
    <?php endif; ?>
    <div class="show_if_managing_stock" <?php if (!$product->managingStock()) {
        echo 'style="display:none"';
    } ?> >
        <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
            <input type="number" name="post_meta[stock]" id="post_meta[stock]"
                   value="<?= $product->stock; ?>"/>
            <label for="post_meta[stock]"><?php _e('Stock Qty', 'shop_ct'); ?></label>
            <span></span>
        </div>
        <div class="shop-ct-field shop-ct-mat-input-select shop-ct-full-width">
            <label for="post_meta[backorders]"><?php _e('Allow backorders?', 'shop_ct'); ?></label>
            <select name="post_meta[backorders]" id="post_meta[backorders]">
                <option value="no" <?php echo selected($product->backorders, 'no') ?>>Do not allow</option>
                <option value="notify" <?php echo selected($product->backorders, 'notify') ?>>Allow, but notify
                    customer
                </option>
                <option value="yes" <?php echo selected($product->backorders, 'yes') ?>>Allow</option>
            </select>
        </div>
    </div>
    <div class="shop-ct-field shop-ct-mat-input-select shop-ct-full-width">
        <label for="post_meta[stock_status]"><?php _e('Stock Status', 'shop_ct'); ?></label>
        <select name="post_meta[stock_status]" id="post_meta[stock_status]">
            <option value="instock" <?php echo selected($product->stock_status, 'instock') ?>>In stock</option>
            <option value="outofstock" <?php echo selected($product->stock_status, 'outofstock') ?>>Out of stock
            </option>
        </select>
    </div>
    <div class="shop-ct-field shop-ct-mat-input-checkbox">
        <input type="hidden" name="post_meta[sold_individually]" value="0"/>
        <label class="shop-ct-mat-input-checkbox-slider">
            <input type="checkbox" id="post_meta[sold_individually]" name="post_meta[sold_individually]"
                   value="1" <?php checked($product->sold_individually); ?> />
            <span></span>
        </label>
        <label for="post_meta[sold_individually]"><?php _e('Sold individually', 'shop_ct'); ?></label>
    </div>
</div>
