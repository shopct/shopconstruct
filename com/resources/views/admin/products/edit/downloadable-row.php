<?php
/**
 * @var $file null|array
 */
if (!isset($file)) $file = array('name' => '', 'url' => '');
?>
<tr>
    <td class="sort">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
            <path fill="none" d="M0 0h24v24H0V0z"/>
            <path d="M11 18c0 1.1-.9 2-2 2s-2-.9-2-2 .9-2 2-2 2 .9 2 2zm-2-8c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0-6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm6 4c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0 2c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"/>
        </svg>
    </td>
    <td class="file_name">
        <input type="text" placeholder="<?php _e('File Name', 'shop_ct'); ?>" name="downloadable-file-names[]"
               value="<?php echo $file['name']; ?>"/>
    </td>
    <td class="file_url">
        <input type="url" class="input_text" placeholder="<?php _e('File URL', 'shop_ct'); ?>"
               name="downloadable-file-urls[]" value="<?php echo $file['url']; ?>"/>
        <button class="upload_button"><?php _e('Choose file'); ?></button>
    </td>
    <td class="remove"><i class="remove_btn material-icons">close</i></td>
</tr>
