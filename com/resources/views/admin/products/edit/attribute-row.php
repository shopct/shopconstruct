<?php
/**
 * @var $product \ShopCT\Models\Product
 * @var $attribute \ShopCT\Models\ProductAttribute
 */
if (null === $attribute): ?>
    <div class="product-attributes-item">
        <input type="text" name="product-attribute-taxonomies[]" value="" placeholder="<?php _e('Name', 'shop_ct'); ?>">
        <textarea name="product-attribute-terms[]"
                  placeholder="<?php _e('Values separated with commas', 'shop_ct'); ?>"></textarea>
        <span class="product-attributes-delete"><i class="material-icons">close</i></span>
    </div>
<?php else: ?>
    <div class="product-attributes-item">
        <span class="product-attribute-taxonomy-placeholder"><?php echo $attribute->name; ?></span>
        <input type="hidden" name="product-attribute-taxonomies[]" value="<?php echo $attribute->id; ?>">
        <textarea name="product-attribute-terms[]"
                  placeholder="<?php _e('Values separated with commas', 'shop_ct'); ?>"><?php
            if (null !== $product):
                $terms = $product->getAttributeTerms($attribute);
                if (!empty($terms)):
                    echo implode(',', array_map(
                        function (\ShopCT\Models\ProductAttributeTerm $el) {
                            return $el->name;
                        }, $terms));
                endif;
            endif;
            ?></textarea>
        <span class="product-attributes-delete"><i class="material-icons">close</i></span>
    </div>
<?php endif;
