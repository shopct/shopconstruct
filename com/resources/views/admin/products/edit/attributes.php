<?php
/**
 * @var $product \ShopCT\Models\Product
 */
?>
<div class="shop-ct-grid-item shop-ct-mat-card">
    <span class="shop-ct-mat-card-title"><?php _e('Attributes', 'shop_ct'); ?></span>
    <div class="shop-ct-field">
        <select id="product-new-attribute-taxonomy">
            <option value="custom"><?php _e('New Attribute', 'shop_ct'); ?></option>
            <?php foreach (\ShopCT\Models\ProductAttribute::getAll() as $attr): ?>
                <option value="<?= $attr->id; ?>"><?= $attr->name; ?></option>
            <?php endforeach; ?>
        </select>
        <button class="product-add-attribute shop-ct-mat-button"><?php _e('Add Attribute', 'shop_ct'); ?></button>
    </div>
    <div class="shop-ct-field">
        <div class="product-attributes-list">
            <?php
            $attributes = $product->attributes;
            if (!empty($attributes)):
                foreach ($product->attributes as $attr_slug => $attr_terms):
                    if (empty($attr_terms)) {
                        continue;
                    }
                    $attribute = new \ShopCT\Models\ProductAttribute(null, array('slug' => $attr_slug));
                    shopCTViewRender('admin/products/edit/attribute-row', compact('product', 'attribute'));
                endforeach;
            endif;
            ?>
        </div>
    </div>
</div>
