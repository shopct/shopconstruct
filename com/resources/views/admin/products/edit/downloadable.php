<?php
/**
 * @var $product \ShopCT\Models\Product
 */
?>
<div class="shop-ct-grid-item shop-ct-mat-card show_if_downloadable">
    <span class="shop-ct-mat-card-title"><?php _e('Downloadable Files', 'shop_ct'); ?></span>
    <div class="product-downloadable-files-block">
        <table class="widefat">
            <tbody class="shop-ct-files-sortable">
            <?php
            $files = $product->downloadable_files;
            if (!empty($files)):
                foreach ($files as $file):
                    shopCTViewRender('admin/products/edit/downloadable-row', compact('file'));
                endforeach;
            else:
                echo '<tr class="no-items"><td colspan="7">' . __('No Files', 'shop_ct') . '</td></tr>';
            endif;
            ?>
            </tbody>
            <thead>
            <tr>
                <td colspan="5">
                    <button class="shop-ct-mat-button shop-ct-mat-button--raised product-add-downloadable-file"
                            data-row="<?php echo htmlspecialchars(shopCTView('admin/products/edit/downloadable-row')); ?>"><?php _e('Add File', 'shop_ct'); ?></button>
                </td>
            </tr>
            </thead>
        </table>
    </div>
    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input type="number" name="post_meta[download_limit]" id="post_meta[download_limit]"
               value="<?= $product->download_limit; ?>"/>
        <label for="post_meta[download_limit]"><?php _e('Download Limit', 'shop_ct'); ?></label>
        <span></span>
    </div>
    <div class="shop-ct-field shop-ct-mat-input-text shop-ct-full-width">
        <input type="number" name="post_meta[download_expiry]" id="post_meta[download_expiry]"
               value="<?= $product->download_expiry; ?>"/>
        <label for="post_meta[download_expiry]"><?php _e('Download Expiry (in days)', 'shop_ct'); ?></label>
        <span></span>
    </div>
</div>