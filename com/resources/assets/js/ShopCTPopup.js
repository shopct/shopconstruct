import {ShopCTMat} from "./ShopCTMat";
import {ShopCTWaterfallGrid} from "./ShopCTWaterfallGrid";

export class ShopCTPopup {
    constructor(wrapperId) {
        this.wrapperId = wrapperId;
        this.wrapper = document.getElementById(wrapperId);
        if (!this.wrapper) {
            let newWrapper = document.createElement('div');
            newWrapper.id = wrapperId;

            document.body.appendChild(newWrapper);
            this.wrapper = newWrapper;
        }
        this.state = 0;
    }

    show(options) {
        this.css = {};

        if (!options.content) {
            this.content = this.wrapper.innerHTML;
        } else {
            this.content = options.content;
        }

        if (options.title) {
            this.title = options.title;
        }

        if (options.onClose) {
            this.onCloseCallback = options.onClose;
        }

        if (options.closing) {
            this.closingCallback = options.closing;
        }

        if (options.afterClose) {
            this.afterCloseCallback = options.afterClose;
        }

        if (options.onOpen) {
            this.onOpenCallback = options.onOpen;
        }

        if (options.actionButtons) {
            this.actionButtons = options.actionButtons;
        }

        if (options.contentLarge) {
            this.contentLarge = options.contentLarge;
        }

        this.popupElement = this.getElement();

        let window = this.popupElement.querySelector('.shop-ct-popup-window');

        if(options.style){
            Object.keys(options.style).map((key) => {
                console.log(key);
               window.style[key] = options.style[key];
            });
        }

        window.addEventListener('animationend', this.opened.bind(this));
        this.popupElement.querySelector('.shop-ct-popup-overlay').addEventListener('click', this.close.bind(this));

        let forms = this.popupElement.querySelectorAll('form');
        if(forms){
            forms.forEach((form) => {
                form.setAttribute('data-changed', 'no');
                form.setAttribute('data-submitted', 'no');

                form.addEventListener('submit',()=> {
                    form.setAttribute('data-submitted', 'yes');
                });

                for(let i = 0; i<form.elements.length;i++) {
                    form.elements[i].addEventListener('change',()=>{
                        form.setAttribute('data-changed', 'yes');
                    });
                }

            });
        }

        this.wrapper.replaceWith(this.popupElement);
        this.state = 1;
    }

    isShown() {
        return !!this.state;
    }

    opened() {
        new ShopCTMat(this.popupElement);

        let grids = this.popupElement.querySelectorAll('.shop-ct-popup-grid');

        if(grids.length){
            grids.forEach((grid)=>{
                new ShopCTWaterfallGrid({
                    container: grid,
                    boxSelector: '.shop-ct-grid-item',
                    minBoxWidth: 400
                })
            });
        }

        if (this.onOpenCallback) {
            this.onOpenCallback(this);
        }
    }

    close() {
        let canBeClosed = true;

        if (this.onCloseCallback) {
            let callBackResult = this.onCloseCallback(this);

            if (callBackResult === false) {
                canBeClosed = false;
            }
        }

        if (canBeClosed) {
            if(this.closingCallback) {
                this.closingCallback();
            }
            let window = this.popupElement.querySelector('.shop-ct-popup-window');

            window.addEventListener('animationend', () => {
                this.popupElement.replaceWith(this.wrapper);
                if(this.afterCloseCallback){
                    this.afterCloseCallback();
                }
            });

            this.popupElement.querySelector('.shop-ct-popup-overlay').remove();
            window.classList.add('shop-ct-popup--closing');
            this.state = 0;
        }
    }

    getElement() {
        let newElement = document.createElement('aside');

        newElement.classList.add('shop-ct-popup-outer');
        newElement.id = this.wrapperId;

        let titleHtml = '';

        if (this.title) {
            titleHtml = '<header class="shop-ct-popup-title-block">' +
                '<h2 class="shop-ct-popup-title">' + this.title + '</h2>' +
                '</header>';
        }

        let contentHtml = '<section class="shop-ct-popup-content' + (this.content ? ' shop-ct-popup-content--large' : '') + '">' + this.content + '</section>';


        let footerHtml = '';
        if (this.actionButtons) {
            let footerButtonsHtml = '';

            this.actionButtons.forEach((el) => {
                footerButtonsHtml += '<button class="shop-ct-mat-button ' + (el.class ? el.class : '') + '" id="' + el.id + '">' + el.text + '</button>';
            });

            footerHtml = '<footer class="shop-ct-popup-footer">' +
                footerButtonsHtml +
                '</footer>';
        }

        newElement.innerHTML = '<div class="shop-ct-popup-window">' +
            titleHtml +
            contentHtml +
            footerHtml +
            '</div>' +
            '<div class="shop-ct-popup-overlay"></div>';

        return newElement;
    }
}