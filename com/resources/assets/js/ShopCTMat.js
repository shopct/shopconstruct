export class ShopCTMat {
    constructor(wrapper){
        this.textFields = wrapper.querySelectorAll('.shop-ct-mat-input-text input');

        if(this.textFields.length){
            this.initTextFields();
        }
    }

    initTextFields() {
        this.textFields.forEach(function(textField) {
            textField.addEventListener('keyup', function() {
                this.setAttribute('value', this.value);

                if(this.value.length){
                    this.classList.add('not-empty');
                } else {
                    this.classList.remove('not-empty');
                }
            });
        })
    }
}