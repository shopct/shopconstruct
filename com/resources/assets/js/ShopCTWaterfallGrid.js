export class ShopCTWaterfallGrid {
    constructor(opts) {
        // define property
        let minBoxWidth;
        Object.defineProperty(this, 'minBoxWidth', {
            get: function() { return minBoxWidth; },
            set: function(value) {
                if(value < 100) value = 100;
                if(value > 1000) value = 1000;

                minBoxWidth = value;
            }
        });

        opts = opts || {};
        let containerSelector = opts.containerSelector || '.shop-ct-waterfall-grid-container';
        let boxSelector = opts.boxSelector || '.shop-ct-waterfall-grid-box';

        // init properties
        this.minBoxWidth = opts.minBoxWidth || 250;
        this.columns = [];
        this.container = opts.container || document.querySelector(containerSelector);
        this.boxes = this.container ?
            Array.prototype.slice.call(this.container.querySelectorAll(boxSelector)) : [];

        // compose once in constructor
        this.compose();

        // handle the image or something which might change size after loaded
        let images = this.container.querySelectorAll('img');
        let clr;
        for (let i = 0; i < images.length; i++) {
            let img = images[i];
            img.onload = () => {
                if(clr) clearTimeout(clr);

                clr = setTimeout(() => {
                    this.compose(true);
                }, 500);
            }
        }

        window.addEventListener('resize', ()=> {
            this.compose();
        });
    }

    computeNumberOfColumns() {
        let num = Math.floor(this.container.clientWidth / this.minBoxWidth);
        num = num || 1; // at least one column

        return num;
    }

    initColumns(num) {
        if(num > 0) {
            // create column div
            this.columns = [];
            let width = (100 / num) + '%';
            while(num--) {
                let column = document.createElement('div');
                column.className = 'shop-ct-waterfall-grid-column';
                column.style.width = width;
                this.columns.push(column);
                this.container.appendChild(column);
            }
        }
    }

    getMinHeightIndex() {
        if(this.columns && this.columns.length > 0) {
            let min = this.columns[0].clientHeight, index = 0;
            for (let i = 1; i < this.columns.length; i++) {
                let columnElem = this.columns[i];
                if(columnElem.clientHeight < min) {
                    min = columnElem.clientHeight;
                    index = i;
                }
            }
            return index;
        }
        else return -1;
    }

    getHighestIndex() {
        if(this.columns && this.columns.length > 0) {
            let max = this.columns[0].clientHeight, index = 0;
            for (let i = 1; i < this.columns.length; i++) {
                let columnElem = this.columns[i];
                if(columnElem.clientHeight > max) {
                    max = columnElem.clientHeight;
                    index = i;
                }
            }
            return index;
        }
        else return -1;
    }

    compose(force) {
        let num = this.computeNumberOfColumns();
        let cols = this.columns.length;

        if(force || num !== cols) {
            // remove old column
            for (let i = 0; i < this.columns.length; i++) {
                let columnElem = this.columns[i];
                this.container.removeChild(columnElem);
            }

            // init new column
            this.initColumns(num);

            // compose
            for (let i = 0, l = this.boxes.length; i < l; i++) {
                let box = this.boxes[i];
                this.addBox(box);
            }
        }
    }

    addBox(elem) {
        // push if new box
        if(this.boxes.indexOf(elem) < 0) this.boxes.push(elem);

        let columnIndex = this.getMinHeightIndex();
        if(columnIndex > -1) {
            let column = this.columns[columnIndex];
            column.appendChild(elem);
        }
    }
}