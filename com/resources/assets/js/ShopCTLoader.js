export class ShopCTLoader {

    static isShown(){
        return !!ShopCTLoader.state;
    }

    static show() {
        ShopCTLoader.renderHtml();

        document.body.appendChild(ShopCTLoader.element);

        ShopCTLoader.state = 1;
    }

    static renderHtml() {
        ShopCTLoader.element = document.createElement('div');
        ShopCTLoader.element.classList.add('shop-ct-mat-loader');

        ShopCTLoader.element.innerHTML = '<div class="shop-ct-mat-spinner">' +
            '<svg focusable="false" preserveAspectRatio="xMidYMid meet" viewBox="0 0 100 100" style="width: 100px; height: 100px;">' +
            '<circle cx="50%" cy="50%" r="45" style="animation-name: shop-ct-mat-progress-spinner-stroke-rotate-100; stroke-dasharray: 282.743px; stroke-width: 4%;"></circle>' +
            '</svg>' +
            '</div>' +
            '<div class="shop-ct-mat-loader-overlay"></div>';


    }

    static remove() {
        if(!ShopCTLoader.isShown()){
            return;
        }
        ShopCTLoader.element.remove();

        ShopCTLoader.state = 0;
    }
}