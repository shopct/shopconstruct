export class ShopCTToastr {
    static show(text, type){
        type= type||'success';
        text = text||'Success!';
        let existingToastrs = document.querySelectorAll('.shop-ct-toastr');

        if(existingToastrs.length >= 4){
            clearInterval(ShopCTToastr.toastInterval);
            ShopCTToastr.toastInterval = false;
            ShopCTToastr.remove();
        }

        let wrap = document.querySelector('.shop-ct-toastr-wrap');

        if(!wrap){
            wrap = document.createElement('div');
            wrap.classList.add('shop-ct-toastr-wrap');
            document.body.appendChild(wrap);
        }

        let newToast = document.createElement('div');
        newToast.classList.add('shop-ct-toastr', type);
        newToast.innerHTML = text;

        wrap.appendChild(newToast);

        if(!ShopCTToastr.toastInterval){
            ShopCTToastr.toastInterval = setInterval(function () {
                ShopCTToastr.remove();
            }, 3000);
        }
    }

    static remove(){
        let toasts = document.querySelectorAll('.shop-ct-toastr');

        if(toasts.length === 1){
            clearInterval(ShopCTToastr.toastInterval);
            ShopCTToastr.toastInterval = false;
        }

        let firstToastr = document.querySelector('.shop-ct-toastr');

        firstToastr.addEventListener('animationend', function() {
            this.remove();
        });
        firstToastr.classList.add('shop-ct-close-toastr');
    }
}