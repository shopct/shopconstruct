import {ShopCTTagPopup} from "../popups/ShopCTTagPopup";

export function shopCTTagsPage() {
    if(!document.getElementById('shop-ct-tags')) {
        return;
    }

    let addNewButtons = [...document.querySelectorAll('.shop-ct-add-new-tag')];

    addNewButtons.forEach((addNewButton) => {
        addNewButton.addEventListener('click', function (e) {
            e.preventDefault();

            (new ShopCTTagPopup(null, true)).open();
        });
    });
}