import {ShopCTLoadMore} from "../ShopCTLoadMore";
import {ShopCTOrderPopup} from "../popups/ShopCTOrderPopup";

export function shopCTOrdersPage() {
    if (!document.getElementById('shop-ct-orders')) {
        return;
    }

    if (document.querySelector('.shop-ct-load-more')) {
        new ShopCTLoadMore(document.getElementById('shop-ct-orders-list-wrap'), 'tbody', 'shop_ct_get_orders_page', shopCTOrdersPage);
    }

    let addNewButtons = [...document.querySelectorAll('.shop-ct-add-new-order')];

    addNewButtons.forEach((addNewButton) => {
        addNewButton.addEventListener('click', function(e) {
           e.preventDefault();

            (new ShopCTOrderPopup(null, true)).open();
        });
    });
}