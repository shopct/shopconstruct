import {ShopCTToastr} from "../../ShopCTToastr";
import {ShopCTLoader} from "../../ShopCTLoader";
import {shopCTProductsPage} from "./shopCTProductsPage";
import {ShopCTPageLoader} from "../ShopCTPageLoader";

export function shopCTReviewsPage() {
    if(!document.getElementById('shop-ct-reviews')) {
        return;
    }

    let updateStatusButtons = [...document.querySelectorAll('.shop-ct-update-review-status')];
    let removeButtons = [...document.querySelectorAll('.shop-ct-remove-review')];

    updateStatusButtons.forEach((updateStatusButton) => {
        updateStatusButton.addEventListener('click', updateReviewStatus)
    });

    removeButtons.forEach((removeButton) => {
        removeButton.addEventListener('click', deleteReview)
    });

    function deleteReview(e) {
        e.preventDefault();

        let id = this.closest('tr').getAttribute('data-review-id');

        let data = new FormData();

        data.append('nonce', shopCTL10n.nonce);
        data.append('action', 'shop_ct_delete_review');
        data.append('id', id);
        data.append('status', status);

        ShopCTLoader.show();

        fetch(shopCTL10n.ajaxUrl, {
            method: 'POST',
            credentials: 'same-origin',
            body: data
        }).then((response) => response.json())
            .then(result => {
                ShopCTLoader.remove();
                if (result.status) {
                    let pageUrl = shopCTL10n.ajaxUrl + window.location.search + '&action=shop_ct_get_reviews_page';
                    ShopCTPageLoader.load(pageUrl, shopCTReviewsPage);
                } else {
                    console.log(result);
                    ShopCTToastr.show(shopCTL10n.serverSideError, 'error');
                }
            });
    }

    function updateReviewStatus(e){
        e.preventDefault();

        let status = this.getAttribute('data-status');
        let id = this.closest('tr').getAttribute('data-review-id');

        let data = new FormData();

        data.append('nonce', shopCTL10n.nonce);
        data.append('action', 'shop_ct_update_review');
        data.append('id', id);
        data.append('status', status);

        ShopCTLoader.show();

        fetch(shopCTL10n.ajaxUrl, {
            method: 'POST',
            credentials: 'same-origin',
            body: data
        }).then((response) => response.json())
            .then(result => {
                ShopCTLoader.remove();
                if (result.status) {
                    let pageUrl = shopCTL10n.ajaxUrl + window.location.search + '&action=shop_ct_get_reviews_page';
                    ShopCTPageLoader.load(pageUrl, shopCTReviewsPage);
                } else {
                    console.log(result);
                    ShopCTToastr.show(shopCTL10n.serverSideError, 'error');
                }
            });
    }
}