import {ShopCTCategoryPopup} from "../popups/ShopCTCategoryPopup";
import {ShopCTLoader} from "../../ShopCTLoader";
import {shopCTProductsPage} from "./shopCTProductsPage";
import {ShopCTToastr} from "../../ShopCTToastr";
import {ShopCTPageLoader} from "../ShopCTPageLoader";

export function shopCTCategoriesPage(){
    if (!document.getElementById('shop-ct-categories')) {
        return;
    }

    let addNewButtons = [...document.querySelectorAll('.shop-ct-add-new-category')];
    let editButtons = [...document.querySelectorAll('.shop-ct-edit-category')];
    let removeButtons = [...document.querySelectorAll('.shop-ct-remove-category')];

    addNewButtons.forEach((addNewButton) => {
        addNewButton.addEventListener('click', function (e) {
            e.preventDefault();

            (new ShopCTCategoryPopup(null, true)).open();
        });
    });

    editButtons.forEach((editButton) => {
        editButton.addEventListener('click', function (e) {
            e.preventDefault();

            let categoryId = this.closest('tr').getAttribute('data-category-id');

            (new ShopCTCategoryPopup(categoryId, true)).open();
        });
    });

    removeButtons.forEach((removeButton) => {
       removeButton.addEventListener('click', remove);
    });

    function remove(e){
        e.preventDefault();

        if(!confirm(shopCTL10n.confirmRemoval)) {
            return;
        }

        let categoryId = this.closest('tr').getAttribute('data-category-id');

        ShopCTLoader.show();

        let data = new FormData();

        data.append('action', 'shop_ct_delete_category');
        data.append('nonce', shopCTL10n.nonce);
        data.append('id', categoryId);

        this.setAttribute("disabled", 'disabled');

        fetch(shopCTL10n.ajaxUrl, {
            method: 'POST',
            credentials: "same-origin",
            body: data,
        })
            .then(result => {
                this.removeAttribute("disabled", 'disabled');
                return result.json();
            })
            .then(result => {
                if (result.status) {
                    ShopCTToastr.show(shopCTL10n.categoryRemoved);
                    let pageUrl = shopCTL10n.ajaxUrl + window.location.search + '&action=shop_ct_get_categories_page';
                    ShopCTPageLoader.load(pageUrl, shopCTCategoriesPage);
                } else {
                    ShopCTLoader.remove();
                    ShopCTToastr.show(shopCTL10n.serverSideError, 'error');
                }
            })
            .catch(reason => {
                ShopCTLoader.remove();
            });
    }
}