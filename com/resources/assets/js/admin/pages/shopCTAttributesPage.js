import {ShopCTAttributePopup} from "../popups/ShopCTAttributePopup";
import {ShopCTToastr} from "../../ShopCTToastr";
import {ShopCTLoader} from "../../ShopCTLoader";
import {shopCTCategoriesPage} from "./shopCTCategoriesPage";
import {ShopCTPageLoader} from "../ShopCTPageLoader";

export function shopCTAttributesPage() {
    if (!document.getElementById('shop-ct-attributes')) {
        return;
    }

    let addNewButtons = [...document.querySelectorAll('.shop-ct-add-new-attribute')];
    let editButtons = [...document.querySelectorAll('.shop-ct-edit-attribute')];
    let removeButtons = [...document.querySelectorAll('.shop-ct-remove-attribute')];

    addNewButtons.forEach((addNewButton) => {
        addNewButton.addEventListener('click', function (e) {
            e.preventDefault();

            (new ShopCTAttributePopup(null, true)).open();
        })
    });

    editButtons.forEach((editButton) => {
        editButton.addEventListener('click', function (e) {
            e.preventDefault();

            let attributeId = this.closest('tr').getAttribute('data-attribute-id');

            (new ShopCTAttributePopup(attributeId, true)).open();
        })
    });

    removeButtons.forEach((removeButton) => {
        removeButton.addEventListener('click', function (e) {
            e.preventDefault();

            if (this.classList.contains('shop-ct-disabled')) {
                alert(shopCTL10n.cannotRemoveAttribute);
                return;
            }

            let attributeId = this.closest('tr').getAttribute('data-attribute-id');
            removeAttribute(attributeId);
        });
    });

    function removeAttribute(id) {
        if(!confirm(shopCTL10n.confirmRemoval)) {
            return;
        }

        let data = new FormData();

        data.append('action', 'shop_ct_delete_attribute');
        data.append('nonce', shopCTL10n.nonce);
        data.append('id', id);

        this.setAttribute("disabled", 'disabled');

        fetch(shopCTL10n.ajaxUrl, {
            method: 'POST',
            credentials: "same-origin",
            body: data,
        })
            .then(result => {
                this.removeAttribute("disabled", 'disabled');
                return result.json();
            })
            .then(result => {
                if (result.status) {
                    ShopCTToastr.show(shopCTL10n.attributeRemoved);
                    let pageUrl = shopCTL10n.ajaxUrl + window.location.search + '&action=shop_ct_get_attributes_page';
                    ShopCTPageLoader.load(pageUrl, shopCTAttributesPage);
                } else {
                    ShopCTLoader.remove();
                    ShopCTToastr.show(shopCTL10n.serverSideError, 'error');
                }
            })
            .catch(reason => {
                ShopCTLoader.remove();
            });
    }
}