import {ShopCTLoader} from "../../ShopCTLoader";
import {ShopCTProductPopup} from "../popups/ShopCTProductPopup";
import {ShopCTPageLoader} from "../ShopCTPageLoader";
import {ShopCTToastr} from "../../ShopCTToastr";
import {ShopCTLoadMore} from "../ShopCTLoadMore";

export function shopCTProductsPage() {
    if (!document.getElementById('shop-ct-products')) {
        return;
    }

    if (document.querySelector('.shop-ct-load-more')) {
        new ShopCTLoadMore(document.getElementById('shop-ct-products-list-wrap'), 'tbody', 'shop_ct_get_products_page', shopCTProductsPage);
    }

    let addNewButtons = [...document.querySelectorAll('.shop-ct-add-new-product')];
    let editButtons = [...document.querySelectorAll('.shop-ct-edit-product')];
    let trashButtons = [...document.querySelectorAll('.shop-ct-trash-product')];
    let copyShortcodeButtons = [...document.querySelectorAll('.shop-ct-copy-product-shortcode')];
    let restoreFromTrashButtons = [...document.querySelectorAll('.shop-ct-untrash-product')];
    let deleteForeverButtons = [...document.querySelectorAll('.shop-ct-remove-product')];


    if(restoreFromTrashButtons){
        restoreFromTrashButtons.forEach((restoreFromTrashButton) => {
           restoreFromTrashButton.addEventListener('click',restoreFromTrash);
        });
    }

    if(deleteForeverButtons){
        deleteForeverButtons.forEach((deleteForeverButton) => {
            deleteForeverButton.addEventListener('click',deleteForever);
        });
    }

    addNewButtons.forEach((addNewButton) => {
        addNewButton.addEventListener('click', function (e) {
            e.preventDefault();

            new ShopCTProductPopup(null, true);
        });
    });

    editButtons.forEach((element) => {
        element.addEventListener('click', function (e) {
            e.preventDefault();

            let productId = this.closest('tr').getAttribute('data-product-id');

            new ShopCTProductPopup(productId, true);
        });
    });

    copyShortcodeButtons.forEach((copyShortcodeButton) => {
        copyShortcodeButton.addEventListener('click', copyShortcode);
    });

    trashButtons.forEach((trashButton) => {
        trashButton.addEventListener('click', trashProduct);
    });

    function deleteForever(e){
        e.preventDefault();

        let productId = this.closest('tr').getAttribute('data-product-id');

        ShopCTLoader.show();

        let data = new FormData();

        data.append('action', 'shop_ct_delete_product');
        data.append('nonce', shopCTL10n.nonce);
        data.append('id', productId);

        this.setAttribute("disabled", 'disabled');

        fetch(shopCTL10n.ajaxUrl, {
            method: 'POST',
            credentials: "same-origin",
            body: data,
        })
            .then(result => {
                this.removeAttribute("disabled", 'disabled');
                return result.json();
            })
            .then(result => {
                if (result.status) {
                    ShopCTToastr.show(shopCTL10n.productSaved);
                    let pageUrl = shopCTL10n.ajaxUrl + window.location.search + '&action=shop_ct_get_products_page';
                    ShopCTPageLoader.load(pageUrl, shopCTProductsPage);
                } else {
                    ShopCTLoader.remove();
                    ShopCTToastr.show(shopCTL10n.serverSideError, 'error');
                }
            })
            .catch(reason => {
                ShopCTLoader.remove();
            });
    }

    function restoreFromTrash(e){
        e.preventDefault();

        let productId = this.closest('tr').getAttribute('data-product-id');

        ShopCTLoader.show();

        let data = new FormData();

        data.append('action', 'shop_ct_restore_product');
        data.append('nonce', shopCTL10n.nonce);
        data.append('id', productId);

        this.setAttribute("disabled", 'disabled');

        fetch(shopCTL10n.ajaxUrl, {
            method: 'POST',
            credentials: "same-origin",
            body: data,
        })
            .then(result => {
                this.removeAttribute("disabled", 'disabled')
                return result.json();
            })
            .then(result => {
                if (result.status) {
                    ShopCTToastr.show(shopCTL10n.productSaved);
                    let pageUrl = shopCTL10n.ajaxUrl + window.location.search + '&action=shop_ct_get_products_page';
                    ShopCTPageLoader.load(pageUrl, shopCTProductsPage);
                } else {
                    ShopCTLoader.remove();
                    ShopCTToastr.show(shopCTL10n.serverSideError);
                }
            })
            .catch(reason => {
                ShopCTLoader.remove();
            });
    }

    function copyShortcode(e) {
        e.preventDefault();

        this.parentNode.querySelector('input').select();

        try {
            document.execCommand('copy');
            ShopCTToastr.show(shopCTL10n.shortcodeCopied);
        } catch (err) {
            alert('please press Ctrl/Cmd+C to copy');
        }
    }

    function trashProduct(e) {
        e.preventDefault();

        let productId = this.closest('tr').getAttribute('data-product-id');

        ShopCTLoader.show();

        let data = new FormData();

        data.append('action', 'shop_ct_trash_product');
        data.append('nonce', shopCTL10n.nonce);
        data.append('id', productId);

        this.setAttribute("disabled", 'disabled');

        fetch(shopCTL10n.ajaxUrl, {
            method: 'POST',
            credentials: "same-origin",
            body: data,
        })
            .then(result => {
                this.removeAttribute("disabled", 'disabled')
                return result.json();
            })
            .then(result => {
                if (result.status) {
                    ShopCTToastr.show(shopCTL10n.productSaved);
                    let pageUrl = shopCTL10n.ajaxUrl + window.location.search + '&action=shop_ct_get_products_page';
                    ShopCTPageLoader.load(pageUrl, shopCTProductsPage);
                } else {
                    ShopCTLoader.remove();
                    ShopCTToastr.show(shopCTL10n.serverSideError);
                }
            })
            .catch(reason => {
                ShopCTLoader.remove();
            });
    }
}