import {ShopCTLoader} from "../ShopCTLoader";
import {ShopCTPageLoader} from "./ShopCTPageLoader";
import {shopCTProductsPage} from "./pages/shopCTProductsPage";

var listElm = document.querySelector('#infinite-list');

export class ShopCTLoadMore {
    constructor(wrap, listSelector, action, callback) {
        this.wrap = wrap;
        this.list = this.wrap.querySelector(listSelector);
        this.loadMoreButton = this.wrap.querySelector('.shop-ct-load-more');
        this.totalCount = parseInt(this.list.getAttribute('data-total-count'));
        this.loading = false;
        this.callback = callback;
        this.action = action;
        this.init();
    }

    init() {
        this.loadMoreButton.addEventListener('click', this.onClick.bind(this));
    }

    onClick(e) {
        e.preventDefault();
        if (!this.loading) {
            this.loadMore();
        }
    }

    loadMore() {
        this.loading = true;
        this.loadMoreButton.setAttribute('disabled', 'disabled');
        let paged = this.list.getAttribute('data-paged');

        if (paged < this.totalCount) {
            if (!paged) {
                paged = 2
            } else {
                paged++;
            }

            let url = shopCTL10n.ajaxUrl + window.location.search + '&action=' + this.action + '&paged=' + paged;

            ShopCTPageLoader.load(url, this.callback)
                .then(()=>{
                    ShopCTLoadMore.updateQueryStringParam('paged', paged);
                });
        } else {
            this.loadMoreButton.remove();
        }


    }

    static updateQueryStringParam(key, value) {
        let baseUrl = [location.protocol, '//', location.host, location.pathname].join('');
        let urlQueryString = document.location.search;
        let newParam = key + '=' + value;
        let params = '?' + newParam;

        // If the "search" string exists, then build params from it
        if (urlQueryString) {
            let keyRegex = new RegExp('([\?&])' + key + '[^&]*');
            // If param exists already, update it
            if (urlQueryString.match(keyRegex) !== null) {
                params = urlQueryString.replace(keyRegex, "$1" + newParam);
            } else { // Otherwise, add it to end of query string
                params = urlQueryString + '&' + newParam;
            }
        }
        window.history.replaceState({}, "", baseUrl + params);
    }
}