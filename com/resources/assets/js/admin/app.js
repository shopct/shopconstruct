import {ShopCTMat} from "../ShopCTMat";
import {shopCTProductsPage} from "./pages/shopCTProductsPage";
import {shopCTCategoriesPage} from "./pages/shopCTCategoriesPage";
import {shopCTTagsPage} from "./pages/shopCTTagsPage";
import {shopCTReviewsPage} from "./pages/shopCTReviewsPage";
import {shopCTAttributesPage} from "./pages/shopCTAttributesPage";
import {shopCTOrdersPage} from "./pages/shopCTOrdersPage";

shopCTProductsPage();
shopCTCategoriesPage();
shopCTTagsPage();
shopCTReviewsPage();
shopCTAttributesPage();
shopCTOrdersPage();

new ShopCTMat(document);