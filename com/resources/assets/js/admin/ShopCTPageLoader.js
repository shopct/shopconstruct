import {ShopCTLoader} from "../ShopCTLoader";

export class ShopCTPageLoader {
    static load(url, callback) {

        return new Promise((resolve, reject) => {
            if(!ShopCTLoader.isShown()){
                ShopCTLoader.show();
            }


            fetch(url, {
                method: 'GET',
                credentials: 'same-origin',
            })
                .then((response) => {
                    ShopCTLoader.remove();
                    return response.json();
                })
                .then(result => {
                    if (result.status) {
                        let wrap = document.querySelector('.shop-ct');

                        wrap.innerHTML = result.data;

                        if(callback){
                            callback();
                            resolve();
                        }
                    }
                });
            })
    }
}