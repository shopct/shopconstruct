import {ShopCTPopup} from "../../ShopCTPopup";
import {ShopCTLoader} from "../../ShopCTLoader";
import {ShopCTToastr} from "../../ShopCTToastr";
import {ShopCTPageLoader} from "../ShopCTPageLoader";
import {shopCTTagsPage} from "../pages/shopCTTagsPage";

export class ShopCTTagPopup {
    constructor(id = null, isTagsPage = false) {
        this.id = id;
        this.popup = new ShopCTPopup('shop-ct-tag-popup-wrapper');
        this.isTagsPage = isTagsPage;

        if (!this.id) {
            this.setNewTagPopupOptions();
        } else {
            this.setEditTagPopupOptions(this.id);
        }
    }

    open() {
        let url = shopCTL10n.ajaxUrl + '?action=shop_ct_edit_tag';

        if (this.id) {
            url += '&id=' + this.id;
        }

        ShopCTLoader.show();

        fetch(url, {
            method: 'GET',
            credentials: 'same-origin'
        }).then((response) => response.json())
            .then(result => {
                ShopCTLoader.remove();
                if (result.status) {
                    this.popupOptions.content = result.data;
                    this.popup.show(this.popupOptions);
                } else {
                    console.log(response);
                    ShopCTToastr.show(shopCTL10n.serverSideError);
                }
            });
    }

    onOpen() {
        this.form = this.popup.popupElement.querySelector('#shop-ct-tag-popup-form');
        this.saveButton = this.popup.popupElement.querySelector('#shop-ct-save-tag');
        this.cancelButton = this.popup.popupElement.querySelector('#shop-ct-cancel-tag');
        this.saveButton = this.popup.popupElement.querySelector('#shop-ct-save-tag');

        this.attachEventListeners();
    }

    attachEventListeners() {
        this.cancelButton.addEventListener('click', this.cancel.bind(this));
        this.form.addEventListener('submit', (e) => {
            e.preventDefault()
        });

        this.saveButton.addEventListener('click', this.save.bind(this));
    }

    save(e) {
        e.preventDefault();

        let formData = new FormData(this.form);

        ShopCTLoader.show();

        this.saveButton.setAttribute("disabled", 'disabled');

        fetch(shopCTL10n.ajaxUrl, {
            method: 'POST',
            credentials: 'same-origin',
            body: formData
        }).then((response) => {
            this.saveButton.removeAttribute("disabled");
            return response.json();
        })
            .then(result => {
                if (result.status) {
                    ShopCTToastr.show(shopCTL10n.productSaved);
                    this.form.setAttribute('data-submitted', 'yes');
                    this.popup.close();
                    if (this.isTagsPage) {
                        let pageUrl = shopCTL10n.ajaxUrl + window.location.search + '&action=shop_ct_get_tags_page';
                        ShopCTPageLoader.load(pageUrl, shopCTTagsPage);
                    } else {
                        ShopCTLoader.remove();
                    }

                } else {
                    ShopCTLoader.remove();
                    if(result.errors){
                        ShopCTToastr.show(result.errors.join(','), 'error');
                    }

                }
            }, (reason) => {
                ShopCTLoader.remove();
            })
            .catch((reason) => {
                ShopCTLoader.remove();
            });
    }

    cancel(e) {
        e.preventDefault();

        this.popup.close();
    }

    onClose() {

    }

    afterClose() {

    }

    setNewTagPopupOptions() {
        this.popupOptions = {
            title: shopCTL10n.newTag,
            contentLarge: true,
            style: {
                width: '500px',
            },
            actionButtons: [
                {
                    id: 'shop-ct-cancel-tag',
                    text: shopCTL10n.cancel,
                },
                {
                    id: 'shop-ct-save-tag',
                    class: 'shop-ct-mat-button--raised shop-ct-mat-button--primary',
                    text: shopCTL10n.publish,
                }
            ],
            onOpen: this.onOpen.bind(this),
            onClose: this.onClose.bind(this),
            afterClose: this.afterClose.bind(this)
        };
    }

    setEditTagPopupOptions() {
        this.popupOptions = {
            title: shopCTL10n.editingTag,
            contentLarge: true,
            style: {
                width: '500px',
            },
            actionButtons: [
                {
                    id: 'shop-ct-cancel-tag',
                    //text: '<i class="material-icons">delete</i>'
                    text: shopCTL10n.cancel,
                },
                {
                    id: 'shop-ct-save-tag',
                    class: 'shop-ct-mat-button--raised shop-ct-mat-button--primary',
                    text: shopCTL10n.publish,
                }
            ],
            onOpen: this.onOpen.bind(this),
            onClose: this.onClose.bind(this),
            afterClose: this.afterClose.bind(this)
        };
    }
}