import {ShopCTPopup} from "../../ShopCTPopup";
import {ShopCTToastr} from "../../ShopCTToastr";
import {ShopCTLoader} from "../../ShopCTLoader";
import {shopCTCategoriesPage} from "../pages/shopCTCategoriesPage";
import {ShopCTPageLoader} from "../ShopCTPageLoader";
import {shopCTAttributesPage} from "../pages/shopCTAttributesPage";

export class ShopCTAttributePopup {
    constructor(id = null, isAttributesPage = false) {
        this.id = id;
        this.popup = new ShopCTPopup('shop-ct-category-popup-wrapper');
        this.isAttributesPage = isAttributesPage;

        if (!this.id) {
            this.setNewAttributePopupOptions();
        } else {
            this.setEditAttributePopupOptions(this.id);
        }
    }

    open() {
        let url = shopCTL10n.ajaxUrl + '?action=shop_ct_edit_attribute';

        if (this.id) {
            url += '&id=' + this.id;
        }

        ShopCTLoader.show();

        fetch(url, {
            method: 'GET',
            credentials: 'same-origin'
        }).then((response) => response.json())
            .then(result => {
                ShopCTLoader.remove();
                if (result.status) {
                    this.popupOptions.content = result.data;
                    this.popup.show(this.popupOptions);
                } else {
                    console.log(response);
                    ShopCTToastr.show(shopCTL10n.serverSideError);
                }
            });
    }

    onOpen() {
        this.form = this.popup.popupElement.querySelector('#shop-ct-attribute-popup-form');
        this.cancelButton = this.popup.popupElement.querySelector('#shop-ct-cancel-attribute');
        this.addNewTermButton = this.popup.popupElement.querySelector('.shop-ct-add-new-attribute-term');
        this.termsTableBody = this.popup.popupElement.querySelector('#shop-ct-attribute-terms');
        this.termRow = this.termsTableBody.getAttribute('data-row');
        this.termsEmptyRow = this.termsTableBody.getAttribute('data-empty-row');
        this.saveButton = this.popup.popupElement.querySelector('#shop-ct-save-attribute');

        this.attachEventListeners();
    }

    attachEventListeners() {
        this.cancelButton.addEventListener('click', this.cancel.bind(this));
        this.addNewTermButton = this.addNewTermButton.addEventListener('click', this.addTerm.bind(this));
        this.termsTableBody.addEventListener('click', this.maybeRemoveTermRow.bind(this));
        this.saveButton.addEventListener('click', this.save.bind(this));
    }

    save(e) {
        e.preventDefault();


        let formData = new FormData(this.form);

        ShopCTLoader.show();

        this.saveButton.setAttribute("disabled", 'disabled');

        fetch(shopCTL10n.ajaxUrl, {
            method: 'POST',
            credentials: 'same-origin',
            body: formData
        }).then((response) => {
            this.saveButton.removeAttribute("disabled");
            return response.json();
        })
            .then(result => {

                if (result.status) {
                    ShopCTToastr.show(shopCTL10n.attributeSaved);
                    this.form.setAttribute('data-submitted', 'yes');
                    this.popup.close();
                    if (this.isAttributesPage) {
                        let pageUrl = shopCTL10n.ajaxUrl + window.location.search + '&action=shop_ct_get_attributes_page';
                        ShopCTPageLoader.load(pageUrl, shopCTAttributesPage);
                    } else {
                        ShopCTLoader.remove();
                    }

                } else {
                    ShopCTLoader.remove();
                    if (result.errors) {
                        ShopCTToastr.show(result.errors.join(','), 'error');
                    }

                }
            }, (reason) => {
                ShopCTLoader.remove();
            })
            .catch((reason) => {
                ShopCTLoader.remove();
            });
    }

    maybeRemoveTermRow(e) {
        e.preventDefault();

        if (!e.target.parentNode.classList.contains('shop-ct-remove-attribute-term')) {
            return;
        }

        if (e.target.parentNode.hasAttribute('disabled') || e.target.parentNode.classList.contains('shop-ct-disabled')) {
            return;
        }

        e.target.closest('tr:not(.shop-ct-attribute-terms-empty)').remove();

        if (!this.termsTableBody.querySelector('tr')) {
            this.termsTableBody.innerHTML = this.termsEmptyRow;
        }
    }


    addTerm(e) {
        e.preventDefault();

        let emptyRow = this.termsTableBody.querySelector('.shop-ct-attribute-terms-empty');

        if (emptyRow) {
            emptyRow.remove();
        }

        this.termsTableBody.insertAdjacentHTML('afterbegin', this.termRow);
    }

    onClose() {

    }

    afterClose() {

    }

    cancel(e) {
        e.preventDefault();

        this.popup.close();
    }

    setNewAttributePopupOptions() {
        this.popupOptions = {
            title: shopCTL10n.newAttribute,
            contentLarge: true,
            style: {
                width: '500px',
                height: '448px',
            },
            actionButtons: [
                {
                    id: 'shop-ct-cancel-attribute',
                    text: shopCTL10n.cancel,
                },
                {
                    id: 'shop-ct-save-attribute',
                    class: 'shop-ct-mat-button--raised shop-ct-mat-button--primary',
                    text: shopCTL10n.publish,
                }
            ],
            onOpen: this.onOpen.bind(this),
            onClose: this.onClose.bind(this),
            afterClose: this.afterClose.bind(this)
        };
    }

    setEditAttributePopupOptions() {
        this.popupOptions = {
            title: shopCTL10n.editingAttribute,
            contentLarge: true,
            style: {
                width: '500px',
                height: '448px',
            },
            actionButtons: [
                {
                    id: 'shop-ct-cancel-attribute',
                    //text: '<i class="material-icons">delete</i>'
                    text: shopCTL10n.cancel,
                },
                {
                    id: 'shop-ct-save-attribute',
                    class: 'shop-ct-mat-button--raised shop-ct-mat-button--primary',
                    text: shopCTL10n.save,
                }
            ],
            onOpen: this.onOpen.bind(this),
            onClose: this.onClose.bind(this),
            afterClose: this.afterClose.bind(this)
        };
    }
}