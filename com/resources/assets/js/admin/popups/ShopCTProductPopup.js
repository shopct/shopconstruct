import {ShopCTLoader} from "../../ShopCTLoader";
import {ShopCTPopup} from "../../ShopCTPopup";
import {ShopCTToastr} from "../../ShopCTToastr";
import Sortable from "../../Sortable";
import Pikaday from "../../Pikaday";
import {ShopCTPageLoader} from "../ShopCTPageLoader";
import {shopCTProductsPage} from "../pages/shopCTProductsPage";
import {ShopCTCategoryPopup} from "./ShopCTCategoryPopup";

export class ShopCTProductPopup {
    constructor(id = null, isProductsPage = false) {
        this.id = id;
        this.popup = new ShopCTPopup('shop-ct-product-popup-wrapper');
        let url = shopCTL10n.ajaxUrl + '?action=shop_ct_edit_product';

        if (!id) {
            this.setNewProductPopupOptions();
        } else {
            this.setEditProductPopupOptions(id);
            url += '&id=' + id;
        }

        this.isProductsPage = isProductsPage;


        ShopCTLoader.show();

        fetch(url, {
            method: 'GET',
            credentials: 'same-origin'
        }).then((response) => response.json())
            .then(result => {
                ShopCTLoader.remove();
                if (result.status) {
                    this.popupOptions.content = result.data;
                    this.popup.show(this.popupOptions);
                } else {
                    console.log(response);
                    ShopCTToastr.show(shopCTL10n.serverSideError);
                }
            });
    }

    onOpen() {
        this.initContentEditor();
        this.form = this.popup.popupElement.querySelector('#shop-ct-product-popup-form');
        this.saveButton = this.popup.popupElement.querySelector('#shop-ct-save-product');
        this.cancelButton = this.popup.popupElement.querySelector('#shop-ct-cancel-product');
        this.trashButton = this.popup.popupElement.querySelector('#shop-ct-trash-product');
        this.productId = parseInt(this.popup.popupElement.querySelector('#product_id').value);
        this.scheduleSaleButton = this.popup.popupElement.querySelector('.product-schedule-sale');
        this.datesWrap = this.popup.popupElement.querySelector('.product-sale-dates');
        this.salePriceInput = this.popup.popupElement.querySelector('#product_sale_price');
        this.regularPriceInput = this.popup.popupElement.querySelector('#product_regular_price');
        this.imageGalleryList = this.popup.popupElement.querySelector('.product-image-gallery');
        this.addCategoryButton = this.popup.popupElement.querySelector('.shop-ct-add-cat-btn');
        this.addTagButton = this.popup.popupElement.querySelector('.product-tags-add');
        this.addTagInput = this.popup.popupElement.querySelector('#product_tags_input');
        this.tagList = this.popup.popupElement.querySelector('.product-tags-list');
        this.addAttributeButton = this.popup.popupElement.querySelector('.product-add-attribute');
        this.newAttributeTaxonomy = this.popup.popupElement.querySelector('#product-new-attribute-taxonomy');
        this.attributesList = this.popup.popupElement.querySelector('.product-attributes-list');
        this.filesList = this.popup.popupElement.querySelector('.shop-ct-files-sortable');
        this.addFileButton = this.popup.popupElement.querySelector('.product-add-downloadable-file');
        this.editSlug = this.popup.popupElement.querySelector('.edit-slug');
        this.productOptionsHandles = [
            '#product_downloadable',
            '#product_virtual',
            '#product_manage_stock',
        ];

        this.initDatepickers();
        this.attachEventListeners();
        this.imageGallerySortable();
    }

    attachEventListeners() {
        if (this.cancelButton) {
            this.cancelButton.addEventListener('click', this.cancel.bind(this));
        }
        if(this.trashButton){
            this.trashButton.addEventListener('click', this.trash.bind(this));
        }
        this.form.addEventListener('submit', (e) => {
            e.preventDefault()
        });
        this.saveButton.addEventListener('click', this.save.bind(this));
        this.scheduleSaleButton.addEventListener('click', this.scheduleSale.bind(this));
        this.popup.popupElement.querySelector('.product-image-gallery-add').addEventListener('click', this.addImageGalleryItem.bind(this));
        this.addCategoryButton.addEventListener('click', this.addCategory.bind(this));
        this.addTagButton.addEventListener('click', this.addTags.bind(this));
        this.addTagInput.addEventListener('keypress', (e) => {
            if (13 === e.which) {
                this.addTags(e);
            }
        });
        this.tagList.addEventListener('click', (e) => {
            if (e.target.classList.has('product-tag-item-delete')) {
                this.removeTag(e);
            }
        });
        this.addAttributeButton.addEventListener('click', this.addAttribute.bind(this));
        this.addFileButton.addEventListener('click', this.addFile.bind(this));
        this.handleProductOptions();
        window.addEventListener('beforeunload', this.beforePageUnload.bind(this));

        if(this.editSlug){
            this.editSlug.addEventListener('click', this.editPermalink);
        }
    }

    save(e) {
        e.preventDefault();

        this.popup.popupElement.querySelector('#product_content').value = wp.editor.getContent('product_content');

        let formData = new FormData(this.form);

        ShopCTLoader.show();

        this.saveButton.setAttribute("disabled", 'disabled');

        fetch(shopCTL10n.ajaxUrl, {
            method: 'POST',
            credentials: 'same-origin',
            body: formData
        }).then((response) => {
            this.saveButton.removeAttribute("disabled");
            return response.json();
        })
            .then(result => {

                if (result.status) {
                    ShopCTToastr.show(shopCTL10n.productSaved);
                    this.form.setAttribute('data-submitted', 'yes');
                    this.popup.close();
                    if (this.isProductsPage) {
                        let pageUrl = shopCTL10n.ajaxUrl + window.location.search + '&action=shop_ct_get_products_page';
                        ShopCTPageLoader.load(pageUrl, shopCTProductsPage);
                    } else {
                        ShopCTLoader.remove();
                    }

                } else {
                    ShopCTLoader.remove();
                    if(result.errors){
                        ShopCTToastr.show(result.errors.join(','), 'error');
                    }

                }
            }, (reason) => {
                ShopCTLoader.remove();
            })
            .catch((reason) => {
                ShopCTLoader.remove();
            });
    }

    trash(e) {
        e.preventDefault();

        this.trashButton.setAttribute("disabled", 'disabled');
        ShopCTLoader.show();

        let data = new FormData();

        data.append('action', 'shop_ct_trash_product');
        data.append('nonce', shopCTL10n.nonce);
        data.append('id', this.productId);

        fetch(shopCTL10n.ajaxUrl, {
            method: 'POST',
            credentials: "same-origin",
            body: data,
        })
            .then(result => {
                this.trashButton.removeAttribute("disabled");
                return result.json();
            })
            .then(result => {
                if(result.status) {
                    ShopCTToastr.show(shopCTL10n.productTrashed);
                    this.form.setAttribute('data-submitted', 'yes');
                    this.popup.close();
                    if (this.isProductsPage) {
                        let pageUrl = shopCTL10n.ajaxUrl + window.location.search + '&action=shop_ct_get_products_page';
                        ShopCTPageLoader.load(pageUrl, shopCTProductsPage);
                    } else {
                        ShopCTLoader.remove();
                    }
                } else {
                    ShopCTLoader.remove();
                    ShopCTToastr.show(shopCTL10n.serverSideError);
                }
            })
            .catch(reason => {
                ShopCTLoader.remove();
            });
    }

    cancel(e) {
        e.preventDefault();

        this.popup.close();
    }

    initContentEditor() {
        wp.editor.initialize('product_content', {
            tinymce: {
                wpautop: true
            },
            quicktags: true
        });
    }

    initDatepickers() {
        let productDatePickers = [];

        productDatePickers['from'] = new Pikaday({
            field: this.popup.popupElement.querySelector('#product_sale_price_dates_from'),
            format: 'YYYY-MM-DD',
            onSelect: function (date) {
                productDatePickers['to'].setMinDate(date)
            }
        });

        productDatePickers['to'] = new Pikaday({
            field: this.popup.popupElement.querySelector('#product_sale_price_dates_to'),
            format: 'YYYY-MM-DD',
            onSelect: function (date) {
                productDatePickers['from'].setMaxDate(date)
            }
        });

        this.datepickers = productDatePickers;
    }

    editPermalink() {
        var $ = jQuery,
            postId = jQuery('#product_id').val(),
            i, slug_value,
            $el, revert_e,
            c = 0,
            real_slug = jQuery('#product_name'),
            revert_slug = real_slug.val(),
            permalink = jQuery('#sample-permalink'),
            permalinkOrig = permalink.html(),
            permalinkInner = jQuery('#sample-permalink a').html(),
            buttons = jQuery('#edit-slug-buttons'),
            buttonsOrig = buttons.html(),
            full = jQuery('#editable-post-name-full');

        // Deal with Twemoji in the post-name
        full.find('img').replaceWith(function () {
            return this.alt;
        });
        full = full.html();

        permalink.html(permalinkInner);
        $el = jQuery('#editable-post-name');
        revert_e = $el.html();

        buttons.html('<button type="button" class="save button button-small">' + shopCTL10n.ok + '</button> <button type="button" class="cancel button-link">' + shopCTL10n.cancel + '</button>');
        buttons.children('.save').click(function () {
            var new_slug = $el.children('input').val();

            if (new_slug === jQuery('#editable-post-name-full').text()) {
                buttons.children('.cancel').click();
                return;
            }
            jQuery.post(shopCTL10n.ajaxUrl, {
                action: 'sample-permalink',
                post_id: postId,
                new_slug: new_slug,
                new_title: jQuery('#product_title').val(),
                samplepermalinknonce: jQuery('#samplepermalinknonce').val()
            }, function (data) {
                var box = jQuery('#edit-slug-box');
                box.html(data);
                if (box.hasClass('hidden')) {
                    box.fadeIn('fast', function () {
                        box.removeClass('hidden');
                    });
                }

                buttons.html(buttonsOrig);
                permalink.html(permalinkOrig);
                real_slug.val(new_slug);
                jQuery('.edit-slug').focus();
            });
        });

        buttons.children('.cancel').click(function () {
            jQuery('#view-post-btn').show();
            $el.html(revert_e);
            buttons.html(buttonsOrig);
            permalink.html(permalinkOrig);
            real_slug.val(revert_slug);
            jQuery('.edit-slug').focus();
        });

        for (i = 0; i < full.length; ++i) {
            if ('%' == full.charAt(i))
                c++;
        }

        slug_value = (c > full.length / 4) ? '' : full;
        $el.html('<input type="text" id="new-post-slug" value="' + slug_value + '" autocomplete="off" />').children('input').keydown(function (e) {
            var key = e.which;
            // On enter, just save the new slug, don't save the post.
            if (13 === key) {
                e.preventDefault();
                buttons.children('.save').click();
            }
            if (27 === key) {
                buttons.children('.cancel').click();
            }
        }).keyup(function () {
            real_slug.val(this.value);
        }).focus();
    }

    showHideProductOptions(e) {
        let isVirtual = jQuery('#product_virtual:checked').size();
        let isDownloadable = jQuery('#product_downloadable:checked').size();
        let managingStock = jQuery('#product_manage_stock:checked').size();

        let hideClasses = '.hide_if_downloadable, .hide_if_virtual';
        let showClasses = '.show_if_downloadable, .show_if_virtual, .show_if_managing_stock';

        jQuery(hideClasses).css('display', 'block');
        jQuery(showClasses).css('display', 'none');

        if (isDownloadable) {
            jQuery('.show_if_downloadable').css('display', 'block');
            jQuery('.hide_if_downloadable').css('display', 'none');
        }
        if (isVirtual) {
            jQuery('.show_if_virtual').css('display', 'block');
            jQuery('.hide_if_virtual').css('display', 'none');
        }

        if (managingStock) {
            jQuery('.show_if_managing_stock').css('display', 'block');
        }
    }

    handleProductOptions() {
        this.productOptionsHandles.forEach((id) => {
            this.popup.popupElement.querySelector(id).addEventListener('change', this.showHideProductOptions.bind(this));
        });
        this.showHideProductOptions();
    }

    filesSortable() {
        this.sortable = new Sortable(this.filesList, {
            handle: ".sort"
        });
    }

    addFile(e) {
        e.preventDefault();

        let row = e.target.getAttribute('data-row');

        let fakeElement = document.createElement('tbody');
        fakeElement.innerHTML = row;

        fakeElement.querySelector('.remove').addEventListener('click', this.removeFile);
        fakeElement.querySelector('.upload_button').addEventListener('click', this.chooseFile);

        let tbody = e.target.closest('table').querySelector('tbody');
        let noItems = tbody.querySelector('tr.no-items');

        if (noItems) {
            noItems.remove();
        }

        tbody.prepend(fakeElement.firstChild);

        this.filesSortable();
    }

    chooseFile(e) {
        e.preventDefault();

        let uploader = wp.media({
            title: shopCTL10n.addDownloadableFile,
            button: {
                text: shopCTL10n.insertFile
            },
            multiple: false
        })
            .on('select', function () {
                let attachment = uploader.state().get('selection').first().toJSON();
                e.target.parentElement.querySelector("input[type='url']").value = attachment.url;
                if (!e.target.parentElement.parentElement.querySelector("input[type='text']").value.length) {
                    e.target.parentElement.parentElement.querySelector("input[type='text']").value = attachment.name;
                }

            })
            .open();
    }

    removeFile(e) {
        e.preventDefault();
        if ([...this.closest('tbody').querySelectorAll('tr')].length === 1) {
            let noItems = document.createElement('tr');
            noItems.classList.add('no-items');
            noItems.innerHTML = '<td colspan="7">' + shopCTL10n.noFiles + '</td>';
            this.closest('tbody').appendChild(noItems);
        }
        this.closest('tr').remove();
    }

    addAttribute(e) {
        e.preventDefault();

        fetch(shopCTL10n.ajaxUrl + '?action=shop_ct_edit_product_attribute&product_id=' + this.productId + '&attribute_taxonomy=' + this.newAttributeTaxonomy.value, {
            method: 'GET',
            credentials: 'same-origin'
        }).then((response) => response.json())
            .then(result => {
                if (result.status) {
                    let fakeElement = document.createElement('div');
                    fakeElement.innerHTML = result.data;
                    fakeElement.querySelector('.product-attributes-delete').addEventListener('click', this.deleteAttribute.bind(this));
                    this.attributesList.appendChild(fakeElement.firstChild);
                } else {
                    console.log(response);
                    ShopCTToastr.show(shopCTL10n.serverSideError);
                }
            });
    }

    deleteAttribute(e) {
        e.preventDefault();
        e.currentTarget.parentElement.remove();
    }

    addTags(e) {
        e.preventDefault();

        let text = this.addTagInput.value;

        if ('undefined' === typeof(text) || '' === text) {
            return false;
        }

        let newTags = this.cleanTags(text).split(','),
            tags = this.getTags();

        if (!newTags.length) {
            return false;
        }

        tags = tags ? tags.concat(newTags) : newTags;
        tags = this.arrayUniqueNoempty(tags);
        this.buildTags(tags);
        this.addTagInput.value = '';

    }

    cleanTags(tags) {
        return tags.replace(/\s*,\s*/g, ',').replace(/,+/g, ',').replace(/[,\s]+$/, '').replace(/^[,\s]+/, '');
    }

    removeTag(e) {
        e.preventDefault();
        e.target.parent.remove();
    }

    getTags() {
        /**
         * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax
         */
        return [...this.popup.popupElement.querySelectorAll('.product-tags-list input')].map(function (el) {
            return el.value;
        });
    }

    arrayUniqueNoempty(array) {
        let out = [];
        array.forEach(function (val) {
            val = val.trim();

            if (val && !out.includes(val)) {
                out.push(val);
            }
        });

        return out;
    }

    buildTags(tags) {
        this.tagList.innerHTML = '';

        tags.forEach((tag) => {
            let newTagElement = document.createElement('span');
            newTagElement.classList.add('product-tag-item');
            newTagElement.innerHTML = '<input type="hidden" name="product_tags[]" value="' + tag + '" />' +
                '<span class="product-tag-item-name">' + tag + '</span>' +
                '<span class="product-tag-item-delete"><i class="material-icons">close</i></span>';
            this.tagList.appendChild(newTagElement);
        });
    }

    addCategory(e) {
        e.preventDefault();

        let popup = new ShopCTCategoryPopup(null, false);

        popup.popupOptions.afterClose = this.afterCategoryPopupClose.bind(this);

        popup.open();
    }

    afterCategoryPopupClose() {
        fetch(shopCTL10n.ajaxUrl + '?action=shop_ct_get_category_checklist&product_id='+this.id, {
            method: 'GET',
            credentials: 'same-origin'
        }).then((response) => response.json())
            .then(result => {
                if (result.status) {
                    this.popup.popupElement.querySelector('.shop-ct-product-cat-checklist-items').innerHTML = result.data;
                } else {
                    console.log(response);
                    ShopCTToastr.show(shopCTL10n.serverSideError);
                }
            });
    }

    onClose() {
        return this.validateClosingAction();
    }

    afterClose() {
        wp.editor.remove('product_content');
        window.removeEventListener('beforeunload', this.beforePageUnload);
    }

    closing() {
        for (let key in this.datepickers) {
           this.datepickers[key].destroy();
        }
    }

    addImageGalleryItem() {
        let customUploader = wp.media({
            title: 'Add Images to Product Gallery',
            library: {
                type: 'image'
            },
            button: {
                text: 'Add to gallery'
            },
            multiple: true  // Set this to true to allow multiple files to be selected
        })
            .on('select', () => {
                let attachments = customUploader.state().get('selection').toJSON();
                attachments.forEach((attachment) => {
                    let newItem = document.createElement('li');
                    newItem.classList.add('product-image-gallery-item');
                    newItem.innerHTML = '<div class="product-image-gallery-inner">' +
                        '<img src="' + attachment.url + '" alt="" />' +
                        '<button class="product-image-gallery-delete"><span class="material-icons">delete</span></button>' +
                        '<input type="hidden" name="post_meta[product_image_gallery][]" id="post_meta[product_image_gallery][]" value="' + attachment.id + '" />' +
                        '</div>';


                    newItem.querySelector('.product-image-gallery-delete').addEventListener('click', this.deleteImageGalleryItem);

                    this.imageGalleryList.appendChild(newItem);

                });
                this.imageGallerySortable();
            }).open();
        return false;
    }

    deleteImageGalleryItem(e) {
        e.preventDefault();
        this.parentElement.parentElement.remove();
    }

    imageGallerySortable() {
        this.sortable = new Sortable(this.imageGalleryList, {
            filter: ".product-image-gallery-add"
        });
    }

    validateClosingAction() {
        let container = this.popup.popupElement.querySelector('#shop-ct-product-popup-form');
        let isAutoDraft = this.popup.popupElement.querySelector("#product_autodraft").value;
        let postId;

        if (container.getAttribute("data-changed") === 'yes' && container.getAttribute("data-submitted") !== 'yes') {
            if (!confirm(shopCTL10n.popupAlert)) {
                return false;
            } else {
                if (isAutoDraft === '1') {
                    postId = this.popup.popupElement.querySelector('#product_id').value;
                    this.sendDeleteProductRequest(postId, true);
                }
            }
        } else {
            if (container.getAttribute("data-submitted") !== 'yes' && isAutoDraft === '1') {
                postId = this.popup.popupElement.querySelector('#product_id').value;
                this.sendDeleteProductRequest(postId, true);
            }
        }
    }


    scheduleSale(e) {
        e.preventDefault();
        if (this.datesWrap.classList.contains('shop-ct-hide')) {
            let salePrice = parseFloat(this.salePriceInput.value);
            let regularPrice = parseFloat(this.regularPriceInput.value);
            if (!salePrice || isNaN(salePrice) || salePrice >= regularPrice) {
                ShopCTToastr.show(shopCTL10n.invalidSalePrice, 'error');
                return false;
            }

            this.scheduleSaleButton.innerHTML = this.scheduleSaleButton.getAttribute('data-remove');
            this.datesWrap.classList.remove('shop-ct-hide');
        } else {
            this.popup.popupElement.querySelector('#product_sale_price_dates_from').value = '';
            this.popup.popupElement.querySelector('#product_sale_price_dates_to').value = '';
            this.popup.popupElement.querySelector('#product_sale_price_dates_from_hours').value = '00';
            this.popup.popupElement.querySelector('#product_sale_price_dates_from_minutes').value = '00';
            this.popup.popupElement.querySelector('#product_sale_price_dates_to_hours').value = '00';
            this.popup.popupElement.querySelector('#product_sale_price_dates_to_minutes').value = '00';
            this.scheduleSaleButton.innerHTML = this.scheduleSaleButton.getAttribute('data-original');
            this.datesWrap.classList.add('shop-ct-hide');
        }
    }

    beforePageUnload(e) {
        if(this.popup.isShown()){
            let container = this.popup.popupElement.querySelector('#shop-ct-product-popup-form');
            if (container.getAttribute("data-changed") === 'yes' && container.getAttribute("data-submitted") !== 'yes') {
                e.returnValue = shopCTL10n.popupAlert;
            }
        }
    }

    sendDeleteProductRequest(id, forceDelete = false) {
        let data = new FormData();
        data.append('action', 'shop_ct_delete_product');
        data.append('nonce', shopCTL10n.nonce);
        data.append('id', id);
        data.append('forceDelete', forceDelete);

        fetch(shopCTL10n.ajaxUrl, {
            method: 'POST',
            credentials: 'same-origin',
            body: data
        });
    }

    setNewProductPopupOptions() {
        this.popupOptions = {
            title: shopCTL10n.newProduct,
            contentLarge: true,
            actionButtons: [
                {
                    id: 'shop-ct-cancel-product',
                    text: shopCTL10n.cancel,
                },
                {
                    id: 'shop-ct-save-product',
                    class: 'shop-ct-mat-button--raised shop-ct-mat-button--primary',
                    text: shopCTL10n.publish,
                }
            ],
            onOpen: this.onOpen.bind(this),
            onClose: this.onClose.bind(this),
            afterClose: this.afterClose.bind(this),
            closing: this.closing.bind(this),
        };
    }

    setEditProductPopupOptions() {
        this.popupOptions = {
            title: shopCTL10n.editingProduct,
            contentLarge: true,
            actionButtons: [
                {
                    id: 'shop-ct-cancel-product',
                    text: shopCTL10n.cancel,
                },
                {
                    id: 'shop-ct-trash-product',
                    class: 'shop-ct-mat-button--raised',
                    text: shopCTL10n.trash,
                },
                {
                    id: 'shop-ct-save-product',
                    class: 'shop-ct-mat-button--raised shop-ct-mat-button--primary',
                    text: shopCTL10n.save,
                }
            ],
            onOpen: this.onOpen.bind(this),
            onClose: this.onClose.bind(this),
            afterClose: this.afterClose.bind(this),
            closing: this.closing.bind(this),
        };
    }


}