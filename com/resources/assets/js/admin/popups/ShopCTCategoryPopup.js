import {ShopCTPopup} from "../../ShopCTPopup";
import {ShopCTLoader} from "../../ShopCTLoader";
import {ShopCTToastr} from "../../ShopCTToastr";
import {ShopCTPageLoader} from "../ShopCTPageLoader";
import {shopCTCategoriesPage} from "../pages/shopCTCategoriesPage";

export class ShopCTCategoryPopup {
    constructor(id = null, isCategoriesPage = false) {
        this.id = id;
        this.popup = new ShopCTPopup('shop-ct-category-popup-wrapper');
        this.isCategoriesPage = isCategoriesPage;

        if (!this.id) {
            this.setNewCategoryPopupOptions();
        } else {
            this.setEditCategoryPopupOptions(this.id);
        }
    }

    open() {
        let url = shopCTL10n.ajaxUrl + '?action=shop_ct_edit_category';

        if (this.id) {
            url += '&id=' + this.id;
        }

        ShopCTLoader.show();

        fetch(url, {
            method: 'GET',
            credentials: 'same-origin'
        }).then((response) => response.json())
            .then(result => {
                ShopCTLoader.remove();
                if (result.status) {
                    this.popupOptions.content = result.data;
                    this.popup.show(this.popupOptions);
                } else {
                    console.log(response);
                    ShopCTToastr.show(shopCTL10n.serverSideError);
                }
            });
    }

    onOpen() {
        this.form = this.popup.popupElement.querySelector('#shop-ct-category-popup-form');
        this.cancelButton = this.popup.popupElement.querySelector('#shop-ct-cancel-category');
        this.saveButton = this.popup.popupElement.querySelector('#shop-ct-save-category');
        this.addImageButton = this.popup.popupElement.querySelector('#shop-ct-category-add-image');
        this.changeImageButton = this.popup.popupElement.querySelector('#shop-ct-category-change-image');
        this.imageElement = this.popup.popupElement.querySelector('#shop-ct-category-image');
        this.imageInput = this.popup.popupElement.querySelector('#shop-ct-category-image-value');
        this.removeImageButton = this.popup.popupElement.querySelector('#shop-ct-category-remove-image');

        this.attachEventListeners();
    }

    attachEventListeners() {
        this.cancelButton.addEventListener('click', this.cancel.bind(this));
        this.form.addEventListener('submit', (e) => {
            e.preventDefault()
        });

        this.addImageButton.addEventListener('click', this.changeImage.bind(this));
        this.changeImageButton.addEventListener('click', this.changeImage.bind(this));
        this.removeImageButton.addEventListener('click', this.removeImage.bind(this));

        this.saveButton.addEventListener('click', this.save.bind(this));
    }

    save(e) {
        e.preventDefault();

        let formData = new FormData(this.form);

        ShopCTLoader.show();

        this.saveButton.setAttribute("disabled", 'disabled');

        fetch(shopCTL10n.ajaxUrl, {
            method: 'POST',
            credentials: 'same-origin',
            body: formData
        }).then((response) => {
            this.saveButton.removeAttribute("disabled");
            return response.json();
        })
            .then(result => {

                if (result.status) {
                    ShopCTToastr.show(shopCTL10n.productSaved);
                    this.form.setAttribute('data-submitted', 'yes');
                    this.popup.close();
                    if (this.isCategoriesPage) {
                        let pageUrl = shopCTL10n.ajaxUrl + window.location.search + '&action=shop_ct_get_categories_page';
                        ShopCTPageLoader.load(pageUrl, shopCTCategoriesPage);
                    } else {
                        ShopCTLoader.remove();
                    }

                } else {
                    ShopCTLoader.remove();
                    if(result.errors){
                        ShopCTToastr.show(result.errors.join(','), 'error');
                    }

                }
            }, (reason) => {
                ShopCTLoader.remove();
            })
            .catch((reason) => {
                ShopCTLoader.remove();
            });
    }

    changeImage(e) {
        e.preventDefault();

        let uploader = wp.media({
            title: shopCTL10n.chooseImage,
            button: {
                text: shopCTL10n.insertImage
            },
            multiple: false,
            library: {
                type: 'image'
            },
        })
            .on('select', () => {
                let attachment = uploader.state().get('selection').first().toJSON();
                this.imageElement.src = attachment.url;
                this.imageInput.value = attachment.id;
                this.addImageButton.classList.add('shop-ct-hide');
                this.changeImageButton.classList.remove('shop-ct-hide');
                this.removeImageButton.classList.remove('shop-ct-hide');
            })
            .open();
    }

    removeImage(e) {
        e.preventDefault();

        this.imageElement.src = "";

        this.imageInput.value = null;

        this.changeImageButton.classList.add('shop-ct-hide');
        this.removeImageButton.classList.add('shop-ct-hide');
        this.addImageButton.classList.remove('shop-ct-hide');
    }

    cancel(e) {
        e.preventDefault();

        this.popup.close();
    }

    onClose() {

    }

    afterClose() {

    }

    setNewCategoryPopupOptions() {
        this.popupOptions = {
            title: shopCTL10n.newCategory,
            contentLarge: true,
            style: {
                width: '500px',
            },
            actionButtons: [
                {
                    id: 'shop-ct-cancel-category',
                    text: shopCTL10n.cancel,
                },
                {
                    id: 'shop-ct-save-category',
                    class: 'shop-ct-mat-button--raised shop-ct-mat-button--primary',
                    text: shopCTL10n.publish,
                }
            ],
            onOpen: this.onOpen.bind(this),
            onClose: this.onClose.bind(this),
            afterClose: this.afterClose.bind(this)
        };
    }

    setEditCategoryPopupOptions() {
        this.popupOptions = {
            title: shopCTL10n.editingCategory,
            contentLarge: true,
            style: {
                width: '500px',
            },
            actionButtons: [
                {
                    id: 'shop-ct-cancel-category',
                    //text: '<i class="material-icons">delete</i>'
                    text: shopCTL10n.cancel,
                },
                {
                    id: 'shop-ct-save-category',
                    class: 'shop-ct-mat-button--raised shop-ct-mat-button--primary',
                    text: shopCTL10n.save,
                }
            ],
            onOpen: this.onOpen.bind(this),
            onClose: this.onClose.bind(this),
            afterClose: this.afterClose.bind(this)
        };
    }
}