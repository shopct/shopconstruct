import {ShopCTPopup} from "../../ShopCTPopup";
import {ShopCTToastr} from "../../ShopCTToastr";
import {ShopCTLoader} from "../../ShopCTLoader";
import Pikaday from "../../Pikaday";
import {shopCTCategoriesPage} from "../pages/shopCTCategoriesPage";
import {ShopCTPageLoader} from "../ShopCTPageLoader";
import {shopCTOrdersPage} from "../pages/shopCTOrdersPage";

export class ShopCTOrderPopup {
    constructor(id = null, isOrdersPage = false) {
        this.id = id;
        this.popup = new ShopCTPopup('shop-ct-order-popup-wrapper');
        this.isOrdersPage = isOrdersPage;

        if (!this.id) {
            this.setNewOrderPopupOptions();
        } else {
            this.setEditOrderPopupOptions();
        }
    }

    open() {
        let url = shopCTL10n.ajaxUrl + '?action=shop_ct_edit_order';

        if (this.id) {
            url += '&id=' + this.id;
        }

        ShopCTLoader.show();

        fetch(url, {
            method: 'GET',
            credentials: 'same-origin'
        }).then((response) => response.json())
            .then(result => {
                ShopCTLoader.remove();
                if (result.status) {
                    this.popupOptions.content = result.data;
                    this.popup.show(this.popupOptions);
                } else {
                    console.log(response);
                    ShopCTToastr.show(shopCTL10n.serverSideError);
                }
            });
    }

    onOpen() {
        this.form = this.popup.popupElement.querySelector('#shop-ct-order-popup-form');
        this.saveButton = this.popup.popupElement.querySelector('#shop-ct-save-order');
        this.cancelButton = this.popup.popupElement.querySelector('#shop-ct-cancel-order');
        this.productsList = this.popup.popupElement.querySelector('#shop-ct-order-items-list');
        this.addProductButton = this.popup.popupElement.querySelector('#shop-ct-add-order-product');
        this.addProductSelectbox = this.popup.popupElement.querySelector('#shop-ct-order-product');
        this.addProductQuantity = this.popup.popupElement.querySelector('#shop-ct-order-product-quantity');
        this.productRow = this.productsList.getAttribute('data-row');
        this.noProductsRow = this.productsList.querySelector('.shop-ct-order-items-list-empty');

        this.datepicker = new Pikaday({
            field: this.popup.popupElement.querySelector('#shop-ct-order-date'),
            format: 'YYYY-MM-DD'
        });

        this.attachEventListeners();
    }

    attachEventListeners() {
        this.addProductButton.addEventListener('click', this.addProduct.bind(this));
        this.cancelButton.addEventListener('click', this.cancel.bind(this));
        this.saveButton.addEventListener('click', this.save.bind(this));
    }

    save(e) {
        e.preventDefault();

        let formData = new FormData(this.form);

        ShopCTLoader.show();

        this.saveButton.setAttribute("disabled", 'disabled');

        fetch(shopCTL10n.ajaxUrl, {
            method: 'POST',
            credentials: 'same-origin',
            body: formData
        }).then((response) => {
            this.saveButton.removeAttribute("disabled");
            return response.json();
        })
            .then(result => {

                if (result.status) {
                    ShopCTToastr.show(shopCTL10n.orderSaved);
                    this.form.setAttribute('data-submitted', 'yes');
                    this.popup.close();
                    if (this.isOrdersPage) {
                        let pageUrl = shopCTL10n.ajaxUrl + window.location.search + '&action=shop_ct_get_orders_page';
                        ShopCTPageLoader.load(pageUrl, shopCTOrdersPage);
                    } else {
                        ShopCTLoader.remove();
                    }

                } else {
                    ShopCTLoader.remove();
                    if(result.errors){
                        ShopCTToastr.show(result.errors.join(','), 'error');
                    }

                }
            }, (reason) => {
                ShopCTLoader.remove();
            })
            .catch((reason) => {
                ShopCTLoader.remove();
            });
    }

    cancel(e) {
        e.preventDefault();

        this.popup.close();
    }

    addProduct(e) {
        e.preventDefault();

        let productId = parseInt(this.addProductSelectbox.value);

        if (!productId) {
           alert(shopCTL10n.chooseProduct);
           return;
        }

        let qty = parseInt(this.addProductQuantity.value);

        let existingRow = this.productsList.querySelector('.shop-ct-mat-list-item[data-product-id="' + productId + '"]');
        if(existingRow) {
            let existingQty = parseInt(existingRow.querySelector('.shop-ct-order-product-qty-input').value);
            existingRow.querySelector('.shop-ct-order-product-qty-input').value = existingQty + qty;
            return;
        }

        let selected = this.addProductSelectbox.querySelector('option:checked');
        let productTitle = selected.innerHTML;
        let price = selected.getAttribute('data-price');
        let imageUrl = selected.getAttribute('data-image-url');

        let html = this.productRow;
        html = html.replace(/{productId}/g, productId);
        html = html.replace(/{productImageUrl}/g, imageUrl);
        html = html.replace(/{productTitle}/g, productTitle);
        html = html.replace(/{productPrice}/g, price);
        html = html.replace(/{quantity}/g, qty);

        this.productsList.insertAdjacentHTML('afterbegin', html);

        let emptyRow = this.productsList.querySelector('.shop-ct-order-items-list-empty');
        if(emptyRow) {
            emptyRow.remove();
        }
    }

    onClose() {

    }

    closing() {
        this.datepicker.destroy();
    }

    afterClose() {

    }

    setNewOrderPopupOptions() {
        this.popupOptions = {
            title: shopCTL10n.newOrder,
            contentLarge: true,
            actionButtons: [
                {
                    id: 'shop-ct-cancel-order',
                    text: shopCTL10n.cancel,
                },
                {
                    id: 'shop-ct-save-order',
                    class: 'shop-ct-mat-button--raised shop-ct-mat-button--primary',
                    text: shopCTL10n.publish,
                }
            ],
            onOpen: this.onOpen.bind(this),
            onClose: this.onClose.bind(this),
            afterClose: this.afterClose.bind(this),
            closing: this.closing.bind(this),
        };
    }

    setEditOrderPopupOptions() {
        this.popupOptions = {
            title: shopCTL10n.editingOrder,
            contentLarge: true,
            actionButtons: [
                {
                    id: 'shop-ct-cancel-order',
                    text: shopCTL10n.cancel,
                },
                {
                    id: 'shop-ct-save-order',
                    class: 'shop-ct-mat-button--raised shop-ct-mat-button--primary',
                    text: shopCTL10n.save,
                }
            ],
            onOpen: this.onOpen.bind(this),
            onClose: this.onClose.bind(this),
            afterClose: this.afterClose.bind(this),
            closing: this.closing.bind(this),
        };
    }
}