<?php

/*register_shutdown_function(function(){
   var_dump(error_get_last());
});*/

require_once __DIR__ . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'core.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'settings.php';

//Since WordPress is using observers (hooks) pattern, we should benefit from it our way
require_once __DIR__ . DIRECTORY_SEPARATOR . 'observers' . DIRECTORY_SEPARATOR . 'common.observers.php';

//todo: ajax requests must not be separated from other requests, change all admin-ajax routes to be REST API routes instead of admin
if (is_admin()) {
    //Attach admin specific observers
    require_once __DIR__ . DIRECTORY_SEPARATOR . 'observers' . DIRECTORY_SEPARATOR . 'admin.observers.php';

    // get admin routes
    $adminRoutes = include __DIR__ . DIRECTORY_SEPARATOR . 'config.admin.php';

    // Call admin router
    $adminMenu = new \ShopCT\Core\AdminMenu($adminRoutes);
} else {
    // Attach frontend specific observers
    require_once __DIR__ . DIRECTORY_SEPARATOR . 'observers' . DIRECTORY_SEPARATOR . 'frontend.observers.php';

    //Call frontend router
    $router = new \ShopCT\Core\Router();
}
