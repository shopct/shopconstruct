<?php

namespace ShopCT\Controllers;


use ShopCT\Core\View;
use ShopCT\Models\Product;
use ShopCT\Models\ProductAttribute;
use ShopCT\Models\ProductAttributeTerm;
use ShopCT\Views\AdminView;

class AttributesController extends ControllerAbstract
{
    public function index()
    {
        $attributes = ProductAttribute::getAll();

        $view = new AdminView(array(
            'content' => View::get('admin/attributes/index/index', compact('attributes'))
        ));

        $html = $view->get();

        if (defined('DOING_AJAX') && DOING_AJAX) {
            die(json_encode(array(
                'status' => 1,
                'data' => $html
            )));
        } else {
            return $html;
        }
    }

    public function edit()
    {
        $attribute = null;
        if (isset($_GET['id'])) {
            $attribute = new ProductAttribute(absint($_GET['id']));
        } else {
            $attribute = new ProductAttribute();
        }

        die(json_encode(array(
            'status' => 1,
            'data' => View::get('admin/attributes/edit/show', compact('attribute')),
        )));
    }

    public function editForProduct()
    {
        $product = new Product($_GET['product_id']);
        $attribute = $_GET['attribute_taxonomy'] === 'custom' ? null : new ProductAttribute($_GET['attribute_taxonomy']);

        echo json_encode(array(
            'status' => 1,
            'data' => View::get('admin/products/edit/attribute-row', compact('product', 'attribute'))
        ));
        die;
    }

    public function update()
    {
        $this->validate('shop_ct');

        $sentTerms = isset($_REQUEST['attribute-terms']) ? $_REQUEST['attribute-terms'] : array();

        /**
         * check if we are creating new attribute or updating existing one
         */
        if (isset($_REQUEST['attribute_id'])) {
            $id = $_REQUEST['attribute_id'];

            $attribute = new ProductAttribute($id);

            /**
             * for existing attribute you can only add new terms
             */


            if (!empty($sentTerms)) {
                foreach ($sentTerms as $term) {
                    $termObj = ProductAttributeTerm::getByName($term, $attribute->slug);

                    /**
                     * if term already exist, we have nothing to do here
                     */
                    if ($termObj) {
                        continue;
                    }

                    $termObj = new ProductAttributeTerm();

                    $termObj->name = sanitize_text_field($term);

                    $attribute->addTerm($termObj);
                }
            }

            $attribute->save();
        } else {
            $attribute = new ProductAttribute();

            $attribute->name = sanitize_text_field($_REQUEST['name']);

            if(empty($_REQUEST['slug'])) {
                $attribute->slug = sanitize_title($attribute->name);
            } else {
                $attribute->slug = sanitize_text_field($_REQUEST['slug']);
            }

            $attribute->save();

            if (!empty($sentTerms)) {
                foreach ($sentTerms as $term) {
                    $termObj = new ProductAttributeTerm();

                    $termObj->name = sanitize_text_field($term);
                    $termObj->setTaxonomy($attribute);

                    $attribute->addTerm($termObj);

                    $termObj->save();
                }
            }
        }

        die(json_encode(array(
            'status' => 1,
        )));
    }
}