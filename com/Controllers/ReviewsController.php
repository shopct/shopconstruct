<?php

namespace ShopCT\Controllers;


use ShopCT\Core\View;
use ShopCT\Models\Review;
use ShopCT\Views\AdminView;

class ReviewsController extends ControllerAbstract
{
    /**
     * @return mixed|string
     */
    public function index()
    {
        $reviews = Review::all();

        $view = new AdminView(array(
            'content' => View::get('admin/reviews/index/index', compact('reviews'))
        ));

        $html = $view->get();

        if (defined('DOING_AJAX') && DOING_AJAX) {
            die(json_encode(array(
                'status' => 1,
                'data' => $html
            )));
        } else {
            return $html;
        }
    }

    public function update()
    {
        $this->validate('shop_ct');

        if (!isset($_REQUEST['id'])) {
            die(json_encode(array(
                'status' => 0,
                'errors' => array('parameter "id" is required')
            )));
        }

        $review = new Review(absint($_REQUEST['id']));

        $review->setApproved($_REQUEST['status']);

        die(json_encode(array(
            'status' => (int)$review->save(),
        )));
    }

    public function destroy()
    {
        $this->validate('shop_ct');

        if (!isset($_REQUEST['id'])) {
            die(json_encode(array(
                'status' => 0,
                'errors' => array('parameter "id" is required')
            )));
        }

        die(json_encode(array(
            'status' => (int)Review::delete(absint($_REQUEST['id'])),
        )));
    }
}