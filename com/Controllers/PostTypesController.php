<?php

namespace ShopCT\Controllers;


use ShopCT\Lib\PostTypes;
use ShopCT\Models\Product;

class PostTypesController
{
    public function register()
    {
        $this->registerTaxonomies();
        $registered = false;

        if (!post_type_exists(Product::$post_type)) {

            $params = array(
                'labels' => array(
                    'name' => __('Products', 'shop_ct'),
                    'singular_name' => __('Product', 'shop_ct'),
                    'menu_name' => _x('Products', 'Admin menu name', 'shop_ct'),
                    'add_new' => __('Add Product', 'shop_ct'),
                    'add_new_item' => __('Add New Product', 'shop_ct'),
                    'edit' => __('Edit', 'shop_ct'),
                    'edit_item' => __('Edit Product', 'shop_ct'),
                    'new_item' => __('New Product', 'shop_ct'),
                    'view' => __('View Product', 'shop_ct'),
                    'view_item' => __('View Product', 'shop_ct'),
                    'search_items' => __('Search Products', 'shop_ct'),
                    'not_found' => __('No Products found', 'shop_ct'),
                    'not_found_in_trash' => __('No Products found in trash', 'shop_ct'),
                    'parent' => __('Parent Product', 'shop_ct'),
                    'featured_image' => __('Product Image', 'shop_ct'),
                    'set_featured_image' => __('Set product image', 'shop_ct'),
                    'remove_featured_image' => __('Remove product image', 'shop_ct'),
                    'use_featured_image' => __('Use as product image', 'shop_ct'),
                ),
                'description' => __('This is where you can add new products to your store.', 'shop_ct'),
                'public' => true,
                'show_ui' => false,
                'capability_type' => Product::$post_type,
                'map_meta_cap' => true,
                'publicly_queryable' => true,
                'exclude_from_search' => false,
                'hierarchical' => false,
                'rewrite' => array(
                    'slug' => 'product',
                    'with_front' => false,
                    'feeds' => true,
                ),
                'query_var' => true,
                'supports' => array(
                    'title',
                    'editor',
                    'excerpt',
                    'thumbnail',
                    'comments',
                    'custom-fields',
                    'page-attributes',
                    'publicize',
                    'wpcom-markdown',
                ),
                'has_archive' => 'shop',
                'show_in_nav_menus' => false,
            );

            register_post_type(Product::$post_type, apply_filters('shop_ct_register_post_type_product', $params));
            $registered = true;
        }

        if (!post_type_exists('shop_ct_order')) {

            $params = array(
                'labels' => array(
                    'name' => __('Orders', 'shop_ct'),
                    'singular_name' => __('Order', 'shop_ct'),
                    'menu_name' => _x('Orders', 'Admin menu name', 'shop_ct'),
                    'add_new' => __('Add Order', 'shop_ct'),
                    'add_new_item' => __('Add New Order', 'shop_ct'),
                    'edit' => __('Edit', 'shop_ct'),
                    'edit_item' => __('Edit Order', 'shop_ct'),
                    'new_item' => __('New Order', 'shop_ct'),
                    'view' => __('View Order', 'shop_ct'),
                    'view_item' => __('View Order', 'shop_ct'),
                    'search_items' => __('Search Orders', 'shop_ct'),
                    'not_found' => __('No Orders found', 'shop_ct'),
                    'not_found_in_trash' => __('No Orders found in trash', 'shop_ct'),
                    'parent' => __('Parent Order', 'shop_ct'),
                    'featured_image' => __('Order Image', 'shop_ct'),
                    'set_featured_image' => __('Set order image', 'shop_ct'),
                    'remove_featured_image' => __('Remove order image', 'shop_ct'),
                    'use_featured_image' => __('Use as order image', 'shop_ct'),
                ),
                'description' => __('This is where you can add new order.', 'shop_ct'),
                'public' => true,
                'show_ui' => false,
                'capability_type' => 'shop_ct_order',
                'map_meta_cap' => true,
                'publicly_queryable' => true,
                'exclude_from_search' => false,
                'hierarchical' => false,
                'query_var' => true,
                'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'comments', 'custom-fields', 'page-attributes', 'publicize', 'wpcom-markdown'),
                'show_in_nav_menus' => true
            );

            register_post_type('shop_ct_order', apply_filters('shop_ct_register_post_type_order', $params));
            $registered = true;
        }

        if ($registered) {
            flush_rewrite_rules();
        }

        $this->registerStatuses();
    }

    public function registerTaxonomies()
    {

        do_action('shop_ct_after_register_taxonomy');
    }


    public function registerStatuses()
    {
        $orderStatuses = PostTypes::getOrderStatuses();

        foreach ($orderStatuses as $key => $arg) {
            register_post_status($key, $arg);
        }

        $shippingZoneStatuses = PostTypes::getShippingZoneStatuses();
        foreach ($shippingZoneStatuses as $status => $arg) {
            register_post_status($status, $arg);
        }
    }


    public function supportJetpackOmnisearch()
    {
        if (class_exists('Jetpack_Omnisearch_Posts')) {
            new \Jetpack_Omnisearch_Posts(Product::$post_type);
        }
    }
}