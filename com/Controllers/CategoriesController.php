<?php

namespace ShopCT\Controllers;


use ShopCT\Core\View;
use ShopCT\Models\Category;
use ShopCT\Views\AdminView;

class CategoriesController extends ControllerAbstract
{
    /**
     * @return mixed|string
     */
    public function index()
    {
        $args = array(
            'hide_empty' => false,
        );

        if (isset($_GET['search'])) {
            $args['search'] = sanitize_text_field($_GET['search']);
        }


        $categories = Category::get($args);

        $view = new AdminView(array(
            'content' => View::get('admin/categories/index/index', compact('categories'))
        ));

        $html = $view->get();


        if (defined('DOING_AJAX') && DOING_AJAX) {
            die(json_encode(array(
                'status' => 1,
                'data' => $html
            )));
        } else {
            return $html;
        }
    }

    public function getChecklist()
    {
        $term_ids = array();
        $post_id = 0;
        if (isset($_REQUEST['product_id']) && !empty($_REQUEST['product_id']) && is_numeric($_REQUEST['product_id'])) {
            $post_id = absint($_REQUEST['product_id']);
            $terms = get_the_terms($post_id, Category::$taxonomy);

            if (is_array($terms)) {
                foreach ($terms as $term) {
                    $term_ids[] = $term->term_id;
                }
            }
        }
        $result = wp_terms_checklist($post_id, array(
            'taxonomy' => Category::$taxonomy,
            'descendants_and_self' => false,
            'popular_cats' => true,
            'walker' => false,
            'checked_ontop' => 1,
            'selected_cats' => $term_ids,
            'echo' => false,
        ));


        echo json_encode(array(
            'status' => 1,
            'data' => $result
        ));
        die;
    }

    public function edit()
    {
        $category = null;
        if (isset($_GET['id'])) {
            $category = new Category(absint($_GET['id']));
        } else {
            $category = new Category();
        }

        echo json_encode(array(
            'status' => 1,
            'data' => View::get('admin/categories/edit/show', compact('category')),
        ));
        die;
    }

    /**
     * Also creates new category(maybe separate later)
     */
    public function update()
    {
        $this->validate('shop_ct');

        $id = null;

        if (isset($_REQUEST['category_id'])) {
            $id = $_REQUEST['category_id'];

            if (absint($id) != $id) {
                die(json_encode(array(
                    'status' => 0,
                    'errors' => array('Invalid value passed for "ID" field')
                )));
            }

            $category = new Category($id);

            if (null === $category->id) {
                die(json_encode(array(
                    'status' => 0,
                    'errors' => array('Specified category does not exist')
                )));
            }
        } else {
            $category = new Category();
        }

        if (isset($_REQUEST['name']) && !empty($_REQUEST['name'])) {
            $category->name = sanitize_text_field($_REQUEST['name']);
        }

        if (isset($_REQUEST['slug']) && !empty($_REQUEST['slug'])) {
            $category->slug = sanitize_text_field($_REQUEST['slug']);
        }

        if (isset($_REQUEST['description']) && !empty($_REQUEST['description'])) {
            $category->description = sanitize_text_field($_REQUEST['description']);
        }

        if (isset($_REQUEST['shop-ct-category-image']) && !empty($_REQUEST['shop-ct-category-image'])) {
            $category->thumbnail_id = absint($_REQUEST['shop-ct-category-image']);
        }

        if (isset($_REQUEST['parent']) && !empty($_REQUEST['parent'])) {
            $category->parent = absint($_REQUEST['parent']);
        }

        if (!is_wp_error($category->save())) {
            die(json_encode(array(
                'status' => 1,
            )));
        } else {
            die(json_encode(array(
                'status' => 0,
            )));
        }
    }

    public function destroy()
    {
        $this->validate('shop_ct');

        if (!isset($_REQUEST['id'])) {
            echo json_encode(array(
                'status' => 0,
                'errors' => array(
                    'parameter "id" is required',
                )
            ));
        }

        $id = absint($_REQUEST['id']);

        wp_delete_term($id, Category::$taxonomy);

        echo json_encode(array(
            'status' => 1,
        ));
        die;
    }
}