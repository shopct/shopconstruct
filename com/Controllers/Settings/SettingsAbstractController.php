<?php

namespace ShopCT\Controllers\Settings;


use ShopCT\Controllers\ControllerAbstract;
use ShopCT\Models\Settings\SettingsAbstract;

abstract class SettingsAbstractController extends ControllerAbstract
{
    /**
     * @return SettingsAbstract
     */
    abstract function getSettingsInstance();

    public function save()
    {
        $this->validate('shop_ct');

        if (!isset($_POST['settings']) || empty($_POST['settings'])) {
            die('Nothing to save');
        }

        $settings = $this->getSettingsInstance();

        foreach ($_POST['settings'] as $key => $value) {
            $settings->$key = $value;
        }

        $settings->save();

        die(json_encode(array(
            'status' => 1,
        )));
    }

}