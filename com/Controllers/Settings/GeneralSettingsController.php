<?php

namespace ShopCT\Controllers\Settings;
use ShopCT\Core\View;
use ShopCT\Views\AdminView;

/**
 * Class GeneralSettingsController
 * @package ShopCT\Controllers\Settings
 */
class GeneralSettingsController
{
    public function index()
    {
        $view = new AdminView(array(
            'content' => View::get('admin/settings/general/index', compact('products', 'availablePostStatuses', 'postsCount', 'totalCount', 'paged', 'defaultStatus'))
        ));

        $html = $view->get();

        return $html;
    }
}