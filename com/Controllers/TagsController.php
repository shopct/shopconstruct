<?php

namespace ShopCT\Controllers;


use ShopCT\Core\View;
use ShopCT\Models\Tag;
use ShopCT\Views\AdminView;

class TagsController extends ControllerAbstract
{
    /**
     * @return mixed|string
     */
    public function index()
    {
        $args = array(
            'hide_empty' => false,
        );

        if (isset($_GET['search'])) {
            $args['search'] = sanitize_text_field($_GET['search']);
        }

        $tags = Tag::get($args);

        $view = new AdminView(array(
            'content' => View::get('admin/tags/index/index', compact('tags'))
        ));

        $html = $view->get();

        if (defined('DOING_AJAX') && DOING_AJAX) {
            die(json_encode(array(
                'status' => 1,
                'data' => $html
            )));
        } else {
            return $html;
        }
    }

    public function edit()
    {
        $tag = null;
        if (isset($_GET['id'])) {
            $tag = new Tag(absint($_GET['id']));
        } else {
            $tag = new Tag();
        }

        echo json_encode(array(
            'status' => 1,
            'data' => View::get('admin/tags/edit/show', compact('tag')),
        ));
        die;
    }

    public function update()
    {
        $this->validate('shop_ct');

        $id = null;

        if (isset($_REQUEST['tag_id'])) {
            $id = $_REQUEST['tag_id'];

            if (absint($id) != $id) {
                echo json_encode(array(
                    'status' => 0,
                    'errors' => array('Invalid value passed for "ID" field')
                ));
                die;
            }

            $tag = new Tag($id);

            if (null === $tag->id) {
                echo json_encode(array(
                    'status' => 0,
                    'errors' => array('Specified category does not exist')
                ));
                die;
            }
        } else {
            $tag = new Tag();
        }

        if (isset($_REQUEST['name']) && !empty($_REQUEST['name'])) {
            $tag->name = sanitize_text_field($_REQUEST['name']);
        }

        if (isset($_REQUEST['slug']) && !empty($_REQUEST['slug'])) {
            $tag->slug = sanitize_text_field($_REQUEST['slug']);
        }

        if (isset($_REQUEST['description']) && !empty($_REQUEST['description'])) {
            $tag->description = sanitize_text_field($_REQUEST['description']);
        }

        if (!is_wp_error($tag->save())) {
            echo json_encode(array(
                'status' => 1,
            ));
        } else {
            echo json_encode(array(
                'status' => 0,
            ));
        }
        die;
    }
}