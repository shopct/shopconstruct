<?php

namespace ShopCT\Controllers;


use ShopCT\Models\Product;
use ShopCT\Models\ProductAttribute;
use ShopCT\Models\Tag;
use ShopCT\Models\Category;

class TaxonomiesController
{
    public function register()
    {
        if (!taxonomy_exists(Category::$taxonomy)) {
            register_taxonomy(Category::$taxonomy,
                apply_filters('shop_ct_taxonomy_objects_product_cat', array(Product::$post_type)),
                apply_filters('shop_ct_taxonomy_args_product_cat', array(
                    'hierarchical' => true,
                    //'update_count_callback' => 'shop_ct_term_recount',
                    'label' => __('Product Categories', 'shop_ct'),
                    'labels' => array(
                        'name' => __('Product Categories', 'shop_ct'),
                        'singular_name' => __('Product Category', 'shop_ct'),
                        'menu_name' => _x('Categories', 'Admin menu name', 'shop_ct'),
                        'search_items' => __('Search Product Categories', 'shop_ct'),
                        'all_items' => __('All Product Categories', 'shop_ct'),
                        'parent_item' => __('Parent Product Category', 'shop_ct'),
                        'parent_item_colon' => __('Parent Product Category:', 'shop_ct'),
                        'edit_item' => __('Edit Product Category', 'shop_ct'),
                        'update_item' => __('Update Product Category', 'shop_ct'),
                        'add_new_item' => __('Add New Product Category', 'shop_ct'),
                        'new_item_name' => __('New Product Category Name', 'shop_ct')
                    ),
                    'show_ui' => true,
                    'query_var' => true,
                    'rewrite' => array(
                        'slug' => _x('product-category', 'slug', 'shop_ct'),
                        'with_front' => true,
                        'hierarchical' => true,
                    ),
                ))
            );
        }

        if (!taxonomy_exists(Tag::$taxonomy)) {
            register_taxonomy('shop_ct_product_tag',
                apply_filters('shop_ct_taxonomy_objects_product_tag', array(Product::$post_type)),
                apply_filters('shop_ct_taxonomy_args_product_tag', array(
                    'hierarchical' => false,
                    //'update_count_callback' => 'shop_ct_term_recount',
                    'label' => __('Product Tags', 'shop_ct'),
                    'labels' => array(
                        'name' => __('Product Tags', 'shop_ct'),
                        'singular_name' => __('Product Tag', 'shop_ct'),
                        'menu_name' => _x('Tags', 'Admin menu name', 'shop_ct'),
                        'search_items' => __('Search Product Tags', 'shop_ct'),
                        'all_items' => __('All Product Tags', 'shop_ct'),
                        'edit_item' => __('Edit Product Tag', 'shop_ct'),
                        'update_item' => __('Update Product Tag', 'shop_ct'),
                        'add_new_item' => __('Add New Product Tag', 'shop_ct'),
                        'new_item_name' => __('New Product Tag Name', 'shop_ct'),
                        'popular_items' => __('Popular Product Tags', 'shop_ct'),
                        'separate_items_with_commas' => __('Separate Product Tags with commas', 'shop_ct'),
                        'add_or_remove_items' => __('Add or remove Product Tags', 'shop_ct'),
                        'choose_from_most_used' => __('Choose from the most used Product tags', 'shop_ct'),
                        'not_found' => __('No Product Tags found', 'shop_ct'),
                    ),
                    'show_ui' => true,
                    'query_var' => true,
                    'rewrite' => array(
                        'slug' => _x('product-tag', 'slug', 'shop_ct'),
                        'with_front' => false
                    ),
                ))
            );
        }

        $this->registerAttributes();
    }

    public function registerAttributes()
    {
        $attributes = ProductAttribute::getAttributesWithoutTerms();
        $result = array();

        foreach ($attributes as $attribute) {
            if (taxonomy_exists($attribute->slug)) {
                continue;
            }

            $args = array(
                'public' => $attribute->public,
                'hierarchical' => false,
                // todo: add update_count_callback function
                'labels' => array(
                    'name' => __('Product Attributes', 'shop_ct'),
                    'singular_name' => __('Product Attribute', 'shop_ct'),
                    'menu_name' => _x('Attributes', 'Admin menu name', 'shop_ct'),
                    'search_items' => __('Search Product Attributes', 'shop_ct'),
                    'all_items' => __('All Product Attributes', 'shop_ct'),
                    'parent_item' => __('Parent Product Attribute', 'shop_ct'),
                    'parent_item_colon' => __('Parent Product Attribute:', 'shop_ct'),
                    'edit_item' => __('Edit Product Attribute', 'shop_ct'),
                    'update_item' => __('Update Product Attribute', 'shop_ct'),
                    'add_new_item' => __('Add New Product Attribute', 'shop_ct'),
                    'new_item_name' => __('New Product Attribute Name', 'shop_ct')
                ),
                'rewrite' => array(
                    'slug' => $attribute->slug,
                    'with_front' => false,
                    'hierarchical' => true,
                )
            );

            $result[] = register_taxonomy(
                $attribute->slug,
                apply_filters('shop_ct_attribute_associated_object_types', array(Product::$post_type)),
                apply_filters('shop_ct_attribute_registration_args', $args)
            );
        }

        return $result;
    }
}