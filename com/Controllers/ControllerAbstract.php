<?php

namespace ShopCT\Controllers;


abstract class ControllerAbstract
{

    protected function validate($action)
    {
        if (!isset($_REQUEST['nonce']) || !wp_verify_nonce($_REQUEST['nonce'], $action)) {
            die('Could not validate security token, please try again');
        }
    }
}