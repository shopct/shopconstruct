<?php

namespace ShopCT\Controllers;


class AdminController
{
    public function addScripts($hook)
    {
        wp_enqueue_script('shop-ct-admin', SHOP_CT_PUBLIC_URL . 'js/admin.js', array('jquery'), SHOP_CT_VERSION, true);

        if (strpos($hook, 'shop_ct_') !== false) {
            wp_enqueue_editor();
            wp_enqueue_media();
        }

        $this->localize();

    }

    public function addStyles($hook)
    {
        wp_enqueue_style('shop-ct-admin', SHOP_CT_PUBLIC_URL . 'css/admin.css');
    }

    public function localize()
    {
        wp_localize_script('shop-ct-admin', 'shopCTL10n', array(
            'nonce' => wp_create_nonce('shop_ct'),
            'ajaxUrl' => admin_url('admin-ajax.php'),
            'publish' => __('Publish', 'shop_ct'),
            'trash' => __('Move to Trash', 'shop_ct'),
            'save' => __('Save', 'shop_ct'),
            'newProduct' => __('New Product', 'shop_ct'),
            'newOrder' => __('New Order', 'shop_ct'),
            'cancel' => __('Cancel', 'shop_ct'),
            'invalidSalePrice' => __('Invalid Sale Price', 'shop_ct'),
            'serverSideError' => __('Something went wrong while processing your request, please try again later', 'shop_ct'),
            'popupAlert' => __('All inserted data will be lost if you close the popup without saving.', 'shop_ct'),
            'noFiles' => __('No Files', 'shop_ct'),
            'addDownloadableFile' => __('Add Downloadable File', 'shop_ct'),
            'productPublished' => __('Product Saved Successfully', 'shop_ct'),
            'ok' => __('OK', 'shop_ct'),
            'editingProduct' => __('Editing Product', 'shop_ct'),
            'shortcodeCopied' => __('Shortcode copied successfully', 'shop_ct'),
            'newCategory' => __('New Category', 'shop_ct'),
            'newTag' => __('New Tag', 'shop_ct'),
            'editingCategory' => __('Editing Category', 'shop_ct'),
            'editingTag' => __('Editing Tag', 'shop_ct'),
            'chooseImage' => __('Choose Image', 'shop_ct'),
            'insertImage' => __('Insert Image', 'shop_ct'),
            'confirmRemoval' => __('Confirm removal of selected item', 'shop_ct'),
            'categoryRemoved' => __('Category has been removed', 'shop_ct'),
            'newAttribute' => __('New Attribute', 'shop_ct'),
            'editingAttribute' => __('Editing Attribute', 'shop_ct'),
            'productTrashed' => __('Product moved to trash successfully', 'shop_ct'),
            'attributeSaved' => __('Attribute saved successfully', 'shop_ct'),
            'attributeRemoved' => __('Attribute has been removed', 'shop_ct'),
            'cannotRemoveAttribute' => __('Cannot remove attribute which has terms', 'shop_ct'),
            'chooseProduct' => __('Please, Choose Product','shop_ct'),
            'orderSaved' => __('Order saved successfully','shop_ct'),
//            'insertFile' => __('Insert file','shop_ct'),
//            'admin_url'=>$admin_url,
//            'plugin_url' => SHOP_CT()->plugin_url(),
//            'hook' => $hook,
//            'scripts_base_url'=>$wp_scripts->base_url,
//            'scripts_editor_src'=>$wp_scripts->registered['editor']->src,
//            'display_settings_click_right'=>__('Click the arrow on the right of the item to reveal additional configuration options.','shop_ct'),
//
//
//            'ask_delete_product' => __('Are you sure you want to delete this product?','shop_ct'),
//            'ask_delete_products' => __('Are you sure you want to delete ALL selected products?','shop_ct'),
//
//            'ok' => __('OK','shop_ct'),
//            'cancel' => __('Cancel','shop_ct'),
//            'publishOn' => __('Publish on:','shop_ct'),
//            'publishOnFuture' =>  __('Schedule for:','shop_ct'),
//            'publishOnPast' => __('Published on:','shop_ct'),
//            'dateFormat' => __('%1$s %2$s, %3$s @ %4$s:%5$s','shop_ct'),
//            'showcomm' => __('Show more comments','shop_ct'),
//            'endcomm' => __('No more comments found.','shop_ct'),
//
//            'schedule' => __('Schedule','shop_ct'),
//            'update' => __('Update','shop_ct'),
//            'savePending' => __('Save as Pending','shop_ct'),
//            'saveDraft' => __('Save Draft','shop_ct'),
//            'private' => __('Private','shop_ct'),
//            'public' => __('Public','shop_ct'),
//            'publicSticky' => __('Public, Sticky','shop_ct'),
//            'password' => __('Password Protected','shop_ct'),
//            'privatelyPublished' => __('Privately Published','shop_ct'),
//            'published' => __('Published','shop_ct'),
//            'saveAlert' => __('The changes you made will be lost if you navigate away from this page.','shop_ct' ),
//            'savingText' => __('Saving Draft&#8230;','shop_ct' ),
//            'permalinkSaved' => __( 'Permalink saved','shop_ct' ),
//            'closePopup' => __('Press \'OK\' to close anyways','shop_ct'),
//            'name' => __('Name','shop_ct'),
//            'comma_separated_values' => __('Values separated with commas','shop_ct'),
//
//
//
//            'order_published' => __('Order Saved Successfully','shop_ct'),
        ));
    }
}