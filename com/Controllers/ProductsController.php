<?php

namespace ShopCT\Controllers;


use ShopCT\Core\View;
use ShopCT\Lib\Dates;
use ShopCT\Models\Category;
use ShopCT\Models\Product;
use ShopCT\Models\ProductAttribute;
use ShopCT\Models\Tag;
use ShopCT\Views\AdminView;

class ProductsController extends ControllerAbstract
{

    public function index()
    {
        $args = array('posts_per_page' => Product::SHOW_PER_PAGE);

        $paged = 1;

        if (isset($_GET['paged'])) {

            $args['posts_per_page'] = Product::SHOW_PER_PAGE * absint($_GET['paged']);

            $paged = $_GET['paged'];
        }

        if (isset($_GET['search'])) {
            $args['s'] = sanitize_text_field($_GET['search']);
        }

        if (isset($_GET['post_status'])) {
            $args['post_status'] = sanitize_text_field($_GET['post_status']);
        } else {
            $args['post_status'] = 'publish';
        }

        $products = Product::get($args);


        $availablePostStatuses = array(
            'publish' => __('Published', 'shop_ct'),
            'draft' => __('Draft', 'shop_ct'),
            'pending' => __('Pending Review', 'shop_ct'),
            'future' => __('Scheduled', 'shop_ct'),
            'private' => __('Private', 'shop_ct'),
            'trash' => __('Trash', 'shop_ct'),
        );

        $defaultStatus = 'publish';

        /* count total pages */
        $countArgs = $args;
        $countArgs['paged'] = $paged;
        $countArgs['posts_per_page'] = Product::SHOW_PER_PAGE;
        $totalCount = Product::countPages($countArgs);

        $postsCount = wp_count_posts(Product::$post_type);

        $view = new AdminView(array(
            'content' => View::get('admin/products/index/index', compact('products', 'availablePostStatuses', 'postsCount', 'totalCount', 'paged', 'defaultStatus'))
        ));

        $html = $view->get();


        if (defined('DOING_AJAX') && DOING_AJAX) {
            die(json_encode(array(
                'status' => 1,
                'data' => $html
            )));
        } else {
            return $html;
        }

    }


    public function edit()
    {
        $id = null;
        if (isset($_GET['id'])) {
            $autoDraft = false;
            $product = new Product(absint($_GET['id']));
        } else {
            $autoDraft = true;
            /** if adding new product create an auto-draft to work with */
            $post = get_default_post_to_edit(Product::$post_type, true);

            $product = new Product($post->ID);
            $product->post->post_title = '';
        }

        echo json_encode(array(
            'status' => 1,
            'data' => View::get('admin/products/edit/show', compact('product', 'autoDraft')),
        ));
        die;
    }

    public function update()
    {
        $this->validate('shop_ct');

        if (isset($_REQUEST['product_id'])) $id = $_REQUEST['product_id'];
        elseif (isset($_REQUEST['post_data']['ID'])) $id = $_REQUEST['post_data']['ID'];
        else return;

        if (absint($id) != $id) {
            echo json_encode(array(
                'status' => 0,
                'errors' => array('Invalid value passed for "ID" field')
            ));
            die;
        }

        $id = absint($id);

        $product = new Product($id);

        if (empty($_REQUEST['post_data']['post_title'])) {
            die(json_encode(array(
                'status' => 0,
                'errors' => array('Title is required')
            )));
        }

        if (isset($_REQUEST['post_data'])) {
            foreach ($_REQUEST['post_data'] as $key => $value) {
                $product->post->$key = $value;
            }
        }

        if (isset($_REQUEST['post_data']['post_date']))
            $product->post->post_date = date("Y-m-d H:i:s", strtotime($_REQUEST['post_data']['post_date']));
        if (isset($_REQUEST['post_data']['post_modified']))
            $product->post->post_modified = date("Y-m-d H:i:s", strtotime($_REQUEST['post_data']['post_modified']));
        if (isset($_REQUEST['post_data']['post_date_gmt']))
            $product->post->post_date_gmt = date("Y-m-d H:i:s", strtotime($_REQUEST['post_data']['post_date_gmt']));
        if (isset($_REQUEST['post_data']['post_modified_gmt']))
            $product->post->post_modified_gmt = date("Y-m-d H:i:s", strtotime($_REQUEST['post_data']['post_modified_gmt']));


        if (isset($_REQUEST['product_sale_price_dates_from']) && !empty($_REQUEST['product_sale_price_dates_from'])) {
            $sale_from = $_REQUEST['product_sale_price_dates_from']
                . ' ' . (!empty($_REQUEST['product_sale_price_dates_from_hours']) && strlen($_REQUEST['product_sale_price_dates_from_hours']) == 2 ? $_REQUEST['product_sale_price_dates_from_hours'] : '00')
                . ':' . (!empty($_REQUEST['product_sale_price_dates_from_minutes']) && strlen($_REQUEST['product_sale_price_dates_from_minutes']) == 2 ? $_REQUEST['product_sale_price_dates_from_minutes'] : '00');
        } else {
            $sale_from = '';
        }


        if (isset($_REQUEST['product_sale_price_dates_to']) && !empty($_REQUEST['product_sale_price_dates_to'])) {
            $sale_to = $_REQUEST['product_sale_price_dates_to']
                . ' ' . (!empty($_REQUEST['product_sale_price_dates_to_hours']) && strlen($_REQUEST['product_sale_price_dates_to_hours']) <= 2 ? $_REQUEST['product_sale_price_dates_to_hours'] : '00')
                . ':' . (!empty($_REQUEST['product_sale_price_dates_to_minutes']) && strlen($_REQUEST['product_sale_price_dates_to_minutes']) <= 2 ? $_REQUEST['product_sale_price_dates_to_minutes'] : '00');
        } else {
            $sale_to = '';
        }

        if ((empty($sale_from) || Dates::validate($sale_from)) && (empty($sale_to) || strtotime($sale_from) < strtotime($sale_to))) {
            $_REQUEST['post_meta']['sale_price_dates_from'] = $sale_from;
        }

        if ((empty($sale_from) || Dates::validate($sale_to)) && (empty($sale_from) || strtotime($sale_from) < strtotime($sale_to))) {
            $_REQUEST['post_meta']['sale_price_dates_to'] = $sale_to;
        }

        if (isset($_REQUEST['downloadable-file-urls']) && !empty($_REQUEST['downloadable-file-urls'])):
            foreach ($_REQUEST['downloadable-file-urls'] as $key => $url):
                $_REQUEST['post_meta']['downloadable_files'][] = array(
                    'url' => $url,
                    'name' => isset($_REQUEST['downloadable-file-names'][$key]) ? $_REQUEST['downloadable-file-names'][$key] : '',
                );
            endforeach;
        endif;

        if (isset($_REQUEST['post_meta']['regular_price'])
            && !empty($_REQUEST['post_meta']['regular_price'])
            && isset($_REQUEST['post_meta']['sale_price'])
            && !empty($_REQUEST['post_meta']['sale_price'])
            && $_REQUEST['post_meta']['sale_price'] >= $_REQUEST['post_meta']['regular_price']) {

            $_REQUEST['post_meta']['sale_price'] = '';
            $_REQUEST['post_meta']['sale_price_dates_to'] = '';
            $_REQUEST['post_meta']['sale_price_dates_from'] = '';
        }


        if (isset($_REQUEST['post_meta'])) {
            foreach ($_REQUEST['post_meta'] as $key => $value) {
                $product->$key = $value;
            }
        }


        if (isset($_REQUEST['tax_input']['shop_ct_product_category'])) {
            $cats = array_map('intval', $_REQUEST['tax_input']['shop_ct_product_category']);
            wp_set_object_terms($id, $cats, Category::$taxonomy, false);
        }

        if (isset($_REQUEST['product_tags'])) {
            wp_set_object_terms($id, $_REQUEST['product_tags'], Tag::$taxonomy, false);
        }

        if (isset($_REQUEST['product-attribute-taxonomies']) && !empty($_REQUEST['product-attribute-taxonomies'])):
            $attributes = $_REQUEST['product-attribute-taxonomies'];
            $attributes_terms = $_REQUEST['product-attribute-terms'];

            foreach ($attributes as $key => $attribute):
                $current_terms = isset($attributes_terms[$key]) ? $attributes_terms[$key] : null;

                // either the id of existing attribute is passed or the name of attribute which needs to be saved
                if (is_numeric($attribute)):

                    $attr = new ProductAttribute($attribute);

                else:

                    $attr = new ProductAttribute();
                    $attr->name = $attribute;
                    $attr->slug = sanitize_title($attribute);
                    $attr->save();
                    // register is required for taxonomy_exists() checks to be true
                    $taxonomyController = new TaxonomiesController();
                    $taxonomyController->registerAttributes();

                endif;


                if (!empty($current_terms)):

                    $current_terms = array_map('trim', explode(',', $current_terms));
                    wp_set_object_terms($id, $current_terms, $attr->slug, false);

                endif;
            endforeach;

        endif;

        try {
            $product->save();
            echo json_encode(array(
                'status' => 1,
            ));
            die;
        } catch (\Exception $e) {
            echo json_encode(array(
                'status' => 0,
                'errors' => array($e->getMessage())
            ));
            die;
        }
    }

    public function destroy()
    {
        $this->validate('shop_ct');

        if (!isset($_REQUEST['id'])) {
            echo json_encode(array(
                'status' => 0,
                'errors' => array(
                    'id parameter is required',
                )
            ));
        }

        $id = absint($_REQUEST['id']);

        $forceDelete = (isset($_REQUEST['forceDelete']) ? $_REQUEST['forceDelete'] : true);

        /* if we are completely deleting the product we need to delete the postmeta too */
        if ($forceDelete) {
            $postMeta = get_post_meta($id);
            if (is_array($postMeta) && !empty($postMeta)) {
                foreach ($postMeta as $metaKey => $metaValue) {
                    delete_post_meta($id, $metaKey);
                }
            }
        }

        $deleted = wp_delete_post($id, $forceDelete);

        if ($deleted != false) {
            echo json_encode(array('status' => 1, 'deletedPost' => $deleted));
            die();
        }
    }

    public function trash()
    {
        $this->validate('shop_ct');

        if (!isset($_REQUEST['id'])) {
            die(json_encode(array(
                'status' => 0,
                'errors' => array('invalid request'),
            )));
        }

        if (is_array($_REQUEST['id'])) {
            $trash_id = array_map('absint', $_REQUEST['id']);

            foreach ($trash_id as $cur_id) {
                wp_trash_post((int)$cur_id);
            }

        } elseif (absint($_REQUEST['id']) == $_REQUEST['id']) {
            $trash_id = absint($_REQUEST['id']);

            wp_trash_post((int)$trash_id);
        } else {
            die(json_encode(array(
                'status' => 0,
                'errors' => array('invalid request'),
            )));
        }

        die(json_encode(array('status' => 1)));
    }

    public function restore()
    {
        $this->validate('shop_ct');

        if (!isset($_REQUEST['id'])) {
            die(json_encode(array(
                'status' => 0,
                'errors' => array('invalid request'),
            )));
        }


        if (is_array($_REQUEST['id'])) {
            $id = array_map('absint', $_REQUEST['id']);
        } elseif (absint($_REQUEST['id']) == $_REQUEST['id']) {
            $id = absint($_REQUEST['id']);
        }

        if (!$id) {
            die(json_encode(array(
                'status' => 0,
                'errors' => array('invalid id'),
            )));
        }

        if (is_array($id)) {
            foreach ($id as $curentId) {
                wp_untrash_post((int)$curentId);
            }
        } else {
            wp_untrash_post((int)$id);
        }

        die(json_encode(array('status' => 1)));
    }

}