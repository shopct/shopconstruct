<?php

namespace ShopCT\Controllers;


use ShopCT\Core\View;
use ShopCT\Lib\Locations;
use ShopCT\Models\Order;
use ShopCT\Models\Product;
use ShopCT\Views\AdminView;

/**
 * Class OrdersController
 * @package ShopCT\Controllers
 */
class OrdersController extends ControllerAbstract
{
    /**
     * @return mixed|string
     */
    public function index()
    {
        $args = array('posts_per_page' => Order::SHOW_PER_PAGE);
        $paged = 1;

        if (isset($_GET['paged'])) {
            $args['posts_per_page'] = Order::SHOW_PER_PAGE * absint($_GET['paged']);
            $paged = $_GET['paged'];
        }

        if (isset($_GET['search'])) {
            $args['s'] = sanitize_text_field($_GET['search']);
        }

        if (isset($_GET['post_status'])) {
            $args['post_status'] = sanitize_text_field($_GET['post_status']);
        } else {
            $args['post_status'] = 'shop-ct-pending';
        }

        $orders = Order::get($args);

        $availablePostStatuses = Order::getAvailableStatuses();

        /* count total pages */
        $countArgs = $args;
        $countArgs['paged'] = $paged;
        $countArgs['posts_per_page'] = Order::SHOW_PER_PAGE;
        $totalCount = Order::countPages($countArgs);

        $postsCount = wp_count_posts(Order::$post_type);

        $view = new AdminView(array(
            'content' => View::get('admin/orders/index/index', compact('orders', 'availablePostStatuses', 'postsCount', 'totalCount', 'paged'))
        ));

        $html = $view->get();

        if (defined('DOING_AJAX') && DOING_AJAX) {
            die(json_encode(array(
                'status' => 1,
                'data' => $html
            )));
        } else {
            return $html;
        }
    }

    public function edit()
    {
        $id = null;
        if (isset($_GET['id'])) {
            $autoDraft = false;
            $order = new Order(absint($_GET['id']));
        } else {
            $autoDraft = true;
            /** if adding new order create an auto-draft to work with */
            $post = get_default_post_to_edit(Order::$post_type, true);

            $order = new Order($post->ID);
            $order->post->post_title = '';
        }

        $countries = Locations::getCountries();
        $publishedProducts = Product::get(['post_status'=>'publish']);

        /**
         * todo: replace this helper with method in customer model
         */
        $customers = shopCTGetCustomersForSelectbox();

        echo json_encode(array(
            'status' => 1,
            'data' => View::get('admin/orders/edit/show', compact('order', 'autoDraft', 'customers', 'countries', 'publishedProducts')),
        ));
        die;
    }

    public function update()
    {
        $this->validate('shop_ct');

        if (isset($_REQUEST['order_id'])) $id = $_REQUEST['order_id'];
        else return;

        if (absint($id) != $id) {
            echo json_encode(array(
                'status' => 0,
                'errors' => array('Invalid value passed for "ID" field')
            ));
            die;
        }

        $id = absint($id);

        $order = new Order($id);

        if (isset($_REQUEST['post_data'])) {
            foreach ($_REQUEST['post_data'] as $key => $value) {
                $order->post->$key = $value;
            }
        }

        if (isset($_REQUEST['date']) && !empty($_REQUEST['date'])) {
            $REQUEST['post_meta']['date'] = $_REQUEST['product_sale_price_dates_to']
                . ' ' . (!empty($_REQUEST['date-hours']) && strlen($_REQUEST['date-hours']) <= 2 ? $_REQUEST['date-hours'] : '00')
                . ':' . (!empty($_REQUEST['date-min']) && strlen($_REQUEST['date-min']) <= 2 ? $_REQUEST['date-min'] : '00');
        } elseif($_REQUEST['order_autodraft'] == 1) {
            $REQUEST['post_meta']['date'] = date("Y-m-d H:i:s");
        }

        if (isset($_REQUEST['post_meta'])) {
            foreach ($_REQUEST['post_meta'] as $key => $value) {
                $order->$key = $value;
            }
        }

        if (isset($_REQUEST['order_products']) && !empty($_REQUEST['order_products'])) {

           if(!empty($order->products)) {
                foreach ($order->products as $existingProductId => $existingProduct) {
                    if (!isset($_REQUEST['order_products'][$existingProductId])) {
                        $order->removeProduct($existingProduct);
                    }
                }
           }

           foreach ($_REQUEST['order_products'] as $productId => $product) {
               $obj = new Product($productId);
               $order->addProduct($obj, $product['quantity']);
           }
        }

        try {
            $order->save();
            echo json_encode(array(
                'status' => 1,
            ));
            die;
        } catch (\Exception $e) {
            echo json_encode(array(
                'status' => 0,
                'errors' => array($e->getMessage())
            ));
            die;
        }
    }
}