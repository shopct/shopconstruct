<?php

namespace ShopCT\Database\Migrations;


class CreateCartTable
{
    public function run()
    {
        global $wpdb;

        $wpdb->query("CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "shop_ct_cart` (
            `hash` varchar(32) NOT NULL,
            `product_id` bigint(20) unsigned NOT NULL,
            `user_id` bigint(20) unsigned DEFAULT NULL,
            `quantity` int(11) unsigned NOT NULL,
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `updated_at` timestamp NOT NULL,
            PRIMARY KEY (`hash`,`product_id`)
            )");
    }
}