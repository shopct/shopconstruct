<?php

namespace ShopCT\Database\Migrations;


class CreateShippingZoneCountriesTable
{
    public function run()
    {
        global $wpdb;

        $wpdb->query("CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "shop_ct_shipping_zone_countries` (
			`zone_id` int(11) unsigned NOT NULL,
            `country_iso_code` varchar(2) NOT NULL,
            PRIMARY KEY (`zone_id`,`country_iso_code`)
        );");
    }
}