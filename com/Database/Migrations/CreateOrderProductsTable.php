<?php

namespace ShopCT\Database\Migrations;


class CreateOrderProductsTable
{
    public function run()
    {
        global $wpdb;

        $wpdb->query("CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "shop_ct_order_products` (
            `order_id` bigint(20) unsigned NOT NULL,
            `product_id` bigint(20) unsigned NOT NULL,
            `quantity` int(11) unsigned NOT NULL,
            `cost` float(11,2) NOT NULL,
            PRIMARY KEY (`order_id`,`product_id`)
        )");
    }
}