<?php

namespace ShopCT\Database\Migrations;


class CreateOrderMetaTable
{
    public function run()
    {
        global $wpdb;

        $wpdb->query("CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "shop_ct_order_meta`(  
          `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
          `item_id` BIGINT(20) UNSIGNED NOT NULL,
          `key` VARCHAR(100) NOT NULL,
          `value` LONGTEXT,
          `date` TIMESTAMP,
          `user_id` BIGINT(20) UNSIGNED NOT NULL,
          PRIMARY KEY  (`id`)
        );");
    }
}