<?php

namespace ShopCT\Database\Migrations;


class CreateShippingZonesTable
{
    public function run()
    {
        global $wpdb;

        $wpdb->query("CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "shop_ct_shipping_zones` (
			`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
            `name` varchar(64) NOT NULL,
			`status` int(1) unsigned NOT NULL DEFAULT 1,
            `cost` int(11) unsigned NOT NULL DEFAULT 0,
            PRIMARY KEY (`id`)
		)");

        if (count($wpdb->get_results("select id from`" . $wpdb->prefix . "shop_ct_shipping_zones` where `id` is not null")) === 0) {
            $wpdb->insert(
                $wpdb->prefix . 'shop_ct_shipping_zones',
                [
                    'id' => 1,
                    'name' => 'Rest of the world',
                    'status' => 1,
                    'cost' => 0,
                ]
            );
        }
    }

}