<?php

namespace ShopCT\Database\Migrations;


class CreateAttributesTable
{
    public function run()
    {
        global $wpdb;

        $wpdb->query("CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "shop_ct_attributes` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `name` varchar(32) NOT NULL,
          `slug` varchar(32) NOT NULL,
          `type` enum('select','text') NOT NULL,
          `order_by` enum('name','name_numeric','term_id') NOT NULL DEFAULT 'name',
          `public` int(1) NOT NULL DEFAULT '1',
          PRIMARY KEY (`id`)
        );");
    }
}