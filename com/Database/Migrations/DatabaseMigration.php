<?php

namespace ShopCT\Database\Migrations;


use ShopCT\Core\Migration;

class DatabaseMigration extends Migration
{
    public function run()
    {
        $this->call(CreateProductMetaTable::class);
        $this->call(CreateOrderMetaTable::class);
        $this->call(CreateAttributesTable::class);
        $this->call(CreateShippingZonesTable::class);
        $this->call(CreateShippingZoneCountriesTable::class);
        $this->call(CreateCartTable::class);
        $this->call(CreateOrderProductsTable::class);
        $this->call(CreateDownloadPermissionsTable::class);
    }
}