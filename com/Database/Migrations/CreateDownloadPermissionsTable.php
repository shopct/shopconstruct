<?php

namespace ShopCT\Database\Migrations;


use ShopCT\Models\Product;

class CreateDownloadPermissionsTable
{
    public function run()
    {
        global $wpdb;

        $wpdb->query("CREATE TABLE IF NOT EXISTS `" . Product::getDownloadPermissionsTableName() . "` (
            `product_id` BIGINT(20) UNSIGNED NOT NULL,
            `order_id` BIGINT(20) UNSIGNED NOT NULL,
            `email` VARCHAR(60) NOT NULL,
            `user_id` BIGINT(20) UNSIGNED,
            `token` TEXT,
            `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `expires_at` TIMESTAMP NULL DEFAULT NULL,
            `limit` int(100),
            PRIMARY KEY (`order_id`,`product_id`,`email`)
        );");
    }
}