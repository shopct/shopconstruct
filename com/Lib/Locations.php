<?php

namespace ShopCT\Lib;


class Locations
{
    /**
     * @var array
     */
    protected static $countries;

    /**
     * Get all countries.
     * @return array
     */
    public static function getCountries() {
        if ( null === self::$countries ) {
            self::$countries = apply_filters( 'shop_ct_countries', Countries::getCountries() );
            if ( apply_filters( 'shop_ct_sort_countries', true ) ) {
                asort( self::$countries );
            }
        }

        return self::$countries;
    }
}