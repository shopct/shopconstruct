<?php
/**
 * Created by PhpStorm.
 * User: Rose
 * Date: 28-May-18
 * Time: 6:20 PM
 */

namespace ShopCT\Lib;


class PostTypes
{
    public static function getOrderStatuses()
    {
        return apply_filters('shop_ct_order_statuses', array(
            'shop-ct-pending' => array(
                'label' => __('Pending Payment', 'shop_ct'),
                'public' => true,
                'exclude_from_search' => false,
                'show_in_admin_all_list' => true,
                'show_in_admin_status_list' => true,

            ),
            'shop-ct-processing' => array(
                'label' => __('Processing', 'shop_ct'),
                'public' => true,
                'exclude_from_search' => false,
                'show_in_admin_all_list' => true,
                'show_in_admin_status_list' => true,

            ),
            'shop-ct-on-hold' => array(
                'label' => __('On Hold', 'shop_ct'),
                'public' => true,
                'exclude_from_search' => false,
                'show_in_admin_all_list' => true,
                'show_in_admin_status_list' => true,
            ),
            'shop-ct-completed' => array(
                'label' => __('Completed', 'shop_ct'),
                'public' => true,
                'exclude_from_search' => false,
                'show_in_admin_all_list' => true,
                'show_in_admin_status_list' => true,
            ),
            'shop-ct-cancelled' => array(
                'label' => __('Cancelled', 'shop_ct'),
                'public' => true,
                'exclude_from_search' => false,
                'show_in_admin_all_list' => true,
                'show_in_admin_status_list' => true,
            ),
            'shop-ct-refunded' => array(
                'label' => __('Refunded', 'shop_ct'),
                'public' => true,
                'exclude_from_search' => false,
                'show_in_admin_all_list' => true,
                'show_in_admin_status_list' => true,
            ),
            'shop-ct-failed' => array(
                'label' => __('Failed', 'shop_ct'),
                'public' => true,
                'exclude_from_search' => false,
                'show_in_admin_all_list' => true,
                'show_in_admin_status_list' => true,
            ),
        ));
    }

    public static function getShippingZoneStatuses()
    {
        return apply_filters('shop_ct_shipping_zone_statuses', array(
            'shop_ct_enabled' => array(
                'label' => 'Enabled',
            ),
            'shop_ct_disabled' => array(
                'label' => 'Disabled',
            ),
        ));
    }
}