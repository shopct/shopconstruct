<?php

namespace ShopCT\Lib;


class Currencies
{
    /**
     * @param string $currency
     * @return string
     */
    public static function getCurrencySymbol($currency)
    {

        switch ($currency) {
            case 'AMD' :
                $currencySymbol = '&#1423;';
                break;
            case 'AED' :
                $currencySymbol = 'د.إ';
                break;
            case 'AUD' :
            case 'ARS' :
            case 'CAD' :
            case 'CLP' :
            case 'COP' :
            case 'HKD' :
            case 'MXN' :
            case 'NZD' :
            case 'SGD' :
            case 'USD' :
                $currencySymbol = '&#36;';
                break;
            case 'BDT':
                $currencySymbol = '&#2547;&nbsp;';
                break;
            case 'BGN' :
                $currencySymbol = '&#1083;&#1074;.';
                break;
            case 'BRL' :
                $currencySymbol = '&#82;&#36;';
                break;
            case 'CHF' :
                $currencySymbol = '&#67;&#72;&#70;';
                break;
            case 'CNY' :
            case 'JPY' :
            case 'RMB' :
                $currencySymbol = '&yen;';
                break;
            case 'CZK' :
                $currencySymbol = '&#75;&#269;';
                break;
            case 'DKK' :
                $currencySymbol = 'DKK';
                break;
            case 'DOP' :
                $currencySymbol = 'RD&#36;';
                break;
            case 'EGP' :
                $currencySymbol = 'EGP';
                break;
            case 'EUR' :
                $currencySymbol = '&euro;';
                break;
            case 'GBP' :
                $currencySymbol = '&pound;';
                break;
            case 'HRK' :
                $currencySymbol = 'Kn';
                break;
            case 'HUF' :
                $currencySymbol = '&#70;&#116;';
                break;
            case 'IDR' :
                $currencySymbol = 'Rp';
                break;
            case 'ILS' :
                $currencySymbol = '&#8362;';
                break;
            case 'INR' :
                $currencySymbol = 'Rs.';
                break;
            case 'ISK' :
                $currencySymbol = 'Kr.';
                break;
            case 'KIP' :
                $currencySymbol = '&#8365;';
                break;
            case 'KRW' :
                $currencySymbol = '&#8361;';
                break;
            case 'MYR' :
                $currencySymbol = '&#82;&#77;';
                break;
            case 'NGN' :
                $currencySymbol = '&#8358;';
                break;
            case 'NOK' :
                $currencySymbol = '&#107;&#114;';
                break;
            case 'NPR' :
                $currencySymbol = 'Rs.';
                break;
            case 'PHP' :
                $currencySymbol = '&#8369;';
                break;
            case 'PLN' :
                $currencySymbol = '&#122;&#322;';
                break;
            case 'PYG' :
                $currencySymbol = '&#8370;';
                break;
            case 'RON' :
                $currencySymbol = 'lei';
                break;
            case 'RUB' :
                $currencySymbol = '&#1088;&#1091;&#1073;.';
                break;
            case 'SEK' :
                $currencySymbol = '&#107;&#114;';
                break;
            case 'THB' :
                $currencySymbol = '&#3647;';
                break;
            case 'TRY' :
                $currencySymbol = '&#8378;';
                break;
            case 'TWD' :
                $currencySymbol = '&#78;&#84;&#36;';
                break;
            case 'UAH' :
                $currencySymbol = '&#8372;';
                break;
            case 'VND' :
                $currencySymbol = '&#8363;';
                break;
            case 'ZAR' :
                $currencySymbol = '&#82;';
                break;
            default :
                $currencySymbol = '';
                break;
        }

        return apply_filters('shop_ct_get_currency_symbol', $currencySymbol, $currency);
    }

    public static function getCurrencies()
    {
        return apply_filters('shop_ct_get_currencies',
            array(
                'AMD' => __('Armenian Dram', 'shop_ct'),
                'AED' => __('United Arab Emirates Dirham', 'shop_ct'),
                'ARS' => __('Argentine Peso', 'shop_ct'),
                'AUD' => __('Australian Dollars', 'shop_ct'),
                'BDT' => __('Bangladeshi Taka', 'shop_ct'),
                'BRL' => __('Brazilian Real', 'shop_ct'),
                'BGN' => __('Bulgarian Lev', 'shop_ct'),
                'CAD' => __('Canadian Dollars', 'shop_ct'),
                'CLP' => __('Chilean Peso', 'shop_ct'),
                'CNY' => __('Chinese Yuan', 'shop_ct'),
                'COP' => __('Colombian Peso', 'shop_ct'),
                'CZK' => __('Czech Koruna', 'shop_ct'),
                'DKK' => __('Danish Krone', 'shop_ct'),
                'DOP' => __('Dominican Peso', 'shop_ct'),
                'EUR' => __('Euros', 'shop_ct'),
                'HKD' => __('Hong Kong Dollar', 'shop_ct'),
                'HRK' => __('Croatia kuna', 'shop_ct'),
                'HUF' => __('Hungarian Forint', 'shop_ct'),
                'ISK' => __('Icelandic krona', 'shop_ct'),
                'IDR' => __('Indonesia Rupiah', 'shop_ct'),
                'INR' => __('Indian Rupee', 'shop_ct'),
                'NPR' => __('Nepali Rupee', 'shop_ct'),
                'ILS' => __('Israeli Shekel', 'shop_ct'),
                'JPY' => __('Japanese Yen', 'shop_ct'),
                'KIP' => __('Lao Kip', 'shop_ct'),
                'KRW' => __('South Korean Won', 'shop_ct'),
                'MYR' => __('Malaysian Ringgits', 'shop_ct'),
                'MXN' => __('Mexican Peso', 'shop_ct'),
                'NGN' => __('Nigerian Naira', 'shop_ct'),
                'NOK' => __('Norwegian Krone', 'shop_ct'),
                'NZD' => __('New Zealand Dollar', 'shop_ct'),
                'PYG' => __('Paraguayan Guaraní', 'shop_ct'),
                'PHP' => __('Philippine Pesos', 'shop_ct'),
                'PLN' => __('Polish Zloty', 'shop_ct'),
                'GBP' => __('Pounds Sterling', 'shop_ct'),
                'RON' => __('Romanian Leu', 'shop_ct'),
                'RUB' => __('Russian Ruble', 'shop_ct'),
                'SGD' => __('Singapore Dollar', 'shop_ct'),
                'ZAR' => __('South African rand', 'shop_ct'),
                'SEK' => __('Swedish Krona', 'shop_ct'),
                'CHF' => __('Swiss Franc', 'shop_ct'),
                'TWD' => __('Taiwan New Dollars', 'shop_ct'),
                'THB' => __('Thai Baht', 'shop_ct'),
                'TRY' => __('Turkish Lira', 'shop_ct'),
                'UAH' => __('Ukrainian Hryvnia', 'shop_ct'),
                'USD' => __('US Dollars', 'shop_ct'),
                'VND' => __('Vietse Dong', 'shop_ct'),
                'EGP' => __('Egyptian Pound', 'shop_ct')
            )
        );
    }
}