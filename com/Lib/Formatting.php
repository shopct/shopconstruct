<?php

namespace ShopCT\Lib;


use ShopCT\Models\Order;
use ShopCT\Models\Product;

class Formatting
{
    public static function snakeCaseToPascalCase($string)
    {
        return str_replace('_', '', ucwords($string, '_'));;
    }

    /**
     * @param float|integer|string $price
     * @param string $symbol
     * @return string
     */
    public static function formatPrice($price, $symbol = null)
    {
        if (empty($price)) {
            return __('Free', 'shop_ct');
        }

        $currency = shopCTGeneralSettings()->currency;
        $currency_pos = shopCTGeneralSettings()->currency_pos;
        $num_decimals = shopCTGeneralSettings()->price_num_decimals;
        $decimal_sep = shopCTGeneralSettings()->price_decimal_sep;
        $thousand_sep = shopCTGeneralSettings()->price_thousand_sep;

        if (empty($symbol)) {
            $symbol = Currencies::getCurrencySymbol($currency);
        }

        if ($currency_pos == "left") {
            $formatted_price = !empty($price)
                ? $symbol . number_format((float)$price, $num_decimals, $decimal_sep, $thousand_sep)
                : "";
        } elseif ($currency_pos == "right") {
            $formatted_price = !empty($price)
                ? number_format((float)$price, $num_decimals, $decimal_sep, $thousand_sep) . $symbol
                : "";
        } elseif ($currency_pos == "left-space") {
            $formatted_price = !empty($price)
                ? $symbol . "&nbsp;" . number_format((float)$price, $num_decimals, $decimal_sep, $thousand_sep)
                : "";
        } else {
            $formatted_price = !empty($price)
                ? number_format((float)$price, $num_decimals, $decimal_sep, $thousand_sep) . "&nbsp;" . $symbol
                : "";
        };

        return $formatted_price;
    }

    /**
     * @param $product Product
     * @return string
     */
    public static function getPriceHtml($product)
    {
        if ($product->isOnSale()) {
            $result = '<del><span class="shop-ct-amount">' . \ShopCT\Lib\Formatting::formatPrice($product->regular_price) . '</span></del>&nbsp;<ins><span class="shop-ct-amount">' . \ShopCT\Lib\Formatting::formatPrice($product->getPrice()) . '</span></ins>';
        } else {
            $result = '<span class="shop-ct-amount">' . \ShopCT\Lib\Formatting::formatPrice($product->getPrice()) . '</span>';
        }

        return apply_filters('shop_ct_product_price_html', $result);
    }

    public static function formatShippingAddresses(Order $order)
    {
        $addresses = array();

        if (!empty($order->shipping_address_1)) {
           $addresses[] = $order->shipping_address_1;
        }

        if(!empty($order->shipping_address_2)) {
            $addresses[] = $order->shipping_address_2;
        }

        if (!empty($addresses)) {
            return implode(',', $addresses);
        } else {
            return '&#8212;';
        }

    }
}