<?php

namespace ShopCT\Services;


use ShopCT\Models\Settings\PaymentGateways\BankTransferSettings;
use ShopCT\Models\Settings\PaymentGateways\CashOnDeliverySettings;
use ShopCT\Models\Settings\PaymentGateways\PayPalSettings;
use ShopCT\Models\Settings\SettingsAbstract;

class PaymentGateways
{
    private static $instances;

    /**
     * @var SettingsAbstract[]
     */
    private static $gateways = array(
        PayPalSettings::class,
        BankTransferSettings::class,
        CashOnDeliverySettings::class,
    );

    private static function getInstances($gateways)
    {
        if (empty(self::$instances)) {
            foreach ($gateways as $gateway) {
                $instance = call_user_func($gateway, 'instance');

                self::$instances[$instance->id] = $instance;
            }
        }

        return self::$instances;
    }

    /**
     * @return mixed
     */
    public static function getAvailablePaymentGateways()
    {
        $paymentGateways = apply_filters('shop_ct_payment_gateways', self::$gateways);

        $instances = self::getInstances($paymentGateways);

        $availablePaymentGateways = array();

        foreach ($instances as $id => $instance) {
            if ($instance->enabled === 'no') {
                unset($instances[$id]);
            }
        }

        return $instances;
    }
}