<?php

define('SHOP_CT_DELIMITER', ',');

$upload_dir = wp_upload_dir();
define('SHOP_CT_LOG_DIR', $upload_dir['basedir'] . '/shop-ct-logs/');

//define('SHOP_CT_TEMPLATE_PATH', 'shop-ct/');

define('SHOP_CT_PLUGIN_PATH', dirname(__FILE__));
define('SHOP_CT_PLUGIN_URL', plugin_dir_url(__FILE__));
define('SHOP_CT_TEMPLATES_PATH', SHOP_CT_PLUGIN_PATH . DIRECTORY_SEPARATOR . 'com' . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . 'views');
define('SHOP_CT_PUBLIC_URL', SHOP_CT_PLUGIN_URL . 'public/');

/**
 * Used for API stuff
 */
define('SHOP_CT_ENDPOINT_URL', site_url('/shop-ct/'));

/*define('SHOP_CT_IMAGES_PATH', $this->plugin_path() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR);
define('SHOP_CT_IMAGES_URL', $this->plugin_url() . '/assets/images/');
define('SHOP_CT_TEMPLATES_PATH', $this->plugin_path() . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR);
define('SHOP_CT_EMAIL_TEMPLATES_PATH', SHOP_CT_TEMPLATES_PATH . 'emails' . DIRECTORY_SEPARATOR);
define('SHOP_CT_ADMIN_EMAIL_TEMPLATES_PATH', SHOP_CT_EMAIL_TEMPLATES_PATH . 'admin' . DIRECTORY_SEPARATOR);
define('SHOP_CT_CUSTOMER_EMAIL_TEMPLATES_PATH', SHOP_CT_EMAIL_TEMPLATES_PATH . 'customer' . DIRECTORY_SEPARATOR);*/