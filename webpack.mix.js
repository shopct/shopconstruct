let mix = require('laravel-mix');

mix.setPublicPath('public');

mix.options({processCssUrls: false});

mix.js('com/resources/assets/js/admin/app.js', 'public/js/admin.js')
    .sass('com/resources/assets/sass/admin/app.scss', 'public/css/admin.css')
    .copy('node_modules/material-design-icons/iconfont/', 'public/css')
    .copy('com/resources/assets/fonts', 'public/fonts');