<?php
/**
 * Plugin Name: ShopConstruct M
 * Plugin URI: http://shopconstruct.com
 * Description: ShopConstruct
 * Version:     10.0.0
 * Author:      ShopConstruct
 * Author URI:  http://shopconstruct.com
 * License: GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 * Domain Path: /languages
 * Text Domain: shop_ct
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

define('SHOP_CT_VERSION', '1.0.0');

//first we need composer autoloaded
require_once(__DIR__ . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php");

//then we need plugin configs
require __DIR__ . DIRECTORY_SEPARATOR . 'config.php';

//we are all set up to bootstrap the plugin
require __DIR__ . DIRECTORY_SEPARATOR . 'com/bootstrap.php';